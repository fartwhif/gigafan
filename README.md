# GigaFan

Partially decompiled Gigabyte library needed to sense and control Gigabyte motherboard fan headers and read temperatures.

## Getting started

To make it easy to get started with GigaFan there's a helper class which saves the dependency DLLs and touches the required registry path.

## Example

```C#
using GigaFan;
using GigaFan.MyDecompiledEnvironmentControl;

using System;
using System.Threading;

namespace ExampleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GigaFanHelper.GigaFanInit();
            MyFanControl fc = new MyFanControl();
            if (fc.IsSupported)
            {
                Console.WriteLine($"FanCount: {fc.FanCount}");
                Console.WriteLine($"FanControlCount: {fc.FanControlCount}");

                // set all fans to 50% PWM
                for (int i = 0; i < fc.FanCount; i++)
                {
                    fc.SetFanSpeedFixedMode(i, 50);
                }

                string displayName = string.Empty;
                int top = Console.CursorTop + 1;
                float fanSpeed = 0;
                float temp = 0;
                while (true)
                {

                    for (int i = 0; i < fc.FanCount; i++)
                    {
                        Console.SetCursorPosition(0, top + i);
                        Console.WriteLine();
                        Console.SetCursorPosition(0, top + i);

                        fc.GetFanDisplayName(i, out displayName);
                        fc.GetFanSpeed(i, out fanSpeed);

                        Console.WriteLine($"{"Fan",-4} {i}: {displayName + ":",-20} {fanSpeed,4:F0} RPM");
                    }
                    Console.WriteLine();
                    for (int i = 0; i < fc.TemperatureCount; i++)
                    {
                        fc.GetTemperatureDisplayName(i, out displayName);
                        fc.GetTemperature(i, out temp);
                        Console.WriteLine($"Temp {i}: {displayName + ":",-20} {temp,4:F0} C");
                    }
                    Thread.Sleep(100);
                }
            }
        }
    }
}
```

### Output:

```
FanCount: 5
FanControlCount: 5

Fan  0: CPU Fan:              646 RPM
Fan  1: CPU OPT:              533 RPM
Fan  2: System Fan 1:         445 RPM
Fan  3: System Fan 2:         228 RPM
Fan  4: System Fan 3:         315 RPM

Temp 0: SYSTEM:                37 C
Temp 1: PCH:                   41 C
Temp 2: CPU:                   53 C
```
