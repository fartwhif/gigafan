﻿using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace GigaFan
{
    public static class GigaFanHelper
    {
        public static void GigaFanInit()
        {
            CopyDependencyDLLs();
            TouchRequiredRegistryLocation(false);
        }
        public static void CopyDependencyDLLs()
        {
            CopyDependencyDLLs(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
        }
        public static void CopyDependencyDLLs(string dstDir)
        {
            string[] dlls = new string[3] {
                "mfccpu.dll","MFCMB.dll","ycc.dll"
            };

            foreach (string dll in dlls)
            {
                string dllDst = Path.Combine(dstDir, dll);
                if (!File.Exists(dllDst))
                {
                    WriteResourceToFile(dll, dllDst);
                }
            }
        }
        private static void WriteResourceToFile(string resourceName, string fileName)
        {
            using (var resource = Assembly.GetExecutingAssembly().GetManifestResourceStream($"GigaFan.{resourceName}"))
            {
                using (var file = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    resource.CopyTo(file);
                }
            }
        }
        public static bool TouchRequiredRegistryLocation(bool LogToConsole)
        {
            string RequiredRegistryPath = @"SOFTWARE\GIGABYTE\ThermalConsole\SIO";
            bool result = Touch(RequiredRegistryPath);
            if (result)
            {
                if (LogToConsole)
                    Console.WriteLine($"Touched required registry path: {RequiredRegistryPath}");
            }
            else
            {
                if (LogToConsole)
                    Console.WriteLine($"Failed to touch required registry path: {RequiredRegistryPath}");
            }
            return result;
        }
        private static bool Touch(string path)
        {
            List<string> parts = path.Split('\\').ToList();
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(parts.First(), true))
            {
                if (key == null) { return false; }
                parts.RemoveAt(0);
                return Touch(key, ref parts);
            }
        }
        private static bool Touch(RegistryKey key, ref List<string> parts)
        {
            using (RegistryKey key2 = key.OpenSubKey(parts.First(), true))
            {
                if (key2 == null)
                {
                    using (RegistryKey key3 = key.CreateSubKey(parts.First(), true))
                    {
                        if (key3 == null)
                        {
                            return false;
                        }
                        else
                        {
                            parts.RemoveAt(0);
                            if (parts.Count == 0)
                            {
                                return true;
                            }
                            else
                            {
                                return Touch(key3, ref parts);
                            }
                        }
                    }
                }
                else
                {
                    parts.RemoveAt(0);
                    if (parts.Count == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return Touch(key2, ref parts);
                    }
                }
            }
        }
    }
}
