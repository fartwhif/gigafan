﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Mainboard.SeriesChipsets
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal enum MySeriesChipsets
    {
        Unknow,
        Intel_Z97Series,
        Intel_H97Series,
        Intel_Z87Series,
        Intel_Z85Series,
        Intel_H81Series,
        Intel_H86Series,
        Intel_H87Series,
        Intel_Q85Series,
        Intel_Q87Series,
        Intel_B85Series,
        Intel_X99Series,
        AMD_A85X,
        AMD_A75,
        AMD_A55,
        AMD_A88X,
    }
}
