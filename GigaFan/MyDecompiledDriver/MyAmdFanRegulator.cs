﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.Amd.AmdFanRegulator
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll


using System;
using System.Collections.Generic;
using System.Threading;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyAmdFanRegulator : MyFanCooler
    {
        private List<MyFanTemperatureInput> m_temperatureInputTable;
        private MyVirtualToPhysicalIndexTable m_virtual2PhysicalFanControlTable;
        private List<bool> m_fanControlEnableTable;
        private MyAmdMotherboardHealthIdentifier m_mbHealthIdentifier;
        private MyVirtualToPhysicalIndexTable m_virtual2PhysicalFanTachometerTable;

        public MyAmdFanRegulator()
          : this(MyAmdMotherboardHealthIdentification.MHID_Unknown)
        {
        }

        public MyAmdFanRegulator(MyAmdMotherboardHealthIdentification healthid)
        {
            MyAmdMotherboardHealthIdentification pGroupNumber = MyAmdMotherboardHealthIdentification.MHID_Unknown;
            if (healthid == MyAmdMotherboardHealthIdentification.MHID_Unknown)
            {
                if (this.m_mbHealthIdentifier == null)
                    this.m_mbHealthIdentifier = new MyAmdMotherboardHealthIdentifier();
                this.m_mbHealthIdentifier.GetGroupNumber(out pGroupNumber);
            }
            else
                pGroupNumber = healthid;
            this.IsSupported = false;
            this.FanControlCount = 0;
            this.FanCount = 0;
            this.TemperatureCount = 0;
            this.UsingExtendedTemperature = false;
            this.MaxSlopeCount = 3;
            this.HealthNumber = MyAmdMotherboardHealthIdentification.MHID_Unknown;
            this.InitObjects(pGroupNumber);
        }

        public MyAmdMotherboardHealthIdentification HealthNumber { get; protected set; }

        public override void SetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.SetProtectValue(fanControlIndex, 0, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            if (smartGuardianFanConfigs.Count == 0)
                throw new ArgumentOutOfRangeException();
            if (slopeIndex >= smartGuardianFanConfigs.Count)
                throw new ArgumentOutOfRangeException();
            try
            {
                this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), slopeIndex, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.SetProtectValue(fanControlIndex, 0, bCalibrateMode, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), slopeIndex, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void GetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                this.m_iteMgr.GetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref guardianFanConfig);
                if (false)
                    return;
                this.RetrieveProtectValue(ref guardianFanConfig, this.MaxSlopeCount, ref pSmartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfig smartGuardianFanConfig)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref smartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void GetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfig pSmartGuardianFanConfig)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            pSmartGuardianFanConfig.DeltaTemperature = (byte)0;
            pSmartGuardianFanConfig.FanSpeed = 0;
            pSmartGuardianFanConfig.Slope = 0.0f;
            pSmartGuardianFanConfig.StartPWM = (byte)0;
            pSmartGuardianFanConfig.TargetZoneBoundary = (byte)0;
            pSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)0;
            pSmartGuardianFanConfig.TemperatureLimitValueOfFanOff = (byte)0;
            pSmartGuardianFanConfig.TemperatureLimitValueOfFanStart = (byte)0;
            try
            {
                int fanControlIndex1 = this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex);
                MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
                this.m_iteMgr.GetFanSmartGuardianConfig(fanControlIndex1, ref fanConfig);
                if (fanConfig == null)
                    return;
                pSmartGuardianFanConfig.DeltaTemperature = fanConfig.DeltaTemperature;
                pSmartGuardianFanConfig.FanSpeed = fanConfig.FanSpeed;
                pSmartGuardianFanConfig.Slope = fanConfig.Slope;
                pSmartGuardianFanConfig.StartPWM = fanConfig.StartPWM;
                pSmartGuardianFanConfig.TargetZoneBoundary = fanConfig.TargetZoneBoundary;
                pSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed = fanConfig.TemperatureLimitValueOfFanFullSpeed;
                pSmartGuardianFanConfig.TemperatureLimitValueOfFanOff = fanConfig.TemperatureLimitValueOfFanOff;
                pSmartGuardianFanConfig.TemperatureLimitValueOfFanStart = fanConfig.TemperatureLimitValueOfFanStart;
            }
            catch
            {
                throw;
            }
        }

        public override void GetCpuFanSpeed(out float fanSpeed)
        {
            try
            {
                fanSpeed = 0.0f;
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanSpeed(int fanIndex, out float fanSpeed)
        {
            try
            {
                fanSpeed = 0.0f;
                List<float> pHardwareMonitorDatas = new List<float>();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                int index = this.m_virtual2PhysicalFanTachometerTable.Virtual2PhysicalIndex(fanIndex);
                if (index >= pHardwareMonitorDatas.Count)
                    return;
                fanSpeed = pHardwareMonitorDatas[index];
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanSpeed(int fanIndex, bool bSwitchFanIndex, out float fanSpeed)
        {
            this.GetFanSpeed(fanIndex, out fanSpeed);
        }

        public override void GetFanDisplayName(int fanIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (!this.IsSupported)
                throw new NotSupportedException();
            this.RetrieveFanDisplayName(this.HealthNumber, fanIndex, out pDisplayName);
        }

        public override void GetFanControlDisplayName(int fanControlIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            this.RetrieveFanControlDisplayName(this.HealthNumber, fanControlIndex, out pDisplayName);
        }

        public override void SetCalibrationPwm(int fanControlIndex, byte pwmPercentage)
        {
            MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
            try
            {
                float maxPwmValue = (float)this.m_iteMgr.MaxPwmValue;
                float d = (float)pwmPercentage * (maxPwmValue / 100f);
                float num1 = 50f;
                float num2 = 80f;
                float num3 = (float)(((double)this.m_iteMgr.MaxPwmValue - (double)d) / ((double)num2 - (double)num1));
                fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                fanConfig.TemperatureLimitValueOfFanStart = (byte)num1;
                fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)num2;
                fanConfig.StartPWM = (byte)Math.Floor((double)d);
                fanConfig.Slope = num3;
                fanConfig.DeltaTemperature = (byte)3;
                fanConfig.TargetZoneBoundary = (byte)0;
                this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void ResetCalibrationPwm()
        {
        }

        public override void SetFanSpeedFixedMode(int fanControlIndex, byte pwmPercentage)
        {
            MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
            try
            {
                float maxPwmValue = (float)this.m_iteMgr.MaxPwmValue;
                float d = (float)pwmPercentage * (maxPwmValue / 100f);
                float num1 = 70f;
                float num2 = 71f;
                float num3 = (float)(((double)this.m_iteMgr.MaxPwmValue - (double)d) / ((double)num2 - (double)num1));
                fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                fanConfig.TemperatureLimitValueOfFanStart = (byte)num1;
                fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)num2;
                fanConfig.StartPWM = (byte)Math.Floor((double)d);
                fanConfig.Slope = (double)num3 == 0.0 ? 0.0f : 1f;
                fanConfig.DeltaTemperature = (byte)3;
                fanConfig.TargetZoneBoundary = (byte)0;
                this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override bool IsRequireSoftwareMonitor(int fanControlIndex) => true;

        public override void GetTemperature(int temperatureIndex, out float targetTemperature)
        {
            targetTemperature = 0.0f;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (temperatureIndex >= this.m_iteMgr.TemperatureCount)
                throw new IndexOutOfRangeException();
            List<float> pHardwareMonitorDatas = new List<float>();
            try
            {
                this.m_iteMgr.Update();
                this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                if (temperatureIndex >= pHardwareMonitorDatas.Count)
                    return;
                targetTemperature = pHardwareMonitorDatas[temperatureIndex];
            }
            catch
            {
                throw;
            }
        }

        public override void GetTemperatureInputSelection(
          int fanControlIndex,
          ref MyFanTemperatureInput pTemperatureInputSelection)
        {
            pTemperatureInputSelection = MyFanTemperatureInput.Unknown;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                int num = fanControlIndex;
                if (this.TemperatureCount == 2)
                {
                    if (num == 0)
                        pTemperatureInputSelection = MyFanTemperatureInput.CPU;
                    else
                        pTemperatureInputSelection = MyFanTemperatureInput.SYSTEM;
                }
                else
                    this.m_iteMgr.GetTemperatureInputSelection(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref pTemperatureInputSelection);
            }
            catch
            {
                throw;
            }
        }

        public override void GetTemperatureDisplayName(int temperatureIndex, out string pDisplayName)
        {
            pDisplayName = "Temperature";
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (temperatureIndex >= this.TemperatureCount)
                throw new ArgumentOutOfRangeException();
            this.RetrieveTemperatureDisplayName(temperatureIndex, out pDisplayName);
        }

        public override void GetHardwareMonitorDatas(
          ref List<float> pTemperatureDatas,
          ref List<float> pFanSpeedDatas)
        {
            if (!this.IsSupported)
                throw new NotSupportedException();
            try
            {
                this.RetrieveTemperatureHardwareMonitorDatas(ref pTemperatureDatas);
                this.RetrieveFanHardwareMonitorDatas(ref pFanSpeedDatas);
            }
            catch
            {
            }
        }

        protected void InitFanObjects(MyAmdMotherboardHealthIdentification healthNumber)
        {
            if (this.m_iteMgr != null)
                return;
            try
            {
                MyIT87XXFinder it87XxFinder = new MyIT87XXFinder();
                bool flag;
                switch (healthNumber)
                {
                    case MyAmdMotherboardHealthIdentification.MHID_B001:
                    case MyAmdMotherboardHealthIdentification.MHID_B003:
                    case MyAmdMotherboardHealthIdentification.MHID_B002:
                        flag = true;
                        break;
                    default:
                        flag = true;
                        break;
                }
                if (flag)
                {
                    for (int index = 0; index < 6; ++index)
                    {
                        it87XxFinder.Find(true, out this.m_iteMgr);
                        if (this.m_iteMgr == null)
                            Thread.Sleep(1000);
                        else
                            break;
                    }
                }
                this.FanControlCount = this.m_iteMgr != null ? this.m_iteMgr.ActualFanControlRegisterCount : throw new NotSupportedException();
                this.IsSupported = true;
                this.MaxPwmValue = this.m_iteMgr.MaxPwmValue;
                this.FanCount += this.m_iteMgr.FanTachometerCount;
            }
            catch
            {
                throw;
            }
        }

        protected virtual void InitFanRelativeVariables(MyAmdMotherboardHealthIdentification healthid)
        {
            switch (healthid)
            {
                case MyAmdMotherboardHealthIdentification.MHID_B001:
                    this.FanControlCount = 3;
                    this.FanCount = 3;
                    break;
                case MyAmdMotherboardHealthIdentification.MHID_B003:
                    this.FanControlCount = 2;
                    this.FanCount = 2;
                    break;
                case MyAmdMotherboardHealthIdentification.MHID_B002:
                    this.FanControlCount = 2;
                    this.FanCount = 2;
                    break;
            }
        }

        protected virtual void InitFanControlSupportTable(MyAmdMotherboardHealthIdentification healthid)
        {
            if (this.m_fanControlEnableTable == null)
                this.m_fanControlEnableTable = new List<bool>();
            if (this.m_fanControlEnableTable.Count > 0)
                this.m_fanControlEnableTable.Clear();
            if (this.FanControlCount == 0)
                return;
            for (int index = 0; index < this.FanControlCount; ++index)
                this.m_fanControlEnableTable.Add(true);
            if (this.m_iteMgr != null)
                return;
            for (int index = 0; index < this.m_fanControlEnableTable.Count; ++index)
                this.m_fanControlEnableTable[index] = false;
        }

        protected virtual void InitFanMappingTable(MyAmdMotherboardHealthIdentification healthid)
        {
            if (this.m_virtual2PhysicalFanControlTable == null)
                this.m_virtual2PhysicalFanControlTable = new MyVirtualToPhysicalIndexTable();
            if (this.m_virtual2PhysicalFanControlTable.Count > 0)
                this.m_virtual2PhysicalFanControlTable.Clear();
            switch (healthid)
            {
                case MyAmdMotherboardHealthIdentification.MHID_B001:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    break;
                case MyAmdMotherboardHealthIdentification.MHID_B003:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    break;
                case MyAmdMotherboardHealthIdentification.MHID_B002:
                    MyFanControlOutputModeSelection fanControlOutputMode1 = MyFanControlOutputModeSelection.SmartGuardianMode;
                    MyFanControlOutputModeSelection fanControlOutputMode2 = MyFanControlOutputModeSelection.SmartGuardianMode;
                    MyFanControlOutputModeSelection fanControlOutputMode3 = MyFanControlOutputModeSelection.SmartGuardianMode;
                    if (this.m_iteMgr != null)
                        this.m_iteMgr.GetFanControlOutputModeSelection(out fanControlOutputMode1, out fanControlOutputMode2, out fanControlOutputMode3);
                    if (fanControlOutputMode1 == MyFanControlOutputModeSelection.SmartGuardianMode && fanControlOutputMode3 == MyFanControlOutputModeSelection.OnOffMode)
                        this.m_virtual2PhysicalFanControlTable.Add(0);
                    else if (fanControlOutputMode1 == MyFanControlOutputModeSelection.OnOffMode && fanControlOutputMode3 == MyFanControlOutputModeSelection.SmartGuardianMode)
                        this.m_virtual2PhysicalFanControlTable.Add(2);
                    else
                        this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    break;
            }
            if (this.m_virtual2PhysicalFanControlTable.Count == 0)
                throw new NotSupportedException();
        }

        protected virtual void InitFanTachometerTable(MyAmdMotherboardHealthIdentification healthid)
        {
            if (this.m_virtual2PhysicalFanTachometerTable == null)
                this.m_virtual2PhysicalFanTachometerTable = new MyVirtualToPhysicalIndexTable();
            if (this.m_virtual2PhysicalFanTachometerTable.Count > 0)
                this.m_virtual2PhysicalFanTachometerTable.Clear();
            switch (healthid)
            {
                case MyAmdMotherboardHealthIdentification.MHID_B001:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    break;
                case MyAmdMotherboardHealthIdentification.MHID_B003:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    break;
                case MyAmdMotherboardHealthIdentification.MHID_B002:
                    MyFanControlOutputModeSelection fanControlOutputMode1 = MyFanControlOutputModeSelection.SmartGuardianMode;
                    MyFanControlOutputModeSelection fanControlOutputMode2 = MyFanControlOutputModeSelection.SmartGuardianMode;
                    MyFanControlOutputModeSelection fanControlOutputMode3 = MyFanControlOutputModeSelection.SmartGuardianMode;
                    if (this.m_iteMgr != null)
                        this.m_iteMgr.GetFanControlOutputModeSelection(out fanControlOutputMode1, out fanControlOutputMode2, out fanControlOutputMode3);
                    if (fanControlOutputMode1 == MyFanControlOutputModeSelection.SmartGuardianMode && fanControlOutputMode3 == MyFanControlOutputModeSelection.OnOffMode)
                        this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    else if (fanControlOutputMode1 == MyFanControlOutputModeSelection.OnOffMode && fanControlOutputMode3 == MyFanControlOutputModeSelection.SmartGuardianMode)
                        this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    else
                        this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    break;
            }
            if (this.m_virtual2PhysicalFanTachometerTable.Count == 0)
                throw new NotSupportedException();
        }

        protected virtual void InitTemperatureMappingTable(MyAmdMotherboardHealthIdentification healthid)
        {
            if (this.m_temperatureInputTable == null)
                this.m_temperatureInputTable = new List<MyFanTemperatureInput>();
            if (this.m_temperatureInputTable.Count > 0)
                this.m_temperatureInputTable.Clear();
            switch (healthid)
            {
                case MyAmdMotherboardHealthIdentification.MHID_B001:
                case MyAmdMotherboardHealthIdentification.MHID_B003:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.Unknown);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    break;
                case MyAmdMotherboardHealthIdentification.MHID_B002:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.Unknown);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    break;
                default:
                    throw new NotSupportedException();
            }
            this.TemperatureCount = this.m_temperatureInputTable.Count;
        }

        protected virtual void RetrieveFanDisplayName(
          MyAmdMotherboardHealthIdentification healthid,
          int fanControlIndex,
          out string pDdisplayName)
        {
            pDdisplayName = string.Empty;
            switch (healthid)
            {
                case MyAmdMotherboardHealthIdentification.MHID_Unknown:
                    break;
                case MyAmdMotherboardHealthIdentification.MHID_B001:
                case MyAmdMotherboardHealthIdentification.MHID_B003:
                case MyAmdMotherboardHealthIdentification.MHID_B002:
                    this.RetrieveFanControlDisplayName4NonProcessorOption(fanControlIndex, out pDdisplayName);
                    break;
                default:
                    this.RetrieveFanControlDisplayName4NonProcessorOption(fanControlIndex, out pDdisplayName);
                    break;
            }
        }

        protected virtual void RetrieveFanControlDisplayName(
          MyAmdMotherboardHealthIdentification healthid,
          int fanControlIndex,
          out string pDdisplayName)
        {
            pDdisplayName = string.Empty;
            switch (healthid)
            {
                case MyAmdMotherboardHealthIdentification.MHID_B001:
                case MyAmdMotherboardHealthIdentification.MHID_B003:
                case MyAmdMotherboardHealthIdentification.MHID_B002:
                    this.RetrieveFanControlDisplayName4NonProcessorOption(fanControlIndex, out pDdisplayName);
                    break;
            }
        }

        protected virtual void RetrieveFanControlDisplayName(
          int fanControlIndex,
          out string pDisplayName)
        {
            pDisplayName = string.Empty;
            switch (fanControlIndex)
            {
                case 0:
                    pDisplayName = "CPU Fan";
                    break;
                case 1:
                    pDisplayName = "CPU OPT";
                    break;
                default:
                    int num = fanControlIndex - 1;
                    pDisplayName = string.Format("System Fan {0}", (object)num);
                    break;
            }
        }

        protected virtual void RetrieveFanControlDisplayName4NonProcessorOption(
          int fanControlIndex,
          out string pDdisplayName)
        {
            pDdisplayName = string.Empty;
            if (fanControlIndex == 0)
            {
                pDdisplayName = "CPU Fan";
            }
            else
            {
                int num = fanControlIndex;
                pDdisplayName = string.Format("System Fan {0}", (object)num);
            }
        }

        protected virtual void RetrieveTemperatureHardwareMonitorDatas(ref List<float> pTemperatureDatas)
        {
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (pTemperatureDatas.Count > 0)
                pTemperatureDatas.Clear();
            try
            {
                List<float> pHardwareMonitorDatas = new List<float>();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                for (int index = 0; index < this.m_temperatureInputTable.Count; ++index)
                {
                    if (index != 1 || this.m_temperatureInputTable[index] != MyFanTemperatureInput.Unknown)
                        pTemperatureDatas.Add(pHardwareMonitorDatas[index]);
                }
            }
            catch
            {
                throw;
            }
        }

        protected virtual void RetrieveFanHardwareMonitorDatas(ref List<float> pFanSpeeds)
        {
            if (this.m_iteMgr.FanTachometerCount == 0)
                return;
            if (pFanSpeeds.Count > 0)
                pFanSpeeds.Clear();
            try
            {
                switch (this.m_iteMgr.chip)
                {
                    case MyChip.IT8620E:
                    case MyChip.IT8628E:
                        this.RetrieveFanHardwareMonitorDatas4IT8620E(ref pFanSpeeds);
                        break;
                    case MyChip.IT8728F:
                        this.RetrieveFanHardwareMonitorDatas4IT8728F(ref pFanSpeeds);
                        break;
                }
            }
            catch
            {
                throw;
            }
        }

        protected void RetrieveFanHardwareMonitorDatas4IT8728F(ref List<float> pFanSpeeds)
        {
            if (pFanSpeeds.Count > 0)
                pFanSpeeds.Clear();
            if (this.m_iteMgr == null)
                return;
            if (this.m_iteMgr.chip != MyChip.IT8728F)
                return;
            try
            {
                List<float> pHardwareMonitorDatas = new List<float>();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                if (pHardwareMonitorDatas.Count == 0)
                    return;
                for (int virtualIndex = 0; virtualIndex < this.m_virtual2PhysicalFanTachometerTable.Count; ++virtualIndex)
                {
                    int index = this.m_virtual2PhysicalFanTachometerTable.Virtual2PhysicalIndex(virtualIndex);
                    float num = pHardwareMonitorDatas[index];
                    pFanSpeeds.Add(num);
                }
                if (this.m_iteMgr.chip != MyChip.IT8728F || this.FanCount <= 3)
                    return;
                for (int index = 3; index < this.FanCount; ++index)
                {
                    float num = pHardwareMonitorDatas[index];
                    pFanSpeeds.Add(num);
                }
            }
            catch
            {
                throw;
            }
        }

        protected void RetrieveFanHardwareMonitorDatas4IT8620E(ref List<float> pFanSpeeds)
        {
            if (pFanSpeeds.Count > 0)
                pFanSpeeds.Clear();
            if (this.m_iteMgr == null)
                return;
            if (this.m_iteMgr.chip != MyChip.IT8620E)
                return;
            try
            {
                List<float> pHardwareMonitorDatas = new List<float>();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                if (pHardwareMonitorDatas.Count == 0)
                    return;
                for (int virtualIndex = 0; virtualIndex < this.m_virtual2PhysicalFanTachometerTable.Count; ++virtualIndex)
                {
                    int index = this.m_virtual2PhysicalFanTachometerTable.Virtual2PhysicalIndex(virtualIndex);
                    float num = pHardwareMonitorDatas[index];
                    pFanSpeeds.Add(num);
                }
            }
            catch
            {
                throw;
            }
        }

        protected virtual void RetrieveTemperatureDisplayName(
          int temperatureIndex,
          out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (this.m_temperatureInputTable.Count == 0 || temperatureIndex >= this.m_temperatureInputTable.Count)
                return;
            int index = temperatureIndex;
            if (temperatureIndex == 1 && this.m_temperatureInputTable[temperatureIndex] == MyFanTemperatureInput.Unknown)
                ++index;
            pDisplayName = this.m_temperatureInputTable[index].ToString();
        }

        private void InitObjects(MyAmdMotherboardHealthIdentification healthNumber)
        {
            if (this.m_mbHealthIdentifier == null)
                this.m_mbHealthIdentifier = new MyAmdMotherboardHealthIdentifier();
            this.HealthNumber = this.m_mbHealthIdentifier.IsSupport(healthNumber) ? healthNumber : throw new NotSupportedException();
            this.InitFanObjects(this.HealthNumber);
            this.InitFanMappingTable(this.HealthNumber);
            this.InitFanTachometerTable(this.HealthNumber);
            this.InitTemperatureMappingTable(this.HealthNumber);
            this.InitFanRelativeVariables(this.HealthNumber);
        }

        private void WriteProtectValue2it8620(
          int fanControlIndex,
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                int fanControlIndex1 = fanControlIndex;
                if (this.UsingExtendedTemperature)
                {
                    MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
                    MySmartGuardianFanConfig originalFanConfig = smartGuardianFanConfigs[slopeIndex];
                    this.RetrieveProtectValueForExtendedTemperature(ref originalFanConfig, ref guardianFanConfig);
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref guardianFanConfig);
                }
                else
                {
                    MySmartGuardianFanConfig fanConfig = smartGuardianFanConfigs[slopeIndex];
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref fanConfig);
                }
            }
            catch
            {
                throw;
            }
        }

        private void WriteProtectValue2it8620(
          int fanControlIndex,
          ref MySmartGuardianFanConfig smartGuardianFanConfig)
        {
            try
            {
                int fanControlIndex1 = fanControlIndex;
                if (this.UsingExtendedTemperature)
                {
                    MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
                    this.RetrieveProtectValueForExtendedTemperature(ref smartGuardianFanConfig, ref guardianFanConfig);
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref guardianFanConfig);
                }
                else
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref smartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }
    }
}
