﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EasyFunctions.DataStructure.SmiData
// Assembly: Gigabyte.EasyFunctions, Version=7.2.0.24, Culture=neutral, PublicKeyToken=null
// MVID: 78B279F9-3DF2-44F7-BFAE-CD74B7F9DD3F
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EasyFunctions.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MySmiData
    {
        public string Signature { get; set; }

        public bool Supported { get; set; }

        public int Port { get; set; }

        public uint BufferPhysicalAddress { get; set; }
    }
}
