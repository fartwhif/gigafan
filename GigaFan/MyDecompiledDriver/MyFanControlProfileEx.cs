﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.FanControlProfileEx
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Collections.Generic;
using System.IO;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyFanControlProfileEx
    {
        private const string STR_PROFILE_FOLDER_NAME = "Profile";
        private const string STR_FAN_PROFILE_EXTENSION_NAME = "xml";
        private const string STR_FAN_PROFILE_PREFIX_NAME = "FanConfig";

        public int FanControlCount { get; set; }

        public bool IsProfileExist()
        {
            bool flag = false;
            if (this.FanControlCount == 0)
                throw new IndexOutOfRangeException();
            for (int fanControlIndex = 0; fanControlIndex < this.FanControlCount; ++fanControlIndex)
            {
                flag = this.IsProfileExist(fanControlIndex);
                if (!flag)
                    break;
            }
            return flag;
        }

        public bool IsProfileExist(int fanControlIndex)
        {
            string pProfileFilePath = string.Empty;
            if (this.FanControlCount == 0)
                throw new IndexOutOfRangeException();
            if (fanControlIndex >= this.FanControlCount)
                throw new IndexOutOfRangeException();
            this.RetrieveProfileFilePath(fanControlIndex, out pProfileFilePath);
            return File.Exists(pProfileFilePath);
        }

        public void CreateNewProfile(
          int fanControlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            this.CreateNewProfile(fanControlIndex, false, string.Empty, MyFanTemperatureInput.Unknown, ref smartGuardianFanConfigs);
        }

        public void CreateNewProfile(
          int fanControlIndex,
          bool bSwHardwareMonitor,
          string fanControlTitle,
          MyFanTemperatureInput temperatureInputSelection,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (this.FanControlCount == 0)
                throw new ArgumentOutOfRangeException();
            if (fanControlIndex > this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            if (smartGuardianFanConfigs.Count == 0)
                throw new ArgumentException();
            try
            {
                MySmartFanConfig pFanConfig = new MySmartFanConfig();
                pFanConfig.Title = fanControlTitle;
                pFanConfig.TemperatureInput = temperatureInputSelection;
                pFanConfig.Mode = MyFanConfigMode.SmartFanMode;
                pFanConfig.SwHardwareMonitor = bSwHardwareMonitor;
                pFanConfig.SmartModeConfig = new MySmartGuardianFanConfigCollection();
                pFanConfig.FixedModeConfig = new MySmartGuardianFanConfig();
                foreach (MySmartGuardianFanConfig guardianFanConfig in (List<MySmartGuardianFanConfig>)smartGuardianFanConfigs)
                    pFanConfig.SmartModeConfig.Add(guardianFanConfig);
                MySmartGuardianFanConfig guardianFanConfig1 = smartGuardianFanConfigs[0];
                pFanConfig.FixedModeConfig.TemperatureLimitValueOfFanOff = (byte)0;
                pFanConfig.FixedModeConfig.TemperatureLimitValueOfFanStart = (byte)70;
                pFanConfig.FixedModeConfig.TemperatureLimitValueOfFanFullSpeed = (byte)72;
                pFanConfig.FixedModeConfig.StartPWM = guardianFanConfig1.StartPWM;
                pFanConfig.FixedModeConfig.Slope = (double)guardianFanConfig1.Slope == 0.0 ? 0.0f : 1f;
                pFanConfig.FixedModeConfig.DeltaTemperature = guardianFanConfig1.DeltaTemperature;
                pFanConfig.FixedModeConfig.TargetZoneBoundary = guardianFanConfig1.TargetZoneBoundary;
                this.WriteFanConfig(fanControlIndex, ref pFanConfig);
            }
            catch
            {
                throw;
            }
        }

        public void GetLastWriteTime(int fanControlIndex, out DateTime lastWriteTime)
        {
            string pProfileFilePath = string.Empty;
            this.RetrieveProfileFilePath(fanControlIndex, out pProfileFilePath);
            lastWriteTime = File.Exists(pProfileFilePath) ? File.GetLastWriteTime(pProfileFilePath) : throw new FileNotFoundException();
        }

        public void WriteFanConfig(int fanControlIndex, ref MySmartFanConfig pFanConfig)
        {
            string pProfileFilePath = string.Empty;
            try
            {
                this.RetrieveProfileFilePath(fanControlIndex, out pProfileFilePath);
                new MySmartFanConfigFile(pProfileFilePath).Write(ref pFanConfig);
            }
            catch
            {
                throw;
            }
        }

        public void ReadFanConfig(int fanControlIndex, ref MySmartFanConfig pFanConfig)
        {
            string pProfileFilePath = string.Empty;
            try
            {
                this.RetrieveProfileFilePath(fanControlIndex, out pProfileFilePath);
                new MySmartFanConfigFile(pProfileFilePath).Read(ref pFanConfig);
            }
            catch
            {
                throw;
            }
        }

        private void RetrieveProfileFilePath(int fanControlIndex, out string pProfileFilePath)
        {
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            string empty3 = string.Empty;
            pProfileFilePath = string.Empty;
            string str = Path.Combine(MyWorkingDirectory.GetCurrentDirectory(true), "Profile");
            if (!Directory.Exists(str))
            {
                Directory.CreateDirectory(str).Attributes = FileAttributes.Hidden | FileAttributes.Directory;
                Console.WriteLine("The directory was created successfully at {0}.", (object)Directory.GetCreationTime(str));
            }
            string path2 = string.Format("{0}{1}.{2}", (object)"FanConfig", (object)fanControlIndex, (object)"xml");
            pProfileFilePath = Path.Combine(str, path2);
        }

        private void RetrieveProfileDirectoryPath(out string pProfilePath)
        {
            string empty = string.Empty;
            pProfilePath = string.Empty;
            string currentDirectory = MyWorkingDirectory.GetCurrentDirectory(true);
            pProfilePath = Path.Combine(currentDirectory, "Profile");
        }
    }
}
