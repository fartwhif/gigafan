﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.VirtualToPhysicalIndexTable
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System.Collections.Generic;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyVirtualToPhysicalIndexTable
    {
        private List<int> m_virtual2physicalTable;

        public MyVirtualToPhysicalIndexTable()
        {
            if (this.m_virtual2physicalTable != null)
                return;
            this.m_virtual2physicalTable = new List<int>();
        }

        public int Count => this.m_virtual2physicalTable.Count;

        public void Add(int physicalIndex)
        {
            if (this.m_virtual2physicalTable.Count > 0 && this.m_virtual2physicalTable.IndexOf(physicalIndex) != -1)
                return;
            this.m_virtual2physicalTable.Add(physicalIndex);
        }

        public void Clear()
        {
            if (this.m_virtual2physicalTable.Count <= 0)
                return;
            this.m_virtual2physicalTable.Clear();
        }

        public int Virtual2PhysicalIndex(int virtualIndex)
        {
            int num = -1;
            return virtualIndex >= this.m_virtual2physicalTable.Count ? num : this.m_virtual2physicalTable[virtualIndex];
        }

        public int Physical2VirtualIndex(int physicalIndex)
        {
            int num = -1;
            int index = this.m_virtual2physicalTable.IndexOf(physicalIndex);
            return index == -1 ? num : this.m_virtual2physicalTable[index];
        }
    }
}
