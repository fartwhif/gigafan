﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EasyFunctions.BdrStructure
// Assembly: Gigabyte.EasyFunctions, Version=7.2.0.24, Culture=neutral, PublicKeyToken=null
// MVID: 78B279F9-3DF2-44F7-BFAE-CD74B7F9DD3F
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EasyFunctions.dll


using System;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class BdrStructure
    {
        private const int MAX_LEN_EASY_FUNCTION_HEADER = 32;
        private const string STR_BDR_SIGNATURE = "$BDR";
        private const int MAX_LEN_BDR_SIGNATURE = 4;

        public void GetMotherboardInfo(out string pModel, out string pBiosVersion)
        {
            pModel = string.Empty;
            pBiosVersion = string.Empty;
            this.RetrieveMotherboardInfo(out pModel, out pBiosVersion);
        }

        private unsafe bool RetrieveSmiPort(ref int pVal)
        {
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            long num1 = 0;
            bool flag1 = false;
            pVal = 0;
            long pVal1 = 0;
            bool flag2 = this.RetrieveFlashPhysicalAddress(out pVal1);
            if (!flag2)
                return flag2;
            try
            {
                ulong ptr = MyNativeYccMethods.MapMemEx(pVal1, 2097151UL);
                if (ptr == 0UL)
                    return flag2;
                byte* numPtr1 = (byte*)ptr;
                int num2 = 2097151;
                string str = "$BDR";
                for (int index1 = 0; index1 < num2; ++index1)
                {
                    byte* numPtr2 = numPtr1 + index1;
                    string empty3 = string.Empty;
                    if (*numPtr2 == (byte)36)
                    {
                        for (int index2 = 0; index2 < 4; ++index2)
                        {
                            empty3 += (string)(object)Convert.ToChar(*numPtr2);
                            ++numPtr2;
                        }
                        if (str.CompareTo(empty3) == 0)
                        {
                            num1 = (long)numPtr2[57] | (long)((int)numPtr2[58] << 8);
                            flag1 = true;
                            break;
                        }
                    }
                }
                MyNativeYccMethods.UnMapMemEx(ptr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            if (!flag1)
                return flag2;
            pVal = (int)num1;
            return flag2;
        }

        private unsafe bool RetrieveMotherboardInfo(out string pModel, out string pBiosVersion)
        {
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            string empty3 = string.Empty;
            string empty4 = string.Empty;
            string empty5 = string.Empty;
            bool flag1 = false;
            pModel = string.Empty;
            pBiosVersion = string.Empty;
            long pVal = 0;
            bool flag2 = this.RetrieveFlashPhysicalAddress(out pVal);
            if (!flag2)
                return flag2;
            try
            {
                ulong ptr = MyNativeYccMethods.MapMemEx(pVal, 1048575UL);
                if (ptr == 0UL)
                    return flag2;
                byte* numPtr1 = (byte*)ptr;
                int num = 1048575;
                string str1 = "$BDR";
                for (int index1 = 0; index1 < num; ++index1)
                {
                    byte* numPtr2 = numPtr1 + index1;
                    string empty6 = string.Empty;
                    if (*numPtr2 == (byte)36)
                    {
                        for (int index2 = 0; index2 < 4; ++index2)
                        {
                            empty6 += (string)(object)Convert.ToChar(*numPtr2);
                            ++numPtr2;
                        }
                        if (str1.CompareTo(empty6) == 0)
                        {
                            flag1 = true;
                            byte* numPtr3 = numPtr2 - 4;
                            byte* numPtr4 = numPtr3 + 64;
                            for (int index3 = 0; index3 < 32; ++index3)
                            {
                                string str2 = string.Empty + (object)Convert.ToChar(numPtr4[index3]);
                                if (!(str2 == "\0"))
                                    empty3 += str2;
                                else
                                    break;
                            }
                            byte* numPtr5 = numPtr3 + 40;
                            for (int index4 = 0; index4 < 4; ++index4)
                            {
                                string str3 = string.Empty + (object)Convert.ToChar(numPtr5[index4]);
                                if (!(str3 == "\0"))
                                    empty4 += str3;
                                else
                                    break;
                            }
                            break;
                        }
                    }
                }
                MyNativeYccMethods.UnMapMemEx(ptr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            if (!flag1)
                return flag2;
            bool flag3 = true;
            pModel = empty3;
            pBiosVersion = empty4;
            return flag3;
        }

        private unsafe bool RetrieveFlashPhysicalAddress(out long pVal)
        {
            bool flag = false;
            IntPtr zero = IntPtr.Zero;
            long num1 = 0;
            try
            {
                pVal = 0L;
                ulong ptr = MyNativeYccMethods.MapMemEx(4294967280L, 16UL);
                if (ptr == 0UL)
                    return flag;
                byte* numPtr = (byte*)ptr;
                long num2 = (num1 = (long)numPtr[15] << 24) | (num1 = (long)numPtr[14] << 16) | (num1 = (long)numPtr[13] << 8) | (long)numPtr[12];
                MyNativeYccMethods.UnMapMemEx(ptr);
                pVal = num2;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                pVal = 0L;
                throw;
            }
        }
    }
}
