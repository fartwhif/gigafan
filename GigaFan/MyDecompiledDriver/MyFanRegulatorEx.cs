﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.Intel.FanRegulatorEx
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Collections.Generic;
using System.Threading;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyFanRegulatorEx : MyFanCooler
    {
        protected MyITH2RamSmartFan m_h2ramMgr;
        protected MyIT87XX m_ite879xMgr;
        protected List<MyFanTemperatureInput> m_temperatureInputTable;
        protected MyVirtualToPhysicalIndexTable m_virtual2PhysicalFanControlTable;

        public MyFanRegulatorEx()
        {
            this.IsSupported = false;
            this.H2RamFlag = false;
            this.FanControlCount = 0;
            this.FanCount = 0;
            this.TemperatureCount = 0;
            this.UsingExtendedTemperature = false;
            this.MaxSlopeCount = 3;
            this.InitObjects();
            this.InitObjectsInternal();
        }

        public bool H2RamFlag { get; protected set; }

        public override void SetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.SetProtectValue(fanControlIndex, 0, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            if (smartGuardianFanConfigs.Count == 0)
                throw new ArgumentOutOfRangeException();
            if (slopeIndex >= smartGuardianFanConfigs.Count)
                throw new ArgumentOutOfRangeException();
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                        this.m_h2ramMgr.SetFanSmartGuardianConfig(fanControlIndex, ref smartGuardianFanConfigs);
                    else
                        this.WriteProtectValue2it8728(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), slopeIndex, ref smartGuardianFanConfigs);
                }
                else
                    this.WriteProtectValue2it8728(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), slopeIndex, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.SetProtectValue(fanControlIndex, 0, bCalibrateMode, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                    {
                        int fanCtrlIndex = fanControlIndex;
                        if (bCalibrateMode)
                            this.m_h2ramMgr.ResetCalibrationPwm();
                        else
                            this.m_h2ramMgr.SetFanSmartGuardianConfig(fanCtrlIndex, ref smartGuardianFanConfigs);
                    }
                    else
                        this.WriteProtectValue2it8728(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), slopeIndex, ref smartGuardianFanConfigs);
                }
                else
                    this.WriteProtectValue2it8728(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), slopeIndex, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void GetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            bool flag = false;
            MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                    {
                        this.m_h2ramMgr.GetFanSmartGuardianConfig(fanControlIndex, this.MaxSlopeCount, ref pSmartGuardianFanConfigs);
                    }
                    else
                    {
                        this.m_iteMgr.GetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), ref guardianFanConfig);
                        flag = true;
                    }
                }
                else
                {
                    this.m_iteMgr.GetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref guardianFanConfig);
                    flag = true;
                }
                if (!flag)
                    return;
                this.RetrieveProtectValue(ref guardianFanConfig, this.MaxSlopeCount, ref pSmartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfig smartGuardianFanConfig)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                    {
                        int fanCtrlIndex = fanControlIndex;
                        MySmartGuardianFanConfigCollection smartGuardianFanConfigs = new MySmartGuardianFanConfigCollection();
                        smartGuardianFanConfigs.Add(smartGuardianFanConfig);
                        this.m_h2ramMgr.SetFanSmartGuardianConfig(fanCtrlIndex, ref smartGuardianFanConfigs);
                    }
                    else
                        this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), ref smartGuardianFanConfig);
                }
                else
                    this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref smartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void GetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfig pSmartGuardianFanConfig)
        {
            MySmartGuardianFanConfig fanConfig = (MySmartGuardianFanConfig)null;
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            pSmartGuardianFanConfig.DeltaTemperature = (byte)0;
            pSmartGuardianFanConfig.FanSpeed = 0;
            pSmartGuardianFanConfig.Slope = 0.0f;
            pSmartGuardianFanConfig.StartPWM = (byte)0;
            pSmartGuardianFanConfig.TargetZoneBoundary = (byte)0;
            pSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)0;
            pSmartGuardianFanConfig.TemperatureLimitValueOfFanOff = (byte)0;
            pSmartGuardianFanConfig.TemperatureLimitValueOfFanStart = (byte)0;
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                    {
                        int fanCtrlIndex = fanControlIndex;
                        MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs = new MySmartGuardianFanConfigCollection();
                        this.m_h2ramMgr.GetFanSmartGuardianConfig(fanCtrlIndex, 1, ref pSmartGuardianFanConfigs);
                        if (pSmartGuardianFanConfigs.Count > 0)
                            fanConfig = pSmartGuardianFanConfigs[0];
                    }
                    else
                    {
                        fanConfig = new MySmartGuardianFanConfig();
                        this.m_iteMgr.GetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), ref fanConfig);
                    }
                }
                else
                {
                    int fanControlIndex1 = this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex);
                    fanConfig = new MySmartGuardianFanConfig();
                    this.m_iteMgr.GetFanSmartGuardianConfig(fanControlIndex1, ref fanConfig);
                }
                if (fanConfig == null)
                    return;
                pSmartGuardianFanConfig.DeltaTemperature = fanConfig.DeltaTemperature;
                pSmartGuardianFanConfig.FanSpeed = fanConfig.FanSpeed;
                pSmartGuardianFanConfig.Slope = fanConfig.Slope;
                pSmartGuardianFanConfig.StartPWM = fanConfig.StartPWM;
                pSmartGuardianFanConfig.TargetZoneBoundary = fanConfig.TargetZoneBoundary;
                pSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed = fanConfig.TemperatureLimitValueOfFanFullSpeed;
                pSmartGuardianFanConfig.TemperatureLimitValueOfFanOff = fanConfig.TemperatureLimitValueOfFanOff;
                pSmartGuardianFanConfig.TemperatureLimitValueOfFanStart = fanConfig.TemperatureLimitValueOfFanStart;
            }
            catch
            {
                throw;
            }
        }

        public override void GetCpuFanSpeed(out float fanSpeed)
        {
            try
            {
                fanSpeed = 0.0f;
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanSpeed(int fanIndex, out float fanSpeed)
        {
            try
            {
                switch (this.m_iteMgr.chip)
                {
                    case MyChip.IT8620E:
                        this.RetrieveFanSpeed4it8620(fanIndex, out fanSpeed);
                        break;
                    default:
                        this.RetrieveFanSpeed4it8728(fanIndex, out fanSpeed);
                        break;
                }
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanSpeed(int fanIndex, bool bSwitchFanIndex, out float fanSpeed)
        {
            try
            {
                switch (this.m_iteMgr.chip)
                {
                    case MyChip.IT8620E:
                        this.GetFanSpeed(fanIndex, out fanSpeed);
                        break;
                    default:
                        int fanIndex1 = fanIndex;
                        if (this.H2RamFlag)
                        {
                            if (bSwitchFanIndex && fanIndex == 5)
                                fanIndex1 = 6;
                            this.GetFanSpeed(fanIndex1, out fanSpeed);
                            break;
                        }
                        if (bSwitchFanIndex)
                        {
                            switch (fanIndex)
                            {
                                case 0:
                                    fanIndex1 = 4;
                                    break;
                                case 2:
                                    fanIndex1 = 3;
                                    break;
                            }
                            this.GetFanSpeed(fanIndex1, out fanSpeed);
                            break;
                        }
                        this.GetFanSpeed(fanIndex1, out fanSpeed);
                        break;
                }
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanDisplayName(int fanIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (!this.IsSupported)
                throw new NotSupportedException();
            int fanIndex1 = fanIndex;
            if (this.H2RamFlag)
            {
                this.RetrieveFanDisplayName4H2RamFlag(fanIndex1, out pDisplayName);
            }
            else
            {
                switch (this.m_iteMgr.chip)
                {
                    case MyChip.IT8620E:
                        this.RetrieveFanDisplayName4it8620(fanIndex1, out pDisplayName);
                        break;
                    default:
                        this.RetrieveFanDisplayName4it8728(fanIndex1, out pDisplayName);
                        break;
                }
            }
        }

        public override void GetFanControlDisplayName(int fanControlIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (!this.IsSupported)
                throw new NotSupportedException();
            int fanControlIndex1 = fanControlIndex < this.FanControlCount ? fanControlIndex : throw new ArgumentOutOfRangeException();
            if (this.H2RamFlag)
            {
                this.RetrieveFanControlDisplayName4H2RamFlag(fanControlIndex1, out pDisplayName);
            }
            else
            {
                switch (this.m_iteMgr.chip)
                {
                    case MyChip.IT8620E:
                        this.RetrieveFanControlDisplayName4it8620(fanControlIndex, out pDisplayName);
                        break;
                    default:
                        this.RetrieveFanControlDisplayName(fanControlIndex, out pDisplayName);
                        break;
                }
            }
        }

        public override void SetCalibrationPwm(int fanControlIndex, byte pwmPercentage)
        {
            MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
            try
            {
                float maxPwmValue = (float)this.m_iteMgr.MaxPwmValue;
                float d = (float)pwmPercentage * (maxPwmValue / 100f);
                float num1 = 50f;
                float num2 = 80f;
                float num3 = (float)(((double)this.m_iteMgr.MaxPwmValue - (double)d) / ((double)num2 - (double)num1));
                fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                fanConfig.TemperatureLimitValueOfFanStart = (byte)num1;
                fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)num2;
                fanConfig.StartPWM = (byte)Math.Floor((double)d);
                fanConfig.Slope = num3;
                fanConfig.DeltaTemperature = (byte)3;
                fanConfig.TargetZoneBoundary = (byte)0;
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                        this.m_h2ramMgr.SetCalibrationPwm((int)fanConfig.StartPWM);
                    else
                        this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), ref fanConfig);
                }
                else
                    this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void ResetCalibrationPwm()
        {
            try
            {
                if (!this.H2RamFlag)
                    return;
                this.m_h2ramMgr.ResetCalibrationPwm();
            }
            catch
            {
                throw;
            }
        }

        public override void SetFanSpeedFixedMode(int fanControlIndex, byte pwmPercentage)
        {
            int fanCtrlIndex = 0;
            MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
            try
            {
                float maxPwmValue = (float)this.m_iteMgr.MaxPwmValue;
                float d = (float)pwmPercentage * (maxPwmValue / 100f);
                float num1 = 70f;
                float num2 = 71f;
                float num3 = (float)(((double)this.m_iteMgr.MaxPwmValue - (double)d) / ((double)num2 - (double)num1));
                fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                fanConfig.TemperatureLimitValueOfFanStart = (byte)num1;
                fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)num2;
                fanConfig.StartPWM = (byte)Math.Floor((double)d);
                fanConfig.Slope = (double)num3 == 0.0 ? 0.0f : 1f;
                fanConfig.DeltaTemperature = (byte)3;
                fanConfig.TargetZoneBoundary = (byte)0;
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                        this.m_h2ramMgr.SetFanSmartGuardianConfig(fanCtrlIndex, (int)fanConfig.StartPWM);
                    else
                        this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), ref fanConfig);
                }
                else
                    this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override bool IsRequireSoftwareMonitor(int fanControlIndex)
        {
            return !this.H2RamFlag || fanControlIndex >= this.m_h2ramMgr.FanControlRegisterCount;
        }

        public override void GetTemperature(int temperatureIndex, out float targetTemperature)
        {
            targetTemperature = 0.0f;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (temperatureIndex >= this.m_iteMgr.TemperatureCount)
                throw new IndexOutOfRangeException();
            List<float> pHardwareMonitorDatas = new List<float>();
            try
            {
                if (this.H2RamFlag)
                {
                    if (temperatureIndex == 2)
                    {
                        if (this.m_ite879xMgr != null)
                        {
                            this.m_ite879xMgr.Update();
                            this.m_ite879xMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        }
                        else
                        {
                            this.m_h2ramMgr.Update();
                            this.m_h2ramMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        }
                        if (pHardwareMonitorDatas.Count <= 0)
                            return;
                        targetTemperature = pHardwareMonitorDatas[0];
                    }
                    else
                    {
                        int index = temperatureIndex;
                        this.m_iteMgr.Update();
                        this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        if (index >= pHardwareMonitorDatas.Count)
                            return;
                        targetTemperature = pHardwareMonitorDatas[index];
                    }
                }
                else
                {
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    if (temperatureIndex >= pHardwareMonitorDatas.Count)
                        return;
                    targetTemperature = pHardwareMonitorDatas[temperatureIndex];
                }
            }
            catch
            {
                throw;
            }
        }

        public override void GetTemperatureInputSelection(
          int fanControlIndex,
          ref MyFanTemperatureInput pTemperatureInputSelection)
        {
            pTemperatureInputSelection = MyFanTemperatureInput.Unknown;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                int num = fanControlIndex;
                if (this.H2RamFlag)
                {
                    int fanCtrlIndex = fanControlIndex;
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                        this.m_h2ramMgr.GetTemperatureInputSelection(fanCtrlIndex, ref pTemperatureInputSelection);
                    else
                        this.m_iteMgr.GetTemperatureInputSelection(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), ref pTemperatureInputSelection);
                }
                else if (this.TemperatureCount == 2)
                {
                    if (num == 0)
                        pTemperatureInputSelection = MyFanTemperatureInput.CPU;
                    else
                        pTemperatureInputSelection = MyFanTemperatureInput.SYSTEM;
                }
                else
                    this.m_iteMgr.GetTemperatureInputSelection(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref pTemperatureInputSelection);
            }
            catch
            {
                throw;
            }
        }

        public override void GetTemperatureDisplayName(int temperatureIndex, out string pDisplayName)
        {
            pDisplayName = "Temperature";
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (temperatureIndex >= this.TemperatureCount)
                throw new ArgumentOutOfRangeException();
            this.RetrieveTemperatureDisplayName(temperatureIndex, out pDisplayName);
        }

        public override void GetHardwareMonitorDatas(
          ref List<float> pTemperatureDatas,
          ref List<float> pFanSpeedDatas)
        {
            if (!this.IsSupported)
                throw new NotSupportedException();
            try
            {
                this.RetrieveTemperatureHardwareMonitorDatas(ref pTemperatureDatas);
                this.RetrieveFanHardwareMonitorDatas(ref pFanSpeedDatas);
            }
            catch
            {
            }
        }

        protected virtual void RetrieveTemperatureHardwareMonitorDatas(ref List<float> pTemperatureDatas)
        {
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (pTemperatureDatas.Count > 0)
                pTemperatureDatas.Clear();
            try
            {
                float single = Convert.ToSingle((byte)128);
                List<float> pHardwareMonitorDatas = new List<float>();
                if (this.H2RamFlag)
                {
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    for (int index = 0; index < pHardwareMonitorDatas.Count - 1; ++index)
                    {
                        if (index != 1 || this.m_temperatureInputTable[index] != MyFanTemperatureInput.Unknown)
                            pTemperatureDatas.Add(pHardwareMonitorDatas[index]);
                    }
                    if (this.m_ite879xMgr != null)
                    {
                        this.m_ite879xMgr.Update();
                        this.m_ite879xMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    }
                    else
                    {
                        this.m_h2ramMgr.Update();
                        this.m_h2ramMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    }
                    if (pHardwareMonitorDatas.Count <= 0)
                        return;
                    pTemperatureDatas.Add(pHardwareMonitorDatas[0]);
                }
                else
                {
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    if (this.TemperatureCount == 3)
                    {
                        for (int index = 0; index < pHardwareMonitorDatas.Count; ++index)
                            pTemperatureDatas.Add(pHardwareMonitorDatas[index]);
                    }
                    else
                    {
                        float num1 = pHardwareMonitorDatas[1];
                        float num2 = pHardwareMonitorDatas[2];
                        if ((double)num1 != (double)single && (double)num2 != (double)single)
                        {
                            for (int index = 0; index < pHardwareMonitorDatas.Count; ++index)
                            {
                                if (this.m_temperatureInputTable[index] != MyFanTemperatureInput.Unknown)
                                    pTemperatureDatas.Add(pHardwareMonitorDatas[index]);
                            }
                        }
                        else
                        {
                            for (int index = 0; index < pHardwareMonitorDatas.Count; ++index)
                            {
                                if (index == 1 && this.m_temperatureInputTable[index] == MyFanTemperatureInput.Unknown)
                                {
                                    if ((double)pHardwareMonitorDatas[index] != (double)single)
                                    {
                                        pTemperatureDatas.Add(pHardwareMonitorDatas[index]);
                                        break;
                                    }
                                }
                                else
                                    pTemperatureDatas.Add(pHardwareMonitorDatas[index]);
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        protected virtual void RetrieveFanHardwareMonitorDatas(ref List<float> pFanSpeeds)
        {
            if (this.m_iteMgr.FanTachometerCount == 0)
                return;
            if (pFanSpeeds.Count > 0)
                pFanSpeeds.Clear();
            try
            {
                List<float> pHardwareMonitorDatas = new List<float>();
                if (this.H2RamFlag)
                {
                    if (this.m_ite879xMgr != null)
                    {
                        this.m_ite879xMgr.Update();
                        this.m_ite879xMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    }
                    else
                    {
                        this.m_h2ramMgr.Update();
                        this.m_h2ramMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    }
                    for (int index = 0; index < pHardwareMonitorDatas.Count; ++index)
                        pFanSpeeds.Add(pHardwareMonitorDatas[index]);
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                }
                else
                {
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                }
                if (pHardwareMonitorDatas.Count <= 0)
                    return;
                for (int index = 0; index < this.m_iteMgr.FanTachometerCount; ++index)
                {
                    float num = pHardwareMonitorDatas[index];
                    pFanSpeeds.Add(num);
                }
            }
            catch
            {
                throw;
            }
        }

        protected virtual void RetrieveTemperatureDisplayName(
          int temperatureIndex,
          out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (this.m_temperatureInputTable.Count == 0 || temperatureIndex >= this.m_temperatureInputTable.Count)
                return;
            int index = temperatureIndex;
            if (temperatureIndex == 1 && this.m_temperatureInputTable[temperatureIndex] == MyFanTemperatureInput.Unknown)
                ++index;
            pDisplayName = this.m_temperatureInputTable[index].ToString();
        }

        private void InitObjects()
        {
            try
            {
                MyIT87XXFinder it87XxFinder = new MyIT87XXFinder();
                for (int index = 0; index < 6; ++index)
                {
                    it87XxFinder.Find(true, out this.m_iteMgr);
                    if (this.m_iteMgr == null)
                        Thread.Sleep(1000);
                    else
                        break;
                }
                for (int index = 0; index < 2; ++index)
                {
                    it87XxFinder.Find(out this.m_h2ramMgr);
                    if (this.m_h2ramMgr == null)
                        Thread.Sleep(1000);
                    else
                        break;
                }
                if (this.m_iteMgr == null)
                    throw new NotSupportedException();
                if (this.m_h2ramMgr != null)
                    this.m_ite879xMgr = (MyIT87XX)new MyIT8790(this.m_h2ramMgr.chip, this.m_h2ramMgr.BaseAddress, this.m_h2ramMgr.GpioAddress, this.m_h2ramMgr.Version);
                for (byte fanControlIndex = 0; (int)fanControlIndex < this.m_iteMgr.FanControlRegisterCount; ++fanControlIndex)
                {
                    bool pbEnable = false;
                    this.m_iteMgr.GetFanTachometerEnableStatus((int)fanControlIndex, out pbEnable);
                    if (pbEnable)
                        ++this.FanControlCount;
                }
                this.FanControlCount = this.m_iteMgr.ActualFanControlRegisterCount;
                this.IsSupported = true;
                this.MaxPwmValue = this.m_iteMgr.MaxPwmValue;
                this.FanCount += this.m_iteMgr.FanTachometerCount;
                if (this.m_h2ramMgr != null)
                {
                    this.FanControlCount += this.m_h2ramMgr.FanControlRegisterCount;
                    this.H2RamFlag = true;
                    this.FanCount += 3;
                    ++this.TemperatureCount;
                }
                this.TemperatureCount += this.m_h2ramMgr != null ? 2 : 3;
            }
            catch
            {
                throw;
            }
        }

        private void InitObjectsInternal()
        {
            int pTemperatureInputCount = 0;
            new MyFanRelatedWinRegistry().ReadTemperatureInputCount(ref pTemperatureInputCount);
            if (pTemperatureInputCount == 0)
                pTemperatureInputCount = 3;
            this.TemperatureCount = pTemperatureInputCount;
            this.InitTemperatureMappingTable(pTemperatureInputCount);
            this.InitFanMappingTable();
        }

        private void InitTemperatureMappingTable(int nTemperatureInputCount)
        {
            if (this.m_temperatureInputTable == null)
                this.m_temperatureInputTable = new List<MyFanTemperatureInput>();
            if (this.m_temperatureInputTable.Count > 0)
                this.m_temperatureInputTable.Clear();
            switch (nTemperatureInputCount)
            {
                case 2:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.Unknown);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    break;
                default:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.PCH);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    break;
            }
        }

        private void InitFanMappingTable()
        {
            if (this.m_iteMgr == null)
                return;
            switch (this.m_iteMgr.chip)
            {
                case MyChip.IT8620E:
                    this.InitFanMappingTable4it8620(this.m_iteMgr.ActualFanControlRegisterCount);
                    break;
                case MyChip.IT8728F:
                    this.InitFanMappingTable4it8728(this.m_iteMgr.ActualFanControlRegisterCount);
                    break;
            }
        }

        private void InitFanMappingTable4it8728(int actualFanControlRegisterCount)
        {
            if (this.m_virtual2PhysicalFanControlTable == null)
                this.m_virtual2PhysicalFanControlTable = new MyVirtualToPhysicalIndexTable();
            if (this.m_virtual2PhysicalFanControlTable.Count > 0)
                this.m_virtual2PhysicalFanControlTable.Clear();
            switch (actualFanControlRegisterCount)
            {
                case 1:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    break;
                case 2:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    break;
                default:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    break;
            }
        }

        private void InitFanMappingTable4it8620(int actualFanControlRegisterCount)
        {
            if (this.m_virtual2PhysicalFanControlTable == null)
                this.m_virtual2PhysicalFanControlTable = new MyVirtualToPhysicalIndexTable();
            if (this.m_virtual2PhysicalFanControlTable.Count > 0)
                this.m_virtual2PhysicalFanControlTable.Clear();
            switch (actualFanControlRegisterCount)
            {
                case 1:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    break;
                case 2:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    break;
                case 3:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    break;
                case 4:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(3);
                    break;
                default:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(4);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(3);
                    break;
            }
        }

        private void RetrieveFanDisplayName4H2RamFlag(int fanIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (!this.H2RamFlag)
                return;
            if (fanIndex < this.m_h2ramMgr.FanTachometerCount)
            {
                switch (fanIndex)
                {
                    case 0:
                        pDisplayName = "CPU Fan";
                        break;
                    case 1:
                        pDisplayName = "CPU OPT";
                        break;
                    case 2:
                        pDisplayName = "System Fan 1";
                        break;
                }
            }
            else
            {
                int num = fanIndex - 1;
                pDisplayName = "System Fan " + num.ToString();
            }
        }

        private void RetrieveFanDisplayName4it8728(int fanIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (this.H2RamFlag)
                return;
            if (fanIndex == 0)
                pDisplayName = "CPU Fan";
            else if (fanIndex == 4)
            {
                pDisplayName = "CPU OPT";
            }
            else
            {
                int num = fanIndex;
                pDisplayName = "System Fan " + num.ToString();
            }
        }

        private void RetrieveFanDisplayName4it8620(int fanIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (this.H2RamFlag)
                return;
            if (this.m_iteMgr.FanTachometerCount == 5)
            {
                switch (fanIndex)
                {
                    case 0:
                        pDisplayName = "CPU Fan";
                        break;
                    case 1:
                        pDisplayName = "CPU OPT";
                        break;
                    default:
                        int num1 = fanIndex - 1;
                        pDisplayName = string.Format("System Fan {0}", (object)num1);
                        break;
                }
            }
            else if (fanIndex == 0)
            {
                pDisplayName = "CPU Fan";
            }
            else
            {
                int num2 = fanIndex;
                pDisplayName = string.Format("System Fan {0}", (object)num2);
            }
        }

        private void RetrieveFanControlDisplayName4H2RamFlag(
          int fanControlIndex,
          out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (!this.H2RamFlag)
                return;
            switch (fanControlIndex)
            {
                case 0:
                    pDisplayName = "CPU Fan";
                    break;
                case 1:
                    pDisplayName = "CPU OPT";
                    break;
                case 2:
                    pDisplayName = "System Fan 1";
                    break;
                case 3:
                    pDisplayName = "System Fan 2";
                    break;
                case 4:
                    pDisplayName = "System Fan 3";
                    break;
                case 5:
                    bool fanTac4Enable = false;
                    bool fanTac5Enable = false;
                    this.m_iteMgr.GetFanTachometer4And5EnableStatus(out fanTac4Enable, out fanTac5Enable);
                    pDisplayName = fanTac4Enable ? "System Fan 4 & 5" : "System Fan 4";
                    break;
                default:
                    pDisplayName = "System Fan";
                    break;
            }
        }

        private void RetrieveFanControlDisplayName(int fanControlIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (this.H2RamFlag)
                return;
            switch (fanControlIndex)
            {
                case 0:
                    if (this.FanControlCount == 3 && this.FanCount == 5)
                    {
                        pDisplayName = "CPU Fan & OPT";
                        break;
                    }
                    pDisplayName = "CPU Fan";
                    break;
                case 1:
                    pDisplayName = "SYSTEM Fan 1";
                    break;
                case 2:
                    if (this.FanControlCount == 3 && (this.FanCount == 5 || this.FanCount == 4))
                    {
                        pDisplayName = "System Fan 2 & 3";
                        break;
                    }
                    if (this.FanControlCount == 3)
                    {
                        pDisplayName = "System Fan 2";
                        break;
                    }
                    pDisplayName = "System Fan";
                    break;
                default:
                    pDisplayName = "System Fan";
                    break;
            }
        }

        private void RetrieveFanControlDisplayName4it8620(int fanControlIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (this.H2RamFlag)
                return;
            if (this.FanCount == 5)
            {
                switch (fanControlIndex)
                {
                    case 0:
                        pDisplayName = "CPU Fan";
                        break;
                    case 1:
                        pDisplayName = "CPU OPT";
                        break;
                    default:
                        int num = fanControlIndex - 1;
                        pDisplayName = string.Format("System Fan {0}", (object)num);
                        break;
                }
            }
            else if (fanControlIndex == 0)
                pDisplayName = "CPU Fan";
            else
                pDisplayName = string.Format("System Fan {0}", (object)fanControlIndex);
        }

        private void RetrieveFanSpeed4it8728(int fanIndex, out float fanSpeed)
        {
            try
            {
                fanSpeed = 0.0f;
                int index = fanIndex;
                List<float> pHardwareMonitorDatas = new List<float>();
                if (this.H2RamFlag)
                {
                    if (index < this.m_h2ramMgr.FanTachometerCount)
                    {
                        if (this.m_ite879xMgr != null)
                        {
                            this.m_ite879xMgr.Update();
                            this.m_ite879xMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        }
                        else
                        {
                            this.m_h2ramMgr.Update();
                            this.m_h2ramMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        }
                    }
                    else
                    {
                        index -= this.m_h2ramMgr.FanTachometerCount;
                        this.m_iteMgr.Update();
                        this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    }
                }
                else
                {
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                }
                if (index >= pHardwareMonitorDatas.Count)
                    return;
                fanSpeed = pHardwareMonitorDatas[index];
            }
            catch
            {
                throw;
            }
        }

        private void RetrieveFanSpeed4it8620(int fanIndex, out float fanSpeed)
        {
            int num1 = 0;
            int num2 = 0;
            num1 = 0;
            num2 = 0;
            List<float> pHardwareMonitorDatas = new List<float>();
            fanSpeed = 0.0f;
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanIndex < this.m_h2ramMgr.FanTachometerCount)
                    {
                        if (this.m_ite879xMgr != null)
                        {
                            this.m_ite879xMgr.Update();
                            this.m_ite879xMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        }
                        else
                        {
                            this.m_h2ramMgr.Update();
                            this.m_h2ramMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        }
                        if (fanIndex >= pHardwareMonitorDatas.Count)
                            return;
                        fanSpeed = pHardwareMonitorDatas[fanIndex];
                    }
                    else
                    {
                        int index = this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanIndex - this.m_h2ramMgr.FanTachometerCount);
                        this.m_iteMgr.Update();
                        this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        if (index >= pHardwareMonitorDatas.Count)
                            return;
                        fanSpeed = pHardwareMonitorDatas[index];
                    }
                }
                else
                {
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    int index = this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanIndex);
                    if (index >= pHardwareMonitorDatas.Count)
                        return;
                    fanSpeed = pHardwareMonitorDatas[index];
                }
            }
            catch
            {
                throw;
            }
        }

        private void WriteProtectValue2it8728(
          int fanControlIndex,
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                int fanControlIndex1 = fanControlIndex;
                if (this.UsingExtendedTemperature)
                {
                    MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
                    MySmartGuardianFanConfig originalFanConfig = smartGuardianFanConfigs[slopeIndex];
                    this.RetrieveProtectValueForExtendedTemperature(ref originalFanConfig, ref guardianFanConfig);
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref guardianFanConfig);
                }
                else
                {
                    MySmartGuardianFanConfig fanConfig = smartGuardianFanConfigs[slopeIndex];
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref fanConfig);
                }
            }
            catch
            {
                throw;
            }
        }

        private void WriteProtectValue2it8620(
          int fanControlIndex,
          ref MySmartGuardianFanConfig smartGuardianFanConfig)
        {
            try
            {
                int fanControlIndex1 = fanControlIndex;
                if (this.UsingExtendedTemperature)
                {
                    MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
                    this.RetrieveProtectValueForExtendedTemperature(ref smartGuardianFanConfig, ref guardianFanConfig);
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref guardianFanConfig);
                }
                else
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref smartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }
    }
}
