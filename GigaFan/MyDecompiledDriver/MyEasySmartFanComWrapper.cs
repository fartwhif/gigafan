﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EasyFunctions.EasySmartFanComWrapper
// Assembly: Gigabyte.EasyFunctions, Version=7.2.0.24, Culture=neutral, PublicKeyToken=null
// MVID: 78B279F9-3DF2-44F7-BFAE-CD74B7F9DD3F
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EasyFunctions.dll

using System;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MyEasySmartFanComWrapper : IMyEasySmartFan
    {
        private MyEasySmartFanGroupId m_groupMgr;

        public bool Supported { get; set; }

        public void GetGroupNumber(out uint pGroupNumber)
        {
            try
            {
                if (this.m_groupMgr == null)
                    this.m_groupMgr = new MyEasySmartFanGroupId();
                this.m_groupMgr.GetGroupNumber(out pGroupNumber);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
