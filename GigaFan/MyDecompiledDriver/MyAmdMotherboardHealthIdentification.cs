﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.AmdMotherboardHealthIdentification
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public enum MyAmdMotherboardHealthIdentification
    {
        MHID_Unknown = 0,
        MHID_B001 = 45057, // 0x0000B001
        MHID_B003 = 45058, // 0x0000B002
        MHID_B002 = 45059, // 0x0000B003
    }
}
