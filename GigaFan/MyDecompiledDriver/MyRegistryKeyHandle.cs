﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.WinRegistry.RegistryKeyHandle
// Assembly: Gigabyte, Version=7.2.0.22, Culture=neutral, PublicKeyToken=null
// MVID: FE603752-DF9C-4680-B734-106AEDD3F213
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.dll

using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.Linq;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MyRegistryKeyHandle
    {
        public MyRegistryKeyHandle() => this.View = RegistryView.Default;

        public MyRegistryKeyHandle(RegistryView view) => this.View = view;

        public RegistryView View { get; set; }

        public bool IsValueNameExist(RegistryHive hKey, string subKey, string valueName)
        {
            bool flag = false;
            if (subKey.Length == 0)
                return flag;
            if (valueName.Length == 0)
                return flag;
            try
            {
                string[] valueNames = RegistryKey.OpenBaseKey(hKey, this.View).OpenSubKey(subKey).GetValueNames();
                if (((IEnumerable<string>)valueNames).Count<string>() != 0)
                {
                    foreach (string strA in valueNames)
                    {
                        if (string.Compare(strA, valueName, true) == 0)
                        {
                            flag = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return flag;
        }

        public bool IsSubkeyExist(RegistryHive hKey, string subKey)
        {
            bool flag = false;
            if (subKey.Length == 0)
                return flag;
            try
            {
                return RegistryKey.OpenBaseKey(hKey, this.View).OpenSubKey(subKey) != null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void DeleteValue(RegistryHive hKey, string subKey, string valueName)
        {
            if (subKey.Length == 0)
                return;
            if (valueName.Length == 0)
                return;
            try
            {
                RegistryKey.OpenBaseKey(hKey, this.View).CreateSubKey(subKey).DeleteValue(valueName);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void DeleteSubKey(RegistryHive hKey, string subKey)
        {
            if (subKey.Length == 0)
                return;
            try
            {
                RegistryKey.OpenBaseKey(hKey, this.View).DeleteSubKeyTree(subKey);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void CreateSubKey(RegistryHive hKey, string subKey)
        {
            if (subKey.Length == 0)
                return;
            try
            {
                RegistryKey.OpenBaseKey(hKey, this.View).CreateSubKey(subKey);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ReadStringValue(
          RegistryHive hKey,
          string subKey,
          string valueName,
          out string pValueText)
        {
            pValueText = string.Empty;
            if (subKey.Length == 0)
                return;
            if (valueName.Length == 0)
                return;
            try
            {
                RegistryKey registryKey = RegistryKey.OpenBaseKey(hKey, this.View).OpenSubKey(subKey);
                bool flag = false;
                string[] valueNames = registryKey.GetValueNames();
                if (((IEnumerable<string>)valueNames).Count<string>() != 0)
                {
                    foreach (string strA in valueNames)
                    {
                        if (string.Compare(strA, valueName, true) == 0)
                        {
                            flag = true;
                            break;
                        }
                    }
                }
                if (!flag)
                    return;
                pValueText = (string)registryKey.GetValue(valueName);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void WriteStringValue(
          RegistryHive hKey,
          string subKey,
          string valueName,
          string valueText)
        {
            if (subKey.Length == 0)
                return;
            if (valueName.Length == 0)
                return;
            try
            {
                RegistryKey.OpenBaseKey(hKey, this.View).CreateSubKey(subKey).SetValue(valueName, (object)valueText, RegistryValueKind.String);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ReadDwordValue(
          RegistryHive hKey,
          string subKey,
          string valueName,
          out uint pDwordValue)
        {
            pDwordValue = 0U;
            if (subKey.Length == 0)
                return;
            if (valueName.Length == 0)
                return;
            try
            {
                RegistryKey registryKey = RegistryKey.OpenBaseKey(hKey, this.View).OpenSubKey(subKey);
                bool flag = false;
                string[] valueNames = registryKey.GetValueNames();
                if (((IEnumerable<string>)valueNames).Count<string>() != 0)
                {
                    foreach (string strA in valueNames)
                    {
                        if (string.Compare(strA, valueName, true) == 0)
                        {
                            flag = true;
                            break;
                        }
                    }
                }
                if (!flag)
                    return;
                pDwordValue = uint.Parse(registryKey.GetValue(valueName, (object)0).ToString());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void WriteDwordValue(
          RegistryHive hKey,
          string subKey,
          string valueName,
          uint dwordValue)
        {
            if (subKey.Length == 0)
                return;
            if (valueName.Length == 0)
                return;
            try
            {
                RegistryKey.OpenBaseKey(hKey, this.View).CreateSubKey(subKey).SetValue(valueName, (object)dwordValue, RegistryValueKind.DWord);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
