﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.Intel.MotherboardHealthIdentifier
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyMotherboardHealthIdentifier : MyMotherboardHealthIdentifierBase
    {
        private const string STR_MODEL_G1_SNIPER_B6 = "G1.Sniper B6";

        public MyMotherboardHealthIdentifier() => this.Vendor = MyProcessorVendors.Intel;

        public void GetGroupNumber(out MyMotherboardHealthIdentification pGroupNumber)
        {
            uint pGroupNumber1 = 0;
            pGroupNumber = MyMotherboardHealthIdentification.MHID_Unknown;
            try
            {
                this.RetrieveGroupNumber(out pGroupNumber1);
                if (pGroupNumber1 > 0U)
                {
                    if (this.IsSupport(pGroupNumber1))
                    {
                        MyMotherboardHealthIdentification healthid = (MyMotherboardHealthIdentification)pGroupNumber1;
                        this.CheckSupport(healthid);
                        pGroupNumber = healthid;
                    }
                }
            }
            catch
            {
                pGroupNumber = MyMotherboardHealthIdentification.MHID_Unknown;
            }
            if (pGroupNumber != MyMotherboardHealthIdentification.MHID_Unknown)
                return;
            try
            {
                if (this.m_motherboardInfo == null)
                    this.m_motherboardInfo = new MyMotherboard();
                this.RetrieveGroupNumber(this.m_motherboardInfo.Model, out pGroupNumber);
            }
            catch
            {
                pGroupNumber = MyMotherboardHealthIdentification.MHID_Unknown;
            }
        }

        public override bool IsSupport(uint groupNumber)
        {
            try
            {
                return this.CheckSupport((MyMotherboardHealthIdentification)groupNumber);
            }
            catch
            {
                return false;
            }
        }

        public bool IsSupport(MyMotherboardHealthIdentification groupNumber)
        {
            return this.CheckSupport(groupNumber);
        }

        private void RetrieveGroupNumber(
          string motherboardModel,
          out MyMotherboardHealthIdentification pGroupNumber)
        {
            pGroupNumber = MyMotherboardHealthIdentification.MHID_Unknown;
            if (string.IsNullOrEmpty(motherboardModel) || string.Compare(motherboardModel, "G1.Sniper B6", true) != 0)
                return;
            pGroupNumber = MyMotherboardHealthIdentification.MHID_F001;
        }

        private bool CheckSupport(MyMotherboardHealthIdentification healthid)
        {
            bool flag;
            switch (healthid)
            {
                case MyMotherboardHealthIdentification.MHID_A001:
                case MyMotherboardHealthIdentification.MHID_A002:
                case MyMotherboardHealthIdentification.MHID_A003:
                case MyMotherboardHealthIdentification.MHID_B001:
                case MyMotherboardHealthIdentification.MHID_B002:
                case MyMotherboardHealthIdentification.MHID_B003:
                case MyMotherboardHealthIdentification.MHID_C001:
                case MyMotherboardHealthIdentification.MHID_C002:
                case MyMotherboardHealthIdentification.MHID_C003:
                case MyMotherboardHealthIdentification.MHID_D001:
                    flag = true;
                    break;
                case MyMotherboardHealthIdentification.MHID_F001:
                    flag = true;
                    break;
                default:
                    flag = false;
                    break;
            }
            return flag;
        }
    }
}
