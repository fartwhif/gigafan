﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EasyFunctions.IEasySmartFan
// Assembly: Gigabyte.EasyFunctions, Version=7.2.0.24, Culture=neutral, PublicKeyToken=null
// MVID: 78B279F9-3DF2-44F7-BFAE-CD74B7F9DD3F
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EasyFunctions.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal interface IMyEasySmartFan
    {
        bool Supported { get; set; }

        void GetGroupNumber(out uint pGroupNumber);
    }
}
