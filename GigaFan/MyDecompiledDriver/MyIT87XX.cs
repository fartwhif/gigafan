﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.IT87XX
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Collections.Generic;
using System.Reflection;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal abstract class MyIT87XX
    {
        protected const int VAL_FAN_CONTROL_1 = 0;
        protected const int VAL_FAN_CONTROL_2 = 1;
        protected const int VAL_FAN_CONTROL_3 = 2;
        protected const int VAL_FAN_CONTROL_4 = 3;
        protected const int VAL_FAN_CONTROL_5 = 4;
        protected const int VAL_FAN_CONTROL_6 = 5;
        protected const byte ITE_VENDOR_ID = 144;
        protected const byte VAL_REGISTER_DEFAULT_UNKNOW = 128;
        protected const byte ADDRESS_REGISTER_OFFSET = 5;
        protected const byte DATA_REGISTER_OFFSET = 6;
        protected const byte CONFIGURATION_REGISTER = 0;
        protected const byte FAN_TACHOMETER_DIVISOR_REGISTER = 11;
        protected const byte FAN_PWM_SMOOTHING_STEP_FREQUENCY_SELECTION_REGISTER = 11;
        protected const byte FAN_TACHOMETER_CONTROL_REGISTER = 12;
        protected const byte FAN_CONTROLLER_MAIN_CONTROL_REGISTER = 19;
        protected const byte VENDOR_ID_REGISTER = 88;
        protected List<int> m_pwmControlRegisters;
        protected List<MySmartGuardianAutomaticModeRegister> m_smartGuardianAutomaticModeRegisters;
        protected List<int> m_voltageReadingRegisters;
        protected List<int> m_temperatureReadingRegisters;
        protected List<int> m_fanTachometerReadingLsbRegisters;
        protected List<int> m_fanTachometerReadingMsbRegisters;
        protected List<float> m_fanSpeeds;
        protected List<float> m_voltages;
        protected List<float> m_temperatures;
        protected List<float> m_controlValues;

        public MyChip chip { get; protected set; }

        public byte Version { get; protected set; }

        public int MaxPwmValue { get; protected set; }

        public int FanControlRegisterCount { get; protected set; }

        public int ActualFanControlRegisterCount { get; protected set; }

        public int FanTachometerCount { get; protected set; }

        public int VoltageCount { get; protected set; }

        public int TemperatureCount { get; protected set; }

        public int GpioCount { get; protected set; }

        public ushort BaseAddress { get; protected set; }

        public ushort AddressRegister { get; protected set; }

        public ushort DataRegister { get; protected set; }

        public ushort GpioAddress { get; protected set; }

        public float VoltageGain { get; protected set; }

        public bool Has16bitFanCounter { get; protected set; }

        public virtual void SetControl(int fanControlIndex, int newValue)
        {
            if (fanControlIndex < 0 || fanControlIndex >= this.m_pwmControlRegisters.Count)
                throw new ArgumentOutOfRangeException();
            byte num = Convert.ToByte(newValue >> 1);
            this.WriteByte(Convert.ToByte(this.m_pwmControlRegisters[fanControlIndex]), num);
        }

        public virtual void Update()
        {
            this.UpdateVoltageDatas();
            this.UpdateTemperatureDatas();
            this.UpdateFanSpeedDatas();
            this.UpdateControlDatas();
        }

        public virtual void SetStartPWM(int fanControlIndex, byte pwmValue)
        {
            try
            {
                this.UpdateStartPWM(fanControlIndex, (int)pwmValue);
            }
            catch
            {
                throw;
            }
        }

        public virtual void GetFanSmartGuardianConfig(
          int fanControlIndex,
          ref MySmartGuardianFanConfig fanConfig)
        {
            try
            {
                this.RetrieveFanSmartGuardianConfig(fanControlIndex, ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public virtual void SetFanSmartGuardianConfig(
          int fanControlIndex,
          ref MySmartGuardianFanConfig fanConfig)
        {
            try
            {
                this.UpdateFanSmartGuardianConfig(fanControlIndex, ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public virtual void GetFanTachometerEnableStatus(int fanControlIndex, out bool pbEnable)
        {
            bool fanTac1Enable = false;
            bool fanTac2Enable = false;
            bool fanTac3Enable = false;
            bool fanTac4Enable = false;
            bool fanTac5Enable = false;
            try
            {
                pbEnable = false;
                if (fanControlIndex < 3)
                    this.RetrieveFanTachometerEnableStatus(out fanTac1Enable, out fanTac2Enable, out fanTac3Enable);
                else
                    this.GetFanTachometer4And5EnableStatus(out fanTac4Enable, out fanTac5Enable);
                switch (fanControlIndex)
                {
                    case 0:
                        pbEnable = fanTac1Enable;
                        break;
                    case 1:
                        pbEnable = fanTac2Enable;
                        break;
                    case 2:
                        pbEnable = fanTac3Enable;
                        break;
                    case 3:
                        pbEnable = fanTac4Enable;
                        break;
                    case 4:
                        pbEnable = fanTac5Enable;
                        break;
                    default:
                        pbEnable = false;
                        break;
                }
            }
            catch
            {
                throw;
            }
        }

        public virtual void GetFanTachometerEnableStatus(
          out bool fanTac1Enable,
          out bool fanTac2Enable,
          out bool fanTac3Enable)
        {
            try
            {
                this.RetrieveFanTachometerEnableStatus(out fanTac1Enable, out fanTac2Enable, out fanTac3Enable);
            }
            catch
            {
                throw;
            }
        }

        public virtual void EnableFanTachometer(
          bool fanTac1Enable,
          bool fanTac2Enable,
          bool fanTac3Enable)
        {
            try
            {
                this.UpdateFanTachometerEnableStatus(fanTac1Enable, fanTac2Enable, fanTac3Enable);
            }
            catch
            {
                throw;
            }
        }

        public virtual void GetFanTachometer4And5EnableStatus(
          out bool fanTac4Enable,
          out bool fanTac5Enable)
        {
            try
            {
                this.RetrieveFanTachometer4And5EnableStatus(out fanTac4Enable, out fanTac5Enable);
            }
            catch
            {
                throw;
            }
        }

        public virtual void EnableFanTachometer4And5(bool fanTac4Enable, bool fanTac5Enable)
        {
            try
            {
                this.UpdateFanTachometer4And5EnableStatus(fanTac4Enable, fanTac5Enable);
            }
            catch
            {
                throw;
            }
        }

        public virtual void GetTemperatureInputSelection(
          int fanControlIndex,
          ref MyFanTemperatureInput pTemperatureInputSelection)
        {
            this.GetTemperatureInputSelection(fanControlIndex, false, ref pTemperatureInputSelection);
        }

        public virtual void GetTemperatureInputSelection(
          int fanControlIndex,
          bool bIgnoreActualFanControlRegisterCount,
          ref MyFanTemperatureInput pTemperatureInputSelection)
        {
            bool valid = false;
            int fanControlIndex1 = 0;
            pTemperatureInputSelection = MyFanTemperatureInput.Unknown;
            if (!bIgnoreActualFanControlRegisterCount && fanControlIndex >= this.ActualFanControlRegisterCount)
                return;
            if (fanControlIndex < this.FanControlRegisterCount)
            {
                this.RetrieveTemperatureInputSelection(fanControlIndex, bIgnoreActualFanControlRegisterCount, ref pTemperatureInputSelection);
            }
            else
            {
                pTemperatureInputSelection = MyFanTemperatureInput.SYSTEM;
                byte num = this.ReadByte((byte)11, out valid);
                if (!valid)
                    return;
                switch (fanControlIndex)
                {
                    case 3:
                        fanControlIndex1 = (int)num & 3;
                        break;
                    case 4:
                        fanControlIndex1 = ((int)num & 12) >> 2;
                        break;
                }
                this.RetrieveTemperatureInputSelection(fanControlIndex1, bIgnoreActualFanControlRegisterCount, ref pTemperatureInputSelection);
            }
        }

        public virtual void GetFanControlOutputModeSelection(
          int fanControlIndex,
          out MyFanControlOutputModeSelection modeSelection)
        {
            MyFanControlOutputModeSelection fanControlOutputMode1 = MyFanControlOutputModeSelection.SmartGuardianMode;
            MyFanControlOutputModeSelection fanControlOutputMode2 = MyFanControlOutputModeSelection.SmartGuardianMode;
            MyFanControlOutputModeSelection fanControlOutputMode3 = MyFanControlOutputModeSelection.SmartGuardianMode;
            modeSelection = MyFanControlOutputModeSelection.SmartGuardianMode;
            try
            {
                this.GetFanControlOutputModeSelection(out fanControlOutputMode1, out fanControlOutputMode2, out fanControlOutputMode3);
                switch (fanControlIndex)
                {
                    case 0:
                        modeSelection = fanControlOutputMode1;
                        break;
                    case 1:
                        modeSelection = fanControlOutputMode2;
                        break;
                    case 2:
                        modeSelection = fanControlOutputMode3;
                        break;
                }
            }
            catch
            {
                modeSelection = MyFanControlOutputModeSelection.SmartGuardianMode;
            }
        }

        public virtual void GetFanControlOutputModeSelection(
          out MyFanControlOutputModeSelection fanControlOutputMode1,
          out MyFanControlOutputModeSelection fanControlOutputMode2,
          out MyFanControlOutputModeSelection fanControlOutputMode3)
        {
            bool valid = false;
            fanControlOutputMode1 = MyFanControlOutputModeSelection.SmartGuardianMode;
            fanControlOutputMode2 = MyFanControlOutputModeSelection.SmartGuardianMode;
            fanControlOutputMode3 = MyFanControlOutputModeSelection.SmartGuardianMode;
            try
            {
                int num1 = (int)this.ReadByte((byte)19, out valid);
                int num2 = num1 & 1;
                fanControlOutputMode1 = num2 == 1 ? MyFanControlOutputModeSelection.SmartGuardianMode : MyFanControlOutputModeSelection.OnOffMode;
                int num3 = num1 >> 1 & 1;
                fanControlOutputMode2 = num3 == 1 ? MyFanControlOutputModeSelection.SmartGuardianMode : MyFanControlOutputModeSelection.OnOffMode;
                int num4 = num1 >> 2 & 1;
                fanControlOutputMode3 = num4 == 1 ? MyFanControlOutputModeSelection.SmartGuardianMode : MyFanControlOutputModeSelection.OnOffMode;
            }
            catch
            {
                fanControlOutputMode1 = MyFanControlOutputModeSelection.SmartGuardianMode;
                fanControlOutputMode2 = MyFanControlOutputModeSelection.SmartGuardianMode;
                fanControlOutputMode3 = MyFanControlOutputModeSelection.SmartGuardianMode;
            }
        }

        public void GetHardwareMonitorDatas(
          ref List<float> pFanSpeedDatas,
          ref List<float> pVoltageDatas,
          ref List<float> pTemperatureDatas)
        {
            if (this.m_fanSpeeds == null || this.m_voltages == null || this.m_temperatures == null)
                return;
            if (pFanSpeedDatas != null && this.m_fanSpeeds.Count > 0)
            {
                if (pFanSpeedDatas.Count > 0)
                    pFanSpeedDatas.Clear();
                foreach (float fanSpeed in this.m_fanSpeeds)
                    pFanSpeedDatas.Add(fanSpeed);
            }
            if (pVoltageDatas != null && this.m_voltages.Count > 0)
            {
                if (pVoltageDatas.Count > 0)
                    pVoltageDatas.Clear();
                foreach (float voltage in this.m_voltages)
                    pVoltageDatas.Add(voltage);
            }
            if (pTemperatureDatas == null || this.m_temperatures.Count <= 0)
                return;
            if (pTemperatureDatas.Count > 0)
                pTemperatureDatas.Clear();
            foreach (float temperature in this.m_temperatures)
                pTemperatureDatas.Add(temperature);
        }

        public void GetFanHardwareMonitorDatas(ref List<float> pHardwareMonitorDatas)
        {
            if (this.m_fanSpeeds == null || pHardwareMonitorDatas == null || this.m_fanSpeeds.Count <= 0)
                return;
            if (pHardwareMonitorDatas.Count > 0)
                pHardwareMonitorDatas.Clear();
            foreach (float fanSpeed in this.m_fanSpeeds)
                pHardwareMonitorDatas.Add(fanSpeed);
        }

        public void GetTemperatureHardwareMonitorDatas(ref List<float> pHardwareMonitorDatas)
        {
            if (this.m_temperatures == null || pHardwareMonitorDatas == null || this.m_temperatures.Count <= 0)
                return;
            if (pHardwareMonitorDatas.Count > 0)
                pHardwareMonitorDatas.Clear();
            foreach (float temperature in this.m_temperatures)
                pHardwareMonitorDatas.Add(temperature);
        }

        public void GetVoltageHardwareMonitorDatas(ref List<float> pHardwareMonitorDatas)
        {
            if (this.m_voltages == null || pHardwareMonitorDatas == null || this.m_voltages.Count <= 0)
                return;
            if (pHardwareMonitorDatas.Count > 0)
                pHardwareMonitorDatas.Clear();
            foreach (float voltage in this.m_voltages)
                pHardwareMonitorDatas.Add(voltage);
        }

        protected virtual void RetrieveFanSmartGuardianConfig(
          int fanControlIndex,
          ref MySmartGuardianFanConfig pSmartGuardianFanConfig)
        {
            if (fanControlIndex >= this.FanControlRegisterCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                bool valid = false;
                MySmartGuardianAutomaticModeRegister automaticModeRegister = this.m_smartGuardianAutomaticModeRegisters[fanControlIndex];
                MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig()
                {
                    TemperatureLimitValueOfFanOff = 0,
                    TemperatureLimitValueOfFanStart = 0,
                    TemperatureLimitValueOfFanFullSpeed = 0,
                    StartPWM = 0,
                    Slope = 0.0f,
                    DeltaTemperature = 0,
                    TargetZoneBoundary = 0
                };
                guardianFanConfig.TemperatureLimitValueOfFanOff = this.ReadByte(automaticModeRegister.TemperatureLimitValueOfOff, out valid);
                if (!valid)
                    return;
                guardianFanConfig.TemperatureLimitValueOfFanStart = this.ReadByte(automaticModeRegister.TemperatureLimitValueOfFanStart, out valid);
                if (!valid)
                    return;
                guardianFanConfig.TemperatureLimitValueOfFanFullSpeed = this.ReadByte(automaticModeRegister.TemperatureLimitValueOfFullSpeed, out valid);
                if (!valid)
                    return;
                guardianFanConfig.StartPWM = this.ReadByte(automaticModeRegister.StartPwm, out valid);
                if (!valid)
                    return;
                byte srcValue = this.ReadByte(automaticModeRegister.Control, out valid);
                if (!valid)
                    return;
                guardianFanConfig.Slope = this.ConvertSlopePwmToSlope(srcValue);
                guardianFanConfig.DeltaTemperature = this.ReadByte(automaticModeRegister.DeltaTemperature, out valid);
                if (!valid)
                    return;
                guardianFanConfig.TargetZoneBoundary = this.ReadByte(automaticModeRegister.TargetZone, out valid);
                if (!valid)
                    return;
                pSmartGuardianFanConfig.TemperatureLimitValueOfFanOff = guardianFanConfig.TemperatureLimitValueOfFanOff;
                pSmartGuardianFanConfig.TemperatureLimitValueOfFanStart = guardianFanConfig.TemperatureLimitValueOfFanStart;
                pSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed = guardianFanConfig.TemperatureLimitValueOfFanFullSpeed;
                pSmartGuardianFanConfig.StartPWM = guardianFanConfig.StartPWM;
                pSmartGuardianFanConfig.Slope = guardianFanConfig.Slope;
                pSmartGuardianFanConfig.DeltaTemperature = guardianFanConfig.DeltaTemperature;
                pSmartGuardianFanConfig.TargetZoneBoundary = guardianFanConfig.TargetZoneBoundary;
            }
            catch
            {
                throw;
            }
        }

        protected virtual void RetrieveTemperatureInputSelection(
          int fanControlIndex,
          ref MyFanTemperatureInput pTemperatureInputSelection)
        {
            this.RetrieveTemperatureInputSelection(fanControlIndex, false, ref pTemperatureInputSelection);
        }

        protected virtual void RetrieveTemperatureInputSelection(
          int fanControlIndex,
          bool bIgnoreActualFanControlRegisterCount,
          ref MyFanTemperatureInput pTemperatureInputSelection)
        {
            bool valid = false;
            pTemperatureInputSelection = MyFanTemperatureInput.Unknown;
            if (!bIgnoreActualFanControlRegisterCount && fanControlIndex >= this.ActualFanControlRegisterCount || fanControlIndex >= this.m_pwmControlRegisters.Count)
                return;
            byte num = this.ReadByte(Convert.ToByte(this.m_pwmControlRegisters[fanControlIndex]), out valid);
            if (!valid)
                return;
            switch ((int)num & 3)
            {
                case 0:
                    pTemperatureInputSelection = MyFanTemperatureInput.SYSTEM;
                    break;
                case 1:
                    pTemperatureInputSelection = MyFanTemperatureInput.PCH;
                    break;
                case 2:
                    pTemperatureInputSelection = MyFanTemperatureInput.CPU;
                    break;
            }
        }

        protected virtual void RetrieveFanTachometerEnableStatus(
          out bool fanTac1Enable,
          out bool fanTac2Enable,
          out bool fanTac3Enable)
        {
            bool valid = false;
            fanTac1Enable = false;
            fanTac2Enable = false;
            fanTac3Enable = false;
            try
            {
                int num = (int)this.ReadByte((byte)19, out valid);
                if ((num >> 4 & 1) == 1)
                    fanTac1Enable = true;
                if ((num >> 5 & 1) == 1)
                    fanTac2Enable = true;
                if ((num >> 6 & 1) != 1)
                    return;
                fanTac3Enable = true;
            }
            catch
            {
                throw;
            }
        }

        protected virtual void RetrieveFanTachometer4And5EnableStatus(
          out bool fanTac4Enable,
          out bool fanTac5Enable)
        {
            bool valid = false;
            fanTac4Enable = false;
            fanTac5Enable = false;
            try
            {
                int num = (int)this.ReadByte((byte)12, out valid);
                if (!valid)
                    return;
                if ((num >> 4 & 1) == 1)
                    fanTac4Enable = true;
                if ((num >> 5 & 1) != 1)
                    return;
                fanTac5Enable = true;
            }
            catch
            {
                throw;
            }
        }

        protected virtual int RetrieveFanTachometerAmount()
        {
            int num = 0;
            bool fanTac1Enable = false;
            bool fanTac2Enable = false;
            bool fanTac3Enable = false;
            bool fanTac4Enable = false;
            bool fanTac5Enable = false;
            try
            {
                this.RetrieveFanTachometerEnableStatus(out fanTac1Enable, out fanTac2Enable, out fanTac3Enable);
                this.RetrieveFanTachometer4And5EnableStatus(out fanTac4Enable, out fanTac5Enable);
                if (fanTac1Enable)
                    ++num;
                if (fanTac2Enable)
                    ++num;
                if (fanTac3Enable)
                    ++num;
                if (fanTac4Enable)
                    ++num;
                if (fanTac5Enable)
                    ++num;
            }
            catch
            {
                throw;
            }
            return num;
        }

        protected virtual void UpdateFanSmartGuardianConfig(
          int fanControlIndex,
          ref MySmartGuardianFanConfig newSmartGuardianFanConfig)
        {
            if (fanControlIndex >= this.FanControlRegisterCount)
                throw new ArgumentOutOfRangeException();
            if (this.m_smartGuardianAutomaticModeRegisters == null || this.m_smartGuardianAutomaticModeRegisters.Count == 0)
                return;
            if (fanControlIndex >= this.m_smartGuardianAutomaticModeRegisters.Count)
                throw new ArgumentOutOfRangeException();
            MySmartGuardianAutomaticModeRegister automaticModeRegister = this.m_smartGuardianAutomaticModeRegisters[fanControlIndex];
            try
            {
                MySmartGuardianFanConfig pSmartGuardianFanConfig = new MySmartGuardianFanConfig();
                this.RetrieveFanSmartGuardianConfig(fanControlIndex, ref pSmartGuardianFanConfig);
                if ((int)pSmartGuardianFanConfig.TemperatureLimitValueOfFanOff != (int)newSmartGuardianFanConfig.TemperatureLimitValueOfFanOff && !this.WriteByte(automaticModeRegister.TemperatureLimitValueOfOff, newSmartGuardianFanConfig.TemperatureLimitValueOfFanOff))
                    Console.WriteLine("[{0}] Error: Fail to write temperatureLimitValueOfFanOff.", (object)MethodBase.GetCurrentMethod().Name);
                if ((int)pSmartGuardianFanConfig.TemperatureLimitValueOfFanStart != (int)newSmartGuardianFanConfig.TemperatureLimitValueOfFanStart && !this.WriteByte(automaticModeRegister.TemperatureLimitValueOfFanStart, newSmartGuardianFanConfig.TemperatureLimitValueOfFanStart))
                    Console.WriteLine("[{0}] Error: Fail to write temperatureLimitValueOfFanStart.", (object)MethodBase.GetCurrentMethod().Name);
                if ((int)pSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed != (int)newSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed && !this.WriteByte(automaticModeRegister.TemperatureLimitValueOfFullSpeed, newSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed))
                    Console.WriteLine("[{0}] Error: Fail to write temperatureLimitValueOfFanFullSpeed.", (object)MethodBase.GetCurrentMethod().Name);
                if ((int)pSmartGuardianFanConfig.StartPWM != (int)newSmartGuardianFanConfig.StartPWM && !this.WriteByte(automaticModeRegister.StartPwm, newSmartGuardianFanConfig.StartPWM))
                    Console.WriteLine("[{0}] Error: Fail to write startPWM.", (object)MethodBase.GetCurrentMethod().Name);
                if ((double)pSmartGuardianFanConfig.Slope != (double)newSmartGuardianFanConfig.Slope)
                {
                    byte slopePwm = this.ConvertSlopeToSlopePwm(newSmartGuardianFanConfig.Slope);
                    if (!this.WriteByte(automaticModeRegister.Control, slopePwm))
                        Console.WriteLine("[{0}] Error: Fail to write slope.", (object)MethodBase.GetCurrentMethod().Name);
                }
                if ((int)pSmartGuardianFanConfig.DeltaTemperature != (int)newSmartGuardianFanConfig.DeltaTemperature && !this.WriteByte(automaticModeRegister.DeltaTemperature, newSmartGuardianFanConfig.DeltaTemperature))
                    Console.WriteLine("[{0}] Error: Fail to write deltaTemperature.", (object)MethodBase.GetCurrentMethod().Name);
                if ((int)pSmartGuardianFanConfig.TargetZoneBoundary == (int)newSmartGuardianFanConfig.TargetZoneBoundary || this.WriteByte(automaticModeRegister.TargetZone, newSmartGuardianFanConfig.TargetZoneBoundary))
                    return;
                Console.WriteLine("[{0}] Error: Fail to write targetZoneBoundary.", (object)MethodBase.GetCurrentMethod().Name);
            }
            catch
            {
                throw;
            }
        }

        protected virtual void UpdateStartPWM(int fanControlIndex, int pwmValue)
        {
            if (fanControlIndex >= this.FanControlRegisterCount)
                throw new ArgumentOutOfRangeException();
            if (this.m_smartGuardianAutomaticModeRegisters == null || this.m_smartGuardianAutomaticModeRegisters.Count == 0)
                return;
            if (fanControlIndex >= this.m_smartGuardianAutomaticModeRegisters.Count)
                throw new ArgumentOutOfRangeException();
            if (pwmValue > this.MaxPwmValue)
                pwmValue = this.MaxPwmValue;
            MySmartGuardianAutomaticModeRegister automaticModeRegister = this.m_smartGuardianAutomaticModeRegisters[fanControlIndex];
            try
            {
                byte num = Convert.ToByte(pwmValue);
                if (this.WriteByte(automaticModeRegister.StartPwm, num))
                    return;
                Console.WriteLine("[{0}] Error: Fail to write startPWM.", (object)MethodBase.GetCurrentMethod().Name);
            }
            catch
            {
                throw;
            }
        }

        protected virtual void UpdateFanTachometerEnableStatus(
          bool fanTac1Enable,
          bool fanTac2Enable,
          bool fanTac3Enable)
        {
            bool valid = false;
            try
            {
                int num1 = (int)this.ReadByte((byte)19, out valid);
                if (!valid)
                    return;
                int num2 = !fanTac1Enable ? num1 & 239 : num1 | 16;
                int num3 = !fanTac2Enable ? num2 & 223 : num2 | 32;
                if (this.WriteByte((byte)19, !fanTac3Enable ? (byte)(num3 & 191) : (byte)(num3 | 64)))
                    ;
            }
            catch
            {
                throw;
            }
        }

        protected virtual void UpdateFanTachometer4And5EnableStatus(
          bool fanTac4Enable,
          bool fanTac5Enable)
        {
            bool valid = false;
            try
            {
                int num1 = (int)this.ReadByte((byte)12, out valid);
                if (!valid)
                    return;
                int num2 = !fanTac4Enable ? num1 & 239 : num1 | 16;
                if (this.WriteByte((byte)12, !fanTac5Enable ? (byte)(num2 & 223) : (byte)(num2 | 32)))
                    ;
            }
            catch
            {
                throw;
            }
        }

        protected virtual void UpdateVoltageDatas()
        {
            bool valid = false;
            if (this.m_voltages == null)
                return;
            if (this.m_voltages.Count > 0)
                this.m_voltages.Clear();
            for (int index = 0; index < this.m_voltageReadingRegisters.Count; ++index)
            {
                float num = (float)this.ReadByte(Convert.ToByte(this.m_voltageReadingRegisters[index]), out valid);
                if (!valid)
                    this.m_voltages.Add(0.0f);
                else
                    this.m_voltages.Add(this.VoltageGain * num);
            }
        }

        protected virtual void UpdateTemperatureDatas()
        {
            bool valid = false;
            if (this.m_temperatures == null)
                return;
            if (this.m_temperatures.Count > 0)
                this.m_temperatures.Clear();
            for (int index = 0; index < this.m_temperatureReadingRegisters.Count; ++index)
            {
                sbyte num = (sbyte)this.ReadByte(Convert.ToByte(this.m_temperatureReadingRegisters[index]), out valid);
                if (valid)
                    this.m_temperatures.Add(Convert.ToSingle(num));
            }
        }

        protected virtual void UpdateFanSpeedDatas()
        {
            bool valid = false;
            if (this.m_fanSpeeds == null)
                return;
            if (this.m_fanSpeeds.Count > 0)
                this.m_fanSpeeds.Clear();
            if (this.Has16bitFanCounter)
            {
                for (int index = 0; index < this.m_fanTachometerReadingLsbRegisters.Count; ++index)
                {
                    int num1 = (int)this.ReadByte(Convert.ToByte(this.m_fanTachometerReadingLsbRegisters[index]), out valid);
                    if (valid)
                    {
                        int num2 = num1 | (int)this.ReadByte(Convert.ToByte(this.m_fanTachometerReadingMsbRegisters[index]), out valid) << 8;
                        if (valid)
                            this.m_fanSpeeds.Add(num2 <= 63 ? 0.0f : (num2 < (int)ushort.MaxValue ? 1350000f / (float)(num2 * 2) : 0.0f));
                    }
                }
            }
            else
            {
                for (int index = 0; index < this.m_fanTachometerReadingLsbRegisters.Count; ++index)
                {
                    int num3 = (int)this.ReadByte(Convert.ToByte(this.m_fanTachometerReadingLsbRegisters[index]), out valid);
                    if (valid)
                    {
                        int num4 = 2;
                        if (index < 2)
                        {
                            int num5 = (int)this.ReadByte((byte)11, out valid);
                            if (valid)
                                num4 = 1 << (num5 >> 3 * index & 7);
                            else
                                continue;
                        }
                        this.m_fanSpeeds.Add(num3 <= 0 ? 0.0f : (num3 < (int)byte.MaxValue ? 1350000f / (float)(num3 * num4) : 0.0f));
                    }
                }
            }
        }

        protected virtual void UpdateControlDatas()
        {
            bool valid = false;
            if (this.m_controlValues == null)
                return;
            if (this.m_controlValues.Count > 0)
                this.m_controlValues.Clear();
            for (int index = 0; index < this.m_pwmControlRegisters.Count; ++index)
            {
                byte num = this.ReadByte(Convert.ToByte(this.m_pwmControlRegisters[index]), out valid);
                if (valid)
                    this.m_controlValues.Add(((int)num & 128) <= 0 ? (float)Math.Round((double)((int)num & (int)sbyte.MaxValue) * 100.0 / (double)sbyte.MaxValue) : 0.0f);
            }
        }

        protected float ConvertSlopePwmToSlope(byte srcValue)
        {
            float num1 = (float)((int)srcValue >> 3);
            float num2 = (float)((int)srcValue & 7);
            return (double)num2 == 0.0 ? num1 : num1 + num2 / 8f;
        }

        protected byte ConvertSlopeToSlopePwm(float srcValue)
        {
            float num1 = 0.0f;
            float num2 = 0.0f;
            byte slopePwm = 0;
            if ((double)srcValue == 0.0)
                return slopePwm;
            int num3 = (int)Math.Floor((double)srcValue);
            if (num3 > 0)
                num1 = (float)(num3 << 3);
            float num4 = srcValue - (float)num3;
            if ((double)num4 > 0.0)
                num2 = num4 * 8f;
            return (byte)Math.Floor((double)num1 + (double)num2);
        }

        protected byte ReadByte(byte register, out bool valid)
        {
            MyNativeYccMethods.gb_outp((uint)this.AddressRegister, register);
            byte num = MyNativeYccMethods.gb_inp((uint)this.DataRegister);
            valid = (int)register == (int)MyNativeYccMethods.gb_inp((uint)this.AddressRegister);
            return num;
        }

        protected bool WriteByte(byte register, byte value)
        {
            MyNativeYccMethods.gb_outp((uint)this.AddressRegister, register);
            MyNativeYccMethods.gb_outp((uint)this.DataRegister, value);
            return (int)register == (int)MyNativeYccMethods.gb_inp((uint)this.AddressRegister);
        }
    }
}
