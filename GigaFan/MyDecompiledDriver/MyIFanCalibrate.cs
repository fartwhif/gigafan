﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.IFanCalibrate
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public interface MyIFanCalibrate
    {
        void SetCalibrationPwm(int fanCtrlIndex, byte pwmPercentage);

        void ResetCalibrationPwm();

        void SetFanSpeedFixedMode(int fanCtrlIndex, byte pwmPercentage);

        void ConvertPwm2Percetage(int dutyCycle, out int dutyCyclePercetage);

        void ConvertPercetage2Pwm(int dutyCyclePercetage, out int dutyCycle);

        void EnableCalibrateStatus(bool bEnable);

        bool IsEnableCalibrateStatus();
    }
}
