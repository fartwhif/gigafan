﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.SystemManagement.WorkingDirectory
// Assembly: Gigabyte, Version=7.2.0.22, Culture=neutral, PublicKeyToken=null
// MVID: FE603752-DF9C-4680-B734-106AEDD3F213
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.dll

using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MyWorkingDirectory
    {
        public MyWorkingDirectory() => this.InitObjects(false);

        public MyWorkingDirectory(bool bFromReferencedDLL) => this.InitObjects(bFromReferencedDLL);

        public string OriginalDirectory { get; protected set; }

        public string ExeDirectory { get; protected set; }

        public string ExeFilePath { get; protected set; }

        public void Change2ExeDirectory() => Directory.SetCurrentDirectory(this.ExeDirectory);

        public void Change2OriginalDirectory() => Directory.SetCurrentDirectory(this.OriginalDirectory);

        public static void ChangeToExeDirectory()
        {
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            string empty3 = string.Empty;
            Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName));
        }

        public static string GetCurrentDirectory() => Directory.GetCurrentDirectory();

        public static string GetCurrentDirectory(bool bFromReferencedDLL)
        {
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            string empty3 = string.Empty;
            Directory.GetCurrentDirectory();
            return Path.GetDirectoryName(!bFromReferencedDLL ? Process.GetCurrentProcess().MainModule.FileName : Assembly.GetExecutingAssembly().Location);
        }

        public static void ChangeCurrentDirectory(string path)
        {
            if (!Directory.Exists(path))
                return;
            Directory.SetCurrentDirectory(path);
        }

        private void InitObjects(bool bFromReferencedDLL)
        {
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            string empty3 = string.Empty;
            string currentDirectory = Directory.GetCurrentDirectory();
            string path = !bFromReferencedDLL ? Process.GetCurrentProcess().MainModule.FileName : Assembly.GetExecutingAssembly().Location;
            string directoryName = Path.GetDirectoryName(path);
            this.OriginalDirectory = currentDirectory;
            this.ExeDirectory = directoryName;
            this.ExeFilePath = path;
        }
    }
}
