﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.NativeAPI.NativeYccMethods
// Assembly: Gigabyte, Version=7.2.0.22, Culture=neutral, PublicKeyToken=null
// MVID: FE603752-DF9C-4680-B734-106AEDD3F213
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.dll

using System;
using System.Runtime.InteropServices;
using System.Security;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    [SuppressUnmanagedCodeSecurity]
    [Serializable]
    public class MyNativeYccMethods
    {
        [NonSerialized]
        public const string ModuleName = "ycc.dll";

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr MapMem(long Address, ulong Length);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool UnMapMem(ref IntPtr ptr);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern ulong MapMemEx(long Address, ulong Length);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool UnMapMemEx(ulong ptr);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool SMICmd(
          ref uint eax,
          ref uint ebx,
          ref uint ecx,
          ref uint edx,
          ref uint esi,
          ref uint edi,
          ref uint flag);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint ReadMSR(uint idx, out ulong data);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint WriteMSR(uint idx, ref ulong data);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void gb_outp(uint Port, byte DataValue);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void gb_outpw(uint Port, ushort DataValue);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void gb_outpd(uint Port, ulong DataValue);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte gb_inp(uint Port);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern ushort gb_inpw(uint Port);

        [DllImport("ycc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint gb_inpd(uint Port);
    }
}
