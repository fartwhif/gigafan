﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.MotherboardHealthIdentifierMask
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal static class MyMotherboardHealthIdentifierMask
    {
        public static uint ToHealthNumber(uint healthID)
        {
            uint num = 0;
            return healthID == 0U ? num : 65295U & healthID;
        }

        public static uint ToLedNumber(uint healthID)
        {
            uint num = 0;
            return healthID == 0U ? num : 240U & healthID;
        }
    }
}
