﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.Xml.XmlBaseFile
// Assembly: Gigabyte, Version=7.2.0.22, Culture=neutral, PublicKeyToken=null
// MVID: FE603752-DF9C-4680-B734-106AEDD3F213
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.dll

using System.IO;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MyXmlBaseFile
    {
        public string FilePath { get; set; }

        public bool Exists => !string.IsNullOrEmpty(this.FilePath) && File.Exists(this.FilePath);

        public void Delete()
        {
            if (this.FilePath.Length == 0 || !File.Exists(this.FilePath))
                return;
            File.Delete(this.FilePath);
        }
    }
}
