﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.IT87XXFinder
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Threading;


namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyIT87XXFinder : MySuperIO
    {
        protected const byte CONFIGURATION_CONTROL_REGISTER = 2;
        protected const byte CHIP_ID_REGISTER = 32;
        protected const byte BASE_ADDRESS_REGISTER = 96;
        protected const byte EXTENDED2_MULTI_FUNCTION_PIN_SELECTION_REGISTER = 44;
        protected const byte IT87_ENVIRONMENT_CONTROLLER_LDN = 4;
        protected const byte IT87_GPIO_LDN = 7;
        protected const byte IT87_CHIP_VERSION_REGISTER = 34;
        protected const byte IT87_SHARE_MEMORY_FLASH_INTERFACE_CONFIGURATION_REGISTER = 15;
        protected const byte IT87_BRAM_CONFIGURATION_REGISTER_LDN = 16;
        protected const byte IT87_BRAM_ACTIVATE = 48;
        protected const byte IT87_BRAM_BASE_ADDRESS_MSB_REGISTER = 96;
        protected const byte IT87_BRAM_BASE_ADDRESS_LSB_REGISTER = 97;
        protected readonly ushort[] REGISTER_PORTS = new ushort[2]
        {
      (ushort) 46,
      (ushort) 78
        };
        protected readonly ushort[] VALUE_PORTS = new ushort[2]
        {
      (ushort) 47,
      (ushort) 79
        };
        private object m_findLock;

        public MyIT87XXFinder()
        {
            this.ChipID = MyChip.Unknown;
            this.RegisterPort = this.REGISTER_PORTS[0];
            this.ValuePort = this.VALUE_PORTS[0];
            if (this.m_findLock != null)
                return;
            this.m_findLock = new object();
        }

        public MyChip ChipID { get; protected set; }

        public ushort RegisterPort { get; protected set; }

        public ushort ValuePort { get; protected set; }

        public bool Find(bool bMainController, out MyIT87XX pEnvironmentController)
        {
            bool flag = false;
            pEnvironmentController = (MyIT87XX)null;
            lock (this.m_findLock)
            {
                try
                {
                    this.ChipID = MyChip.Unknown;
                    if (bMainController)
                        this.FindMainEnvironmentController(out pEnvironmentController);
                    else
                        this.FindSecondaryEnvironmentController(out pEnvironmentController);
                    flag = pEnvironmentController != null;
                }
                catch
                {
                    pEnvironmentController = (MyIT87XX)null;
                }
            }
            return flag;
        }

        public bool Find(out MyITH2RamSmartFan pEnvironmentController)
        {
            bool flag = false;
            pEnvironmentController = (MyITH2RamSmartFan)null;
            lock (this.m_findLock)
            {
                try
                {
                    this.ChipID = MyChip.Unknown;
                    this.FindIT8790H2RAM(this.REGISTER_PORTS[1], this.VALUE_PORTS[1], out pEnvironmentController);
                    flag = pEnvironmentController != null;
                }
                catch
                {
                    pEnvironmentController = (MyITH2RamSmartFan)null;
                }
            }
            return flag;
        }

        public bool IsExist()
        {
            bool flag = false;
            MyIT87XX pEnvironmentController = (MyIT87XX)null;
            lock (this.m_findLock)
            {
                try
                {
                    this.ChipID = MyChip.Unknown;
                    this.FindMainEnvironmentController(out pEnvironmentController);
                    flag = pEnvironmentController != null;
                }
                catch
                {
                    flag = false;
                }
            }
            return flag;
        }

        public bool IsSecondaryExist()
        {
            bool flag = false;
            MyIT87XX pEnvironmentController = (MyIT87XX)null;
            lock (this.m_findLock)
            {
                try
                {
                    this.ChipID = MyChip.Unknown;
                    this.FindSecondaryEnvironmentController(out pEnvironmentController);
                    flag = pEnvironmentController != null;
                }
                catch
                {
                    flag = false;
                }
            }
            return flag;
        }

        public void EnableFSPI4Secondary(bool bEnableFSPI, bool bFindSecondaryEnvironmentController)
        {
            if (bFindSecondaryEnvironmentController && !this.IsSecondaryExist())
                return;
            ushort registerPort;
            this.RegisterPort = registerPort = this.REGISTER_PORTS[1];
            ushort valuePort;
            this.ValuePort = valuePort = this.VALUE_PORTS[1];
            this.Select(registerPort, valuePort, (byte)7);
            byte num1 = this.ReadByte(registerPort, valuePort, (byte)44);
            Thread.Sleep(1);
            byte num2;
            if (bEnableFSPI)
            {
                if (((int)num1 >> 3 & 1) == 1)
                    return;
                num2 = (byte)((uint)num1 | 8U);
            }
            else
            {
                if (((int)num1 >> 3 & 1) == 0)
                    return;
                num2 = Convert.ToByte((int)num1 & -9);
            }
            this.Select(registerPort, valuePort, (byte)7);
            this.WriteByte(registerPort, valuePort, (byte)44, num2);
        }

        public void EnableSmartFanControl4Secondary(bool bEnable)
        {
            try
            {
                MyITH2RamSmartFan pEnvironmentController;
                this.Find(out pEnvironmentController);
                pEnvironmentController?.EnableSmartFanControl(bEnable);
            }
            catch
            {
            }
        }

        public void ReadSubzeroIoPort(
          out MyChip pChipset,
          out bool pBramActivate,
          out ushort pIndexPort,
          out ushort pDataPort)
        {
            try
            {
                this.FindSubzeroIoPort(this.REGISTER_PORTS[1], this.VALUE_PORTS[1], out pChipset, out pBramActivate, out pIndexPort, out pDataPort);
            }
            catch
            {
                pChipset = MyChip.Unknown;
                pBramActivate = false;
                pIndexPort = (ushort)0;
                pDataPort = (ushort)0;
            }
        }

        public void Find(out MyIT879XSubzero pSubzeroTemperature)
        {
            try
            {
                pSubzeroTemperature = (MyIT879XSubzero)null;
                MyChip pChipset;
                bool pBramActivate;
                ushort pIndexPort;
                ushort pDataPort;
                this.FindSubzeroIoPort(this.REGISTER_PORTS[1], this.VALUE_PORTS[1], out pChipset, out pBramActivate, out pIndexPort, out pDataPort);
                if (!pBramActivate || pIndexPort == (ushort)0 || pDataPort == (ushort)0)
                    return;
                pSubzeroTemperature = new MyIT879XSubzero(pChipset, (uint)pIndexPort, (uint)pDataPort);
            }
            catch
            {
                pSubzeroTemperature = (MyIT879XSubzero)null;
            }
        }

        protected void IT87Enter(ushort registerPort)
        {
            try
            {
                if (registerPort == (ushort)46)
                {
                    MyNativeYccMethods.gb_outp((uint)registerPort, (byte)135);
                    MyNativeYccMethods.gb_outp((uint)registerPort, (byte)1);
                    MyNativeYccMethods.gb_outp((uint)registerPort, (byte)85);
                    MyNativeYccMethods.gb_outp((uint)registerPort, (byte)85);
                }
                else
                {
                    if (registerPort != (ushort)78)
                        return;
                    MyNativeYccMethods.gb_outp((uint)registerPort, (byte)135);
                    MyNativeYccMethods.gb_outp((uint)registerPort, (byte)1);
                    MyNativeYccMethods.gb_outp((uint)registerPort, (byte)85);
                    MyNativeYccMethods.gb_outp((uint)registerPort, (byte)170);
                }
            }
            catch
            {
                throw;
            }
        }

        protected void IT87Exit(ushort registerPort, ushort valuePort)
        {
            try
            {
                MyNativeYccMethods.gb_outp((uint)registerPort, (byte)2);
                MyNativeYccMethods.gb_outp((uint)valuePort, (byte)2);
            }
            catch
            {
                throw;
            }
        }

        private bool FindMainEnvironmentController(out MyIT87XX pEnvironmentController)
        {
            try
            {
                pEnvironmentController = (MyIT87XX)null;
                this.RegisterPort = this.REGISTER_PORTS[0];
                this.ValuePort = this.VALUE_PORTS[0];
                return this.FindMainEnvironmentController(this.REGISTER_PORTS[0], this.VALUE_PORTS[0], out pEnvironmentController);
            }
            catch
            {
                throw;
            }
        }

        private bool FindMainEnvironmentController(
          ushort registerPort,
        ushort valuePort,
          out MyIT87XX pEnvironmentController)
        {
            bool environmentController1 = false;
            pEnvironmentController = (MyIT87XX)null;
            if (registerPort != (ushort)46)
                return environmentController1;
            this.IT87Enter(registerPort);
            ushort num1 = this.ReadWord(registerPort, valuePort, (byte)32);
            MyChip chip;
            switch (num1)
            {
                case 34336:
                    chip = MyChip.IT8620E;
                    break;
                case 34344:
                    chip = MyChip.IT8628E;
                    break;
                case 34600:
                    chip = MyChip.IT8728F;
                    break;
                default:
                    chip = MyChip.Unknown;
                    break;
            }
            this.ChipID = chip;
            if (chip == MyChip.Unknown && num1 != (ushort)0 && num1 != ushort.MaxValue)
            {
                this.IT87Exit(registerPort, valuePort);
                return environmentController1;
            }
            this.Select(registerPort, valuePort, (byte)4);
            ushort baseAddress = this.ReadWord(registerPort, valuePort, (byte)96);
            Thread.Sleep(1);
            ushort num2 = this.ReadWord(registerPort, valuePort, (byte)96);
            byte version = (byte)((uint)this.ReadByte(registerPort, valuePort, (byte)34) & 15U);
            this.Select(registerPort, valuePort, (byte)7);
            ushort gpioAddress = this.ReadWord(registerPort, valuePort, (byte)98);
            Thread.Sleep(1);
            ushort num3 = this.ReadWord(registerPort, valuePort, (byte)98);
            this.IT87Exit(registerPort, valuePort);
            if ((int)baseAddress != (int)num2 || baseAddress < (ushort)256 || ((int)baseAddress & 61447) != 0 || (int)gpioAddress != (int)num3 || gpioAddress < (ushort)256 || ((int)gpioAddress & 61447) != 0)
                return environmentController1;
            bool environmentController2 = true;
            switch (chip)
            {
                case MyChip.IT8620E:
                case MyChip.IT8628E:
                    pEnvironmentController = (MyIT87XX)new MyIT8620(this.ChipID, baseAddress, gpioAddress, version);
                    break;
                case MyChip.IT8728F:
                    pEnvironmentController = (MyIT87XX)new MyIT8728(this.ChipID, baseAddress, gpioAddress, version);
                    break;
                default:
                    environmentController2 = false;
                    break;
            }
            return environmentController2;
        }

        private bool FindSecondaryEnvironmentController(out MyIT87XX pEnvironmentController)
        {
            try
            {
                pEnvironmentController = (MyIT87XX)null;
                this.RegisterPort = this.REGISTER_PORTS[1];
                this.ValuePort = this.VALUE_PORTS[1];
                return this.FindSecondaryEnvironmentController(this.REGISTER_PORTS[1], this.VALUE_PORTS[1], out pEnvironmentController);
            }
            catch
            {
                throw;
            }
        }

        private bool FindSecondaryEnvironmentController(
          ushort registerPort,
        ushort valuePort,
          out MyIT87XX pEnvironmentController)
        {
            bool environmentController1 = false;
            pEnvironmentController = (MyIT87XX)null;
            if (registerPort != (ushort)78)
                return environmentController1;
            this.IT87Enter(registerPort);
            ushort num1 = this.ReadWord(registerPort, valuePort, (byte)32);
            MyChip chip;
            switch (num1)
            {
                case 34611:
                    chip = MyChip.IT8791E;
                    break;
                case 34704:
                    chip = MyChip.IT8790F;
                    break;
                default:
                    chip = MyChip.Unknown;
                    break;
            }
            this.ChipID = chip;
            if (chip == MyChip.Unknown && num1 != (ushort)0 && num1 != ushort.MaxValue)
            {
                this.IT87Exit(registerPort, valuePort);
                return environmentController1;
            }
            this.Select(registerPort, valuePort, (byte)4);
            ushort baseAddress = this.ReadWord(registerPort, valuePort, (byte)96);
            Thread.Sleep(1);
            ushort num2 = this.ReadWord(registerPort, valuePort, (byte)96);
            byte version = (byte)((uint)this.ReadByte(registerPort, valuePort, (byte)34) & 15U);
            this.Select(registerPort, valuePort, (byte)7);
            ushort gpioAddress = this.ReadWord(registerPort, valuePort, (byte)98);
            Thread.Sleep(1);
            ushort num3 = this.ReadWord(registerPort, valuePort, (byte)98);
            this.IT87Exit(registerPort, valuePort);
            if ((int)baseAddress != (int)num2 || baseAddress < (ushort)256 || ((int)baseAddress & 61447) != 0 || (int)gpioAddress != (int)num3 || gpioAddress < (ushort)256 || ((int)gpioAddress & 61447) != 0)
                return environmentController1;
            bool environmentController2 = true;
            switch (chip)
            {
                case MyChip.IT8791E:
                case MyChip.IT8790F:
                    pEnvironmentController = (MyIT87XX)new MyIT8790(this.ChipID, baseAddress, gpioAddress, version);
                    break;
                default:
                    environmentController2 = false;
                    break;
            }
            return environmentController2;
        }

        private bool FindIT8790H2RAM(
          ushort registerPort,
          ushort valuePort,
          out MyITH2RamSmartFan pEnvironmentController)
        {
            bool it8790H2Ram = false;
            pEnvironmentController = (MyITH2RamSmartFan)null;
            if (registerPort != (ushort)78)
                return it8790H2Ram;
            this.IT87Enter(registerPort);
            ushort num1 = this.ReadWord(registerPort, valuePort, (byte)32);
            switch (num1)
            {
                case 34611:
                    this.ChipID = MyChip.IT8791E;
                    break;
                case 34704:
                    this.ChipID = MyChip.IT8790F;
                    break;
                default:
                    this.ChipID = MyChip.Unknown;
                    break;
            }
            if (this.ChipID == MyChip.Unknown)
            {
                if (num1 != (ushort)0 && num1 != ushort.MaxValue)
                    this.IT87Exit(registerPort, valuePort);
            }
            else
            {
                this.Select(registerPort, valuePort, (byte)4);
                ushort baseAddress = this.ReadWord(registerPort, valuePort, (byte)96);
                Thread.Sleep(1);
                ushort num2 = this.ReadWord(registerPort, valuePort, (byte)96);
                byte version = (byte)((uint)this.ReadByte(registerPort, valuePort, (byte)34) & 15U);
                this.Select(registerPort, valuePort, (byte)7);
                ushort gpioAddress = this.ReadWord(registerPort, valuePort, (byte)98);
                Thread.Sleep(1);
                ushort num3 = this.ReadWord(registerPort, valuePort, (byte)98);
                this.Select(registerPort, valuePort, (byte)15);
                ushort h2ramAddressBits = (ushort)this.ReadByte(registerPort, valuePort, (byte)246);
                Thread.Sleep(1);
                ushort num4 = (ushort)this.ReadByte(registerPort, valuePort, (byte)246);
                this.IT87Exit(registerPort, valuePort);
                if ((int)baseAddress != (int)num2 || baseAddress < (ushort)256 || ((int)baseAddress & 61447) != 0 || (int)gpioAddress != (int)num3 || gpioAddress < (ushort)256 || ((int)gpioAddress & 61447) != 0 || (int)h2ramAddressBits != (int)num4)
                    return it8790H2Ram;
                pEnvironmentController = new MyITH2RamSmartFan(this.ChipID, baseAddress, gpioAddress, h2ramAddressBits, version);
                it8790H2Ram = true;
            }
            return it8790H2Ram;
        }

        private bool FindSubzeroIoPort(
          ushort registerPort,
          ushort valuePort,
          out MyChip pChipset,
          out bool pBramActivate,
          out ushort pIndexPort,
          out ushort pDataPort)
        {
            bool subzeroIoPort1 = false;
            ushort num1 = 0;
            pChipset = MyChip.Unknown;
            pBramActivate = false;
            pIndexPort = (ushort)0;
            pDataPort = (ushort)0;
            if (registerPort != (ushort)78 || valuePort != (ushort)79)
                return subzeroIoPort1;
            this.IT87Enter(registerPort);
            ushort num2 = this.ReadWord(registerPort, valuePort, (byte)32);
            MyChip chip;
            switch (num2)
            {
                case 34611:
                    chip = MyChip.IT8791E;
                    break;
                case 34704:
                    chip = MyChip.IT8790F;
                    break;
                default:
                    chip = MyChip.Unknown;
                    break;
            }
            if (chip == MyChip.Unknown)
            {
                if (num2 != (ushort)0 && num2 != ushort.MaxValue)
                    this.IT87Exit(registerPort, valuePort);
                return subzeroIoPort1;
            }
            try
            {
                pChipset = chip;
                this.Select(registerPort, valuePort, (byte)16);
                ushort num3 = (ushort)this.ReadByte(registerPort, valuePort, (byte)48);
                if (num3 == (ushort)0)
                    return true;
                pBramActivate = num3 == (ushort)1;
                ushort num4 = this.ReadWord(registerPort, valuePort, (byte)96);
                if (num4 == (ushort)0)
                {
                    bool subzeroIoPort2 = true;
                    pBramActivate = false;
                    pIndexPort = (ushort)0;
                    pDataPort = (ushort)0;
                    return subzeroIoPort2;
                }
                pIndexPort = num4;
                pDataPort = num1 = (ushort)((uint)num4 + 1U);
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                this.IT87Exit(registerPort, valuePort);
            }
        }
    }
}
