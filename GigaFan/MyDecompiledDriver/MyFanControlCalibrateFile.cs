﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.FanControlCalibrateFile
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll



using System;
using System.IO;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyFanControlCalibrateFile
    {
        private const string STR_PROFILE_FOLDER_NAME = "Profile";
        private const string STR_FAN_CALIBRATE_EXTENSION_NAME = "xml";
        private const string STR_FAN_CALIBRATE_PREFIX_NAME = "FanCalibrate";

        public int FanControlCount { get; set; }

        public bool IsCalibrateFileExist()
        {
            bool flag = false;
            if (this.FanControlCount == 0)
                throw new IndexOutOfRangeException();
            for (int fanControlIndex = 0; fanControlIndex < this.FanControlCount; ++fanControlIndex)
            {
                flag = this.IsCalibrateFileExist(fanControlIndex);
                if (!flag)
                    break;
            }
            return flag;
        }

        public bool IsCalibrateFileExist(int fanControlIndex)
        {
            string pProfileFilePath = string.Empty;
            if (this.FanControlCount == 0)
                throw new IndexOutOfRangeException();
            if (fanControlIndex >= this.FanControlCount)
                throw new IndexOutOfRangeException();
            this.RetrieveProfileFilePath(fanControlIndex, out pProfileFilePath);
            return File.Exists(pProfileFilePath);
        }

        public void ReadCalibrateData(int fanControlIndex, ref MyFanSpeedDataCollection pFanSpeedDatas)
        {
            string pProfileFilePath = string.Empty;
            if (this.FanControlCount == 0)
                throw new IndexOutOfRangeException();
            if (fanControlIndex >= this.FanControlCount)
                throw new IndexOutOfRangeException();
            try
            {
                this.RetrieveProfileFilePath(fanControlIndex, out pProfileFilePath);
                MyFanSpeedDataFile fanSpeedDataFile = new MyFanSpeedDataFile(pProfileFilePath);
                if (!fanSpeedDataFile.FileExist)
                    throw new FileNotFoundException();
                fanSpeedDataFile.Read(ref pFanSpeedDatas);
            }
            catch
            {
                throw;
            }
        }

        public void WriteCalibrateData(int fanControlIndex, ref MyFanSpeedDataCollection pFanSpeedDatas)
        {
            string pProfileFilePath = string.Empty;
            if (this.FanControlCount == 0)
                throw new IndexOutOfRangeException();
            if (fanControlIndex >= this.FanControlCount)
                throw new IndexOutOfRangeException();
            if (pFanSpeedDatas.Count == 0)
                throw new ArgumentException();
            try
            {
                this.RetrieveProfileFilePath(fanControlIndex, out pProfileFilePath);
                new MyFanSpeedDataFile(pProfileFilePath).Write(ref pFanSpeedDatas);
                if ((File.GetAttributes(pProfileFilePath) & FileAttributes.Hidden) == FileAttributes.Hidden)
                    return;
                File.SetAttributes(pProfileFilePath, File.GetAttributes(pProfileFilePath) | FileAttributes.Hidden);
            }
            catch
            {
                throw;
            }
        }

        public void DeleteCalibrateData(int fanControlIndex)
        {
            string pProfileFilePath = string.Empty;
            if (this.FanControlCount == 0)
                throw new IndexOutOfRangeException();
            if (fanControlIndex >= this.FanControlCount)
                throw new IndexOutOfRangeException();
            try
            {
                this.RetrieveProfileFilePath(fanControlIndex, out pProfileFilePath);
                if (!File.Exists(pProfileFilePath))
                    return;
                File.Delete(pProfileFilePath);
            }
            catch
            {
                throw;
            }
        }

        private void RetrieveProfileFilePath(int fanControlIndex, out string pProfileFilePath)
        {
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            string empty3 = string.Empty;
            pProfileFilePath = string.Empty;
            string str = Path.Combine(MyWorkingDirectory.GetCurrentDirectory(true), "Profile");
            if (!Directory.Exists(str))
            {
                Directory.CreateDirectory(str).Attributes = FileAttributes.Hidden | FileAttributes.Directory;
                Console.WriteLine("The directory was created successfully at {0}.", (object)Directory.GetCreationTime(str));
            }
            string path2 = string.Format("{0}{1}.{2}", (object)"FanCalibrate", (object)fanControlIndex, (object)"xml");
            pProfileFilePath = Path.Combine(str, path2);
        }

        private void RetrieveProfileDirectoryPath(out string pProfilePath)
        {
            string empty = string.Empty;
            pProfilePath = string.Empty;
            string currentDirectory = MyWorkingDirectory.GetCurrentDirectory(true);
            pProfilePath = Path.Combine(currentDirectory, "Profile");
        }
    }
}
