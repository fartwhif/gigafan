﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.WinRegistry.RegistryGigabyteKeyPath
// Assembly: Gigabyte, Version=7.2.0.22, Culture=neutral, PublicKeyToken=null
// MVID: FE603752-DF9C-4680-B734-106AEDD3F213
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.dll

using System;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public static class MyRegistryGigabyteKeyPath
    {
        private const string REG_SUBKEY_ROOT_COMPANY = "SOFTWARE\\GIGABYTE";
        private const string REG_SUBKEY_ROOT_COMPANY_WOW6432NODE = "SOFTWARE\\Wow6432Node\\GIGABYTE";

        public static void Get(out string pKeyPath) => MyRegistryGigabyteKeyPath.Get(out pKeyPath, true);

        public static void Get(out string pKeyPath, bool bIgnoreEnvironment4Is64BitProcess)
        {
            if (!Environment.Is64BitOperatingSystem)
                pKeyPath = "SOFTWARE\\GIGABYTE";
            else if (bIgnoreEnvironment4Is64BitProcess)
                pKeyPath = "SOFTWARE\\Wow6432Node\\GIGABYTE";
            else if (!Environment.Is64BitProcess)
                pKeyPath = "SOFTWARE\\Wow6432Node\\GIGABYTE";
            else
                pKeyPath = "SOFTWARE\\GIGABYTE";
        }
    }
}
