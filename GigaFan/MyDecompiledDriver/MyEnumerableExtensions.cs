﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.Utilities.EnumerableExtensions
// Assembly: Gigabyte, Version=7.2.0.22, Culture=neutral, PublicKeyToken=null
// MVID: FE603752-DF9C-4680-B734-106AEDD3F213
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.dll

using System.Collections;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public static class MyEnumerableExtensions
    {
        public static int Count(this IEnumerable source)
        {
            int num = 0;
            foreach (object obj in source)
                ++num;
            return num;
        }
    }
}
