﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.ITH2RamFirmwareVersion
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyITH2RamFirmwareVersion : MyITH2RamBase
    {
        private const int MAX_EC_SPACE_BUFFER_SIZE = 255;
        private const int EC_SPACE_OFFSET_BASE = 2560;
        private const int EC_SPACE_OFFSET_EC1_VER_MAIN = 0;
        private const int EC_SPACE_OFFSET_EC1_VER_SUB = 1;
        private const int EC_SPACE_OFFSET_EC2_VER_BYTE0 = 2;
        private const int EC_SPACE_OFFSET_EC2_VER_BYTE1 = 3;
        private const int EC_SPACE_OFFSET_EC2_VER_BYTE2 = 4;
        private const int EC_SPACE_OFFSET_EC2_VER_BYTE3 = 5;
        private const int EC_SPACE_OFFSET_EC2_VER_BYTE4 = 6;

        public MyITH2RamFirmwareVersion(
          MyChip chip,
          ushort baseAddress,
          ushort gpioAddress,
          ushort h2ramAddressBits,
          byte version)
        {
            bool flag = false;
            int pMainVersion = 0;
            int pSubVerxion = 0;
            if (chip == MyChip.IT8790F || chip == MyChip.IT8791E)
                flag = true;
            if (!flag)
                throw new NotSupportedException();
            this.BaseAddress = baseAddress;
            this.chip = chip;
            this.Version = version;
            this.AddressRegister = (ushort)((uint)baseAddress + 5U);
            this.DataRegister = (ushort)((uint)baseAddress + 6U);
            this.GpioAddress = gpioAddress;
            this.Supported = false;
            bool valid;
            byte num = this.ReadByte((byte)88, out valid);
            if (!valid || num != (byte)144 || ((int)this.ReadByte((byte)0, out valid) & 16) == 0 || !valid)
                return;
            this.H2RamBaseAddress = (uint)(-16777216 | (int)h2ramAddressBits << 16) + 2560U;
            this.H2RamPhysicalAddress = 0UL;
            this.EnableShareMemory(true);
            this.RetrieveIT8791Version(out pMainVersion, out pSubVerxion);
            this.EnableShareMemory(false);
            if (pMainVersion == 0 && pSubVerxion == 0)
                return;
            this.MainVersion = pMainVersion;
            this.SubVersion = pSubVerxion;
            this.Supported = true;
        }

        public int MainVersion { get; protected set; }

        public int SubVersion { get; protected set; }

        private void EnableShareMemory(bool bEnable)
        {
            this.EnableShareMemory(bEnable, (uint)byte.MaxValue);
        }

        private void RetrieveIT8791Version(out int pMainVersion, out int pSubVerxion)
        {
            bool valid = false;
            pMainVersion = 0;
            pSubVerxion = 0;
            byte num1 = this.ReadByteEx(0, out valid);
            if (!valid)
                return;
            byte num2 = this.ReadByteEx(1, out valid);
            if (!valid)
                return;
            pMainVersion = (int)num1;
            pSubVerxion = (int)num2;
        }
    }
}
