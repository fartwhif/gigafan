﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.FanSpeedDataCollection
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MyFanSpeedDataCollection : ObservableCollection<MyFanSpeedData>
    {
        public void UpdateCollection()
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}
