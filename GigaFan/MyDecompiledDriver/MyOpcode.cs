﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Processor.Opcode
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Runtime.InteropServices;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal static class Opcode
    {
        private static IntPtr codeBuffer;
        private static ulong size;
        public static Opcode.RdtscDelegate Rdtsc;
        private static readonly byte[] RDTSC_32 = new byte[3]
        {
      (byte) 15,
      (byte) 49,
      (byte) 195
        };
        private static readonly byte[] RDTSC_64 = new byte[10]
        {
      (byte) 15,
      (byte) 49,
      (byte) 72,
      (byte) 193,
      (byte) 226,
      (byte) 32,
      (byte) 72,
      (byte) 11,
      (byte) 194,
      (byte) 195
        };
        public static Opcode.CpuidDelegate Cpuid;
        private static readonly byte[] CPUID_32 = new byte[68]
        {
      (byte) 85,
      (byte) 139,
      (byte) 236,
      (byte) 131,
      (byte) 236,
      (byte) 16,
      (byte) 139,
      (byte) 69,
      (byte) 8,
      (byte) 139,
      (byte) 77,
      (byte) 12,
      (byte) 83,
      (byte) 15,
      (byte) 162,
      (byte) 86,
      (byte) 141,
      (byte) 117,
      (byte) 240,
      (byte) 137,
      (byte) 6,
      (byte) 139,
      (byte) 69,
      (byte) 16,
      (byte) 137,
      (byte) 94,
      (byte) 4,
      (byte) 137,
      (byte) 78,
      (byte) 8,
      (byte) 137,
      (byte) 86,
      (byte) 12,
      (byte) 139,
      (byte) 77,
      (byte) 240,
      (byte) 137,
      (byte) 8,
      (byte) 139,
      (byte) 69,
      (byte) 20,
      (byte) 139,
      (byte) 77,
      (byte) 244,
      (byte) 137,
      (byte) 8,
      (byte) 139,
      (byte) 69,
      (byte) 24,
      (byte) 139,
      (byte) 77,
      (byte) 248,
      (byte) 137,
      (byte) 8,
      (byte) 139,
      (byte) 69,
      (byte) 28,
      (byte) 139,
      (byte) 77,
      (byte) 252,
      (byte) 94,
      (byte) 137,
      (byte) 8,
      (byte) 91,
      (byte) 201,
      (byte) 194,
      (byte) 24,
      (byte) 0
        };
        private static readonly byte[] CPUID_64_WINDOWS = new byte[37]
        {
      (byte) 72,
      (byte) 137,
      (byte) 92,
      (byte) 36,
      (byte) 8,
      (byte) 139,
      (byte) 193,
      (byte) 139,
      (byte) 202,
      (byte) 15,
      (byte) 162,
      (byte) 65,
      (byte) 137,
      (byte) 0,
      (byte) 72,
      (byte) 139,
      (byte) 68,
      (byte) 36,
      (byte) 40,
      (byte) 65,
      (byte) 137,
      (byte) 25,
      (byte) 72,
      (byte) 139,
      (byte) 92,
      (byte) 36,
      (byte) 8,
      (byte) 137,
      (byte) 8,
      (byte) 72,
      (byte) 139,
      (byte) 68,
      (byte) 36,
      (byte) 48,
      (byte) 137,
      (byte) 16,
      (byte) 195
        };
        private static readonly byte[] CPUID_64_LINUX = new byte[27]
        {
      (byte) 73,
      (byte) 137,
      (byte) 210,
      (byte) 73,
      (byte) 137,
      (byte) 203,
      (byte) 83,
      (byte) 137,
      (byte) 248,
      (byte) 137,
      (byte) 241,
      (byte) 15,
      (byte) 162,
      (byte) 65,
      (byte) 137,
      (byte) 2,
      (byte) 65,
      (byte) 137,
      (byte) 27,
      (byte) 65,
      (byte) 137,
      (byte) 8,
      (byte) 65,
      (byte) 137,
      (byte) 17,
      (byte) 91,
      (byte) 195
        };

        public static void Open()
        {
            int platform = (int)Environment.OSVersion.Platform;
            byte[] source1;
            byte[] source2;
            if (IntPtr.Size == 4)
            {
                source1 = Opcode.RDTSC_32;
                source2 = Opcode.CPUID_32;
            }
            else
            {
                source1 = Opcode.RDTSC_64;
                source2 = Opcode.CPUID_64_WINDOWS;
            }
            Opcode.size = (ulong)(source1.Length + source2.Length);
            Opcode.codeBuffer = Opcode.NativeMethods.VirtualAlloc(IntPtr.Zero, (UIntPtr)Opcode.size, Opcode.AllocationType.COMMIT | Opcode.AllocationType.RESERVE, Opcode.MemoryProtection.EXECUTE_READWRITE);
            Marshal.Copy(source1, 0, Opcode.codeBuffer, source1.Length);
            Opcode.Rdtsc = Marshal.GetDelegateForFunctionPointer(Opcode.codeBuffer, typeof(Opcode.RdtscDelegate)) as Opcode.RdtscDelegate;
            IntPtr num = (IntPtr)((long)Opcode.codeBuffer + (long)source1.Length);
            Marshal.Copy(source2, 0, num, source2.Length);
            Opcode.Cpuid = Marshal.GetDelegateForFunctionPointer(num, typeof(Opcode.CpuidDelegate)) as Opcode.CpuidDelegate;
        }

        public static void Close()
        {
            Opcode.Rdtsc = (Opcode.RdtscDelegate)null;
            Opcode.Cpuid = (Opcode.CpuidDelegate)null;
            Opcode.NativeMethods.VirtualFree(Opcode.codeBuffer, UIntPtr.Zero, Opcode.FreeType.RELEASE);
        }

        public static bool CpuidTx(
          uint index,
          uint ecxValue,
          out uint eax,
          out uint ebx,
          out uint ecx,
          out uint edx,
          ulong threadAffinityMask)
        {
            bool flag = false;
            try
            {
                MyProcessorAffinity processorAffinity = new MyProcessorAffinity();
                processorAffinity.SetProcessAffinityMask((uint)threadAffinityMask);
                int num = Opcode.Cpuid(index, ecxValue, out eax, out ebx, out ecx, out edx) ? 1 : 0;
                processorAffinity.RestoreProcessAffinityMask();
                flag = true;
            }
            catch (Exception ex)
            {
                eax = ebx = ecx = edx = 0U;
            }
            return flag;
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate ulong RdtscDelegate();

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate bool CpuidDelegate(
          uint index,
          uint ecxValue,
          out uint eax,
          out uint ebx,
          out uint ecx,
          out uint edx);

        [Flags]
        public enum AllocationType : uint
        {
            COMMIT = 4096, // 0x00001000
            RESERVE = 8192, // 0x00002000
            RESET = 524288, // 0x00080000
            LARGE_PAGES = 536870912, // 0x20000000
            PHYSICAL = 4194304, // 0x00400000
            TOP_DOWN = 1048576, // 0x00100000
            WRITE_WATCH = 2097152, // 0x00200000
        }

        [Flags]
        public enum MemoryProtection : uint
        {
            EXECUTE = 16, // 0x00000010
            EXECUTE_READ = 32, // 0x00000020
            EXECUTE_READWRITE = 64, // 0x00000040
            EXECUTE_WRITECOPY = 128, // 0x00000080
            NOACCESS = 1,
            READONLY = 2,
            READWRITE = 4,
            WRITECOPY = 8,
            GUARD = 256, // 0x00000100
            NOCACHE = 512, // 0x00000200
            WRITECOMBINE = 1024, // 0x00000400
        }

        [Flags]
        public enum FreeType
        {
            DECOMMIT = 16384, // 0x00004000
            RELEASE = 32768, // 0x00008000
        }

        private static class NativeMethods
        {
            private const string KERNEL = "kernel32.dll";

            [DllImport("kernel32.dll")]
            public static extern IntPtr VirtualAlloc(
              IntPtr lpAddress,
              UIntPtr dwSize,
              Opcode.AllocationType flAllocationType,
              Opcode.MemoryProtection flProtect);

            [DllImport("kernel32.dll")]
            public static extern bool VirtualFree(
              IntPtr lpAddress,
              UIntPtr dwSize,
              Opcode.FreeType dwFreeType);
        }
    }
}
