﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.FanRelatedWinRegistry
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using Microsoft.Win32;

using System;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyFanRelatedWinRegistry
    {
        private const string REG_SUBKEY_DEFAULT_ROOT_FOLDER = "ThermalConsole";
        private const string REG_SUBKEY_FOLDER = "SIO";
        private const string REG_FAN_COUNT_NAME = "FanCount";
        private const string REG_TEMPERATURE_INPUT_COUNT_NAME = "TemperatureInputCount";
        private const string REG_VOLTAGE_INPUT_COUNT_NAME = "VoltageInputCount";
        private MyRegistryKeyHandle m_regKeyMgr;

        public MyFanRelatedWinRegistry() => this.InitObjects("ThermalConsole");

        public bool DataExist { get; protected set; }

        public string ProductName { get; protected set; }

        public RegistryHive HiveMemberName { get; protected set; }

        public string Subkey { get; protected set; }

        public void WriteTemperatureInputCount(int temperatureInputCount)
        {
            if (!this.DataExist || this.m_regKeyMgr == null)
                return;
            this.m_regKeyMgr.WriteDwordValue(this.HiveMemberName, this.Subkey, "TemperatureInputCount", Convert.ToUInt32(temperatureInputCount));
        }

        public void ReadTemperatureInputCount(ref int pTemperatureInputCount)
        {
            uint pDwordValue = 0;
            pTemperatureInputCount = 0;
            if (!this.DataExist || this.m_regKeyMgr == null)
                return;
            this.m_regKeyMgr.ReadDwordValue(this.HiveMemberName, this.Subkey, "TemperatureInputCount", out pDwordValue);
            pTemperatureInputCount = Convert.ToInt32(pDwordValue);
        }

        public void WriteFanCount(int fanCount)
        {
            if (!this.DataExist || this.m_regKeyMgr == null)
                return;
            this.m_regKeyMgr.WriteDwordValue(this.HiveMemberName, this.Subkey, "FanCount", Convert.ToUInt32(fanCount));
        }

        public void ReadFanCount(ref int pFanCount)
        {
            uint pDwordValue = 0;
            pFanCount = 0;
            if (!this.DataExist || this.m_regKeyMgr == null)
                return;
            this.m_regKeyMgr.ReadDwordValue(this.HiveMemberName, this.Subkey, "FanCount", out pDwordValue);
            pFanCount = Convert.ToInt32(pDwordValue);
        }

        public void WriteVoltageInputCount(int voltageInputCount)
        {
            if (!this.DataExist || this.m_regKeyMgr == null)
                return;
            this.m_regKeyMgr.WriteDwordValue(this.HiveMemberName, this.Subkey, "VoltageInputCount", Convert.ToUInt32(voltageInputCount));
        }

        public void ReadVoltageInputCount(ref int pVoltageInputCount)
        {
            uint pDwordValue = 0;
            pVoltageInputCount = 0;
            if (!this.DataExist || this.m_regKeyMgr == null)
                return;
            this.m_regKeyMgr.ReadDwordValue(this.HiveMemberName, this.Subkey, "VoltageInputCount", out pDwordValue);
            pVoltageInputCount = Convert.ToInt32(pDwordValue);
        }

        private void InitObjects(string productName)
        {
            try
            {
                this.DataExist = false;
                this.ProductName = productName;
                this.HiveMemberName = RegistryHive.LocalMachine;
                string pKeyPath;
                MyRegistryGigabyteKeyPath.Get(out pKeyPath);
                this.Subkey = string.Format("{0}\\{1}\\{2}", (object)pKeyPath, (object)productName, (object)"SIO");
                if (this.m_regKeyMgr == null)
                    this.m_regKeyMgr = new MyRegistryKeyHandle();
                if (!this.m_regKeyMgr.IsSubkeyExist(this.HiveMemberName, this.Subkey))
                    this.m_regKeyMgr.CreateSubKey(this.HiveMemberName, this.Subkey);
                if (!this.m_regKeyMgr.IsValueNameExist(this.HiveMemberName, this.Subkey, "TemperatureInputCount"))
                    this.m_regKeyMgr.WriteDwordValue(this.HiveMemberName, this.Subkey, "TemperatureInputCount", 0U);
                if (!this.m_regKeyMgr.IsValueNameExist(this.HiveMemberName, this.Subkey, "FanCount"))
                    this.m_regKeyMgr.WriteDwordValue(this.HiveMemberName, this.Subkey, "FanCount", 0U);
                if (!this.m_regKeyMgr.IsValueNameExist(this.HiveMemberName, this.Subkey, "VoltageInputCount"))
                    this.m_regKeyMgr.WriteDwordValue(this.HiveMemberName, this.Subkey, "VoltageInputCount", 0U);
                this.DataExist = true;
            }
            catch
            {
                throw;
            }
        }
    }
}
