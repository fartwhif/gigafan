﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.ITH2RamBase
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Threading;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyITH2RamBase
    {
        protected const byte ITE_VENDOR_ID = 144;
        protected const byte ADDRESS_REGISTER_OFFSET = 5;
        protected const byte DATA_REGISTER_OFFSET = 6;
        protected const byte CONFIGURATION_REGISTER = 0;
        protected const byte VENDOR_ID_REGISTER = 88;

        public MyChip chip { get; protected set; }

        public byte Version { get; protected set; }

        public bool Supported { get; protected set; }

        public ushort BaseAddress { get; protected set; }

        public ushort AddressRegister { get; protected set; }

        public ushort DataRegister { get; protected set; }

        public ushort GpioAddress { get; protected set; }

        public uint H2RamBaseAddress { get; protected set; }

        public ulong H2RamPhysicalAddress { get; protected set; }

        public ushort H2RamAddressBits { get; protected set; }

        protected void EnableShareMemory(bool bEnable, uint shareMemorySize)
        {
            uint pShareMemoryCapabilityValue = 0;
            uint pEnvironmentControlCapabilityValue = 0;
            if (bEnable && shareMemorySize == 0U)
                return;
            ulong Length = (ulong)shareMemorySize;
            if (this.CheckEnableShareMemory() == bEnable)
                return;
            try
            {
                this.RetrieveRegisterValue4ShareMemory(out pShareMemoryCapabilityValue, out pEnvironmentControlCapabilityValue);
                uint shareMemoryCapabilityValue;
                uint environmentControlCapabilityValue;
                if (bEnable)
                {
                    shareMemoryCapabilityValue = MyBitWiseHelper.ZeroBit(pShareMemoryCapabilityValue, 0);
                    environmentControlCapabilityValue = MyBitWiseHelper.BitSet(pEnvironmentControlCapabilityValue, 0);
                }
                else
                {
                    shareMemoryCapabilityValue = Convert.ToUInt32(MyBitWiseHelper.BitSet(pShareMemoryCapabilityValue, 0));
                    environmentControlCapabilityValue = Convert.ToUInt32(MyBitWiseHelper.ZeroBit(pEnvironmentControlCapabilityValue, 0));
                }
                this.UpdateRegisterValue4ShareMemory(shareMemoryCapabilityValue, environmentControlCapabilityValue);
                Thread.Sleep(500);
                if (bEnable)
                {
                    if (this.H2RamPhysicalAddress != 0UL)
                        return;
                    this.H2RamPhysicalAddress = MyNativeYccMethods.MapMemEx((long)this.H2RamBaseAddress, Length);
                    if (this.H2RamPhysicalAddress != 0UL)
                        return;
                    this.H2RamPhysicalAddress = 0UL;
                }
                else
                {
                    if (this.H2RamPhysicalAddress == 0UL)
                        return;
                    MyNativeYccMethods.UnMapMemEx(this.H2RamPhysicalAddress);
                    this.H2RamPhysicalAddress = 0UL;
                }
            }
            catch
            {
                throw;
            }
        }

        protected bool CheckEnableShareMemory()
        {
            uint pShareMemoryCapabilityValue = 0;
            uint pEnvironmentControlCapabilityValue = 0;
            bool flag1 = false;
            if (this.H2RamPhysicalAddress == 0UL)
                return flag1;
            try
            {
                this.RetrieveRegisterValue4ShareMemory(out pShareMemoryCapabilityValue, out pEnvironmentControlCapabilityValue);
                bool flag2 = (pShareMemoryCapabilityValue & 1U) == 0U;
                bool flag3 = (pEnvironmentControlCapabilityValue & 1U) == 1U;
                if (flag2)
                {
                    if (flag3)
                        flag1 = true;
                }
            }
            catch
            {
                flag1 = false;
            }
            return flag1;
        }

        protected void RetrieveRegisterValue4ShareMemory(
          out uint pShareMemoryCapabilityValue,
          out uint pEnvironmentControlCapabilityValue)
        {
            pShareMemoryCapabilityValue = 0U;
            pEnvironmentControlCapabilityValue = 0U;
            uint busNumber = 0;
            uint deviceNumber = 31;
            uint functionNumber = 0;
            try
            {
                uint registerOffset1 = 216;
                MyNativeYccMethods.gb_outpd(3320U, (ulong)MyPciFunctionAddress.ToAddress(busNumber, deviceNumber, functionNumber, registerOffset1));
                uint num1 = MyNativeYccMethods.gb_inpd(3324U);
                pShareMemoryCapabilityValue = num1;
                uint registerOffset2 = 152;
                MyNativeYccMethods.gb_outpd(3320U, (ulong)MyPciFunctionAddress.ToAddress(busNumber, deviceNumber, functionNumber, registerOffset2));
                uint num2 = MyNativeYccMethods.gb_inpd(3324U);
                pEnvironmentControlCapabilityValue = num2;
            }
            catch
            {
                pShareMemoryCapabilityValue = 0U;
                pEnvironmentControlCapabilityValue = 0U;
            }
        }

        protected void UpdateRegisterValue4ShareMemory(
          uint shareMemoryCapabilityValue,
          uint environmentControlCapabilityValue)
        {
            try
            {
                uint busNumber = 0;
                uint deviceNumber = 31;
                uint functionNumber = 0;
                uint registerOffset1 = 216;
                MyNativeYccMethods.gb_outpd(3320U, (ulong)MyPciFunctionAddress.ToAddress(busNumber, deviceNumber, functionNumber, registerOffset1));
                MyNativeYccMethods.gb_outpd(3324U, (ulong)shareMemoryCapabilityValue);
                uint registerOffset2 = 152;
                MyNativeYccMethods.gb_outpd(3320U, (ulong)MyPciFunctionAddress.ToAddress(busNumber, deviceNumber, functionNumber, registerOffset2));
                MyNativeYccMethods.gb_outpd(3324U, (ulong)environmentControlCapabilityValue);
            }
            catch
            {
                throw;
            }
        }

        protected uint RetrieveH2RamInterfaceMemoryAddress(uint ecSpaceOffsetBase)
        {
            uint num1 = 0;
            try
            {
                MyNativeYccMethods.gb_outpd(3320U, (ulong)MyPciFunctionAddress.ToAddress(0U, 31U, 0U, 152U));
                uint num2 = MyNativeYccMethods.gb_inpd(3324U);
                return num2 == 0U ? num1 : (num2 & 4294901760U) + ecSpaceOffsetBase;
            }
            catch
            {
                throw;
            }
        }

        protected byte ReadByte(byte register, out bool valid)
        {
            MyNativeYccMethods.gb_outp((uint)this.AddressRegister, register);
            byte num = MyNativeYccMethods.gb_inp((uint)this.DataRegister);
            valid = (int)register == (int)MyNativeYccMethods.gb_inp((uint)this.AddressRegister);
            return num;
        }

        protected bool WriteByte(byte register, byte value)
        {
            MyNativeYccMethods.gb_outp((uint)this.AddressRegister, register);
            MyNativeYccMethods.gb_outp((uint)this.DataRegister, value);
            return (int)register == (int)MyNativeYccMethods.gb_inp((uint)this.AddressRegister);
        }

        protected unsafe byte ReadByteEx(int spaceOffset, out bool valid)
        {
            byte num1 = 0;
            valid = false;
            if (this.H2RamBaseAddress == 0U || this.H2RamPhysicalAddress == 0UL)
                return num1;
            byte num2 = *(byte*)(this.H2RamPhysicalAddress + (ulong)spaceOffset);
            valid = true;
            return num2;
        }

        protected unsafe bool WriteByteEx(int spaceOffset, byte value)
        {
            bool flag = false;
            if (this.H2RamBaseAddress == 0U || this.H2RamPhysicalAddress == 0UL)
                return flag;
            *(byte*)(this.H2RamPhysicalAddress + (ulong)spaceOffset) = value;
            return true;
        }

        protected unsafe uint ReadUInt32Ex(int spaceOffset, out bool valid)
        {
            uint num1 = 0;
            valid = false;
            if (this.H2RamBaseAddress == 0U || this.H2RamPhysicalAddress == 0UL)
                return num1;
            uint num2 = *(uint*)(this.H2RamPhysicalAddress + (ulong)spaceOffset);
            valid = true;
            return num2;
        }

        protected unsafe bool WriteUInt32Ex(int spaceOffset, uint value)
        {
            bool flag = false;
            if (this.H2RamBaseAddress == 0U || this.H2RamPhysicalAddress == 0UL)
                return flag;
            *(uint*)(this.H2RamPhysicalAddress + (ulong)spaceOffset) = value;
            return true;
        }
    }
}
