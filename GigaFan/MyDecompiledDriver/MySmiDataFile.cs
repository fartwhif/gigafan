﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EasyFunctions.DataStructure.SmiDataFile
// Assembly: Gigabyte.EasyFunctions, Version=7.2.0.24, Culture=neutral, PublicKeyToken=null
// MVID: 78B279F9-3DF2-44F7-BFAE-CD74B7F9DD3F
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EasyFunctions.dll


using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class SmiDataFile
    {
        private const string XML_CONFIG_FILE_NAME = "EasyTable.xml";

        public SmiDataFile()
        {
            this.FilePath = Path.Combine(MyWorkingDirectory.GetCurrentDirectory(true), "EasyTable.xml");
        }

        public string FilePath { get; set; }

        public bool FileExists
        {
            get
            {
                bool flag = false;
                return string.IsNullOrEmpty(this.FilePath) ? flag : File.Exists(this.FilePath);
            }
        }

        public void Read(ref SmiDataCollection pSmiDatas)
        {
            if (this.FilePath.Length == 0)
                throw new FileNotFoundException();
            if (!File.Exists(this.FilePath))
                throw new FileNotFoundException();
            if (pSmiDatas.Count > 0)
                pSmiDatas.Clear();
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(this.FilePath);
                XmlNodeList elementsByTagName1 = xmlDocument.GetElementsByTagName("Signature");
                XmlNodeList elementsByTagName2 = xmlDocument.GetElementsByTagName("Supported");
                XmlNodeList elementsByTagName3 = xmlDocument.GetElementsByTagName("Port");
                XmlNodeList elementsByTagName4 = xmlDocument.GetElementsByTagName("BufferPhysicalAddress");
                for (int i = 0; i < elementsByTagName1.Count; ++i)
                    pSmiDatas.Add(new MySmiData()
                    {
                        Signature = elementsByTagName1[i].InnerText,
                        Supported = bool.Parse(elementsByTagName2[i].InnerText),
                        Port = int.Parse(elementsByTagName3[i].InnerText),
                        BufferPhysicalAddress = uint.Parse(elementsByTagName4[i].InnerText)
                    });
            }
            catch (Exception ex)
            {
                Console.WriteLine("[{0}] Error: {1}", (object)MethodBase.GetCurrentMethod().Name, (object)ex.Message);
                throw;
            }
        }

        public void Write(ref SmiDataCollection pSmiDatas)
        {
            if (this.FilePath.Length == 0)
                throw new FileNotFoundException();
            if (pSmiDatas.Count == 0)
                throw new ArgumentException();
            if (File.Exists(this.FilePath))
                File.Delete(this.FilePath);
            try
            {
                XElement xelement1 = new XElement((XName)"SmiDatas");
                foreach (object obj in (List<MySmiData>)pSmiDatas)
                {
                    XElement xelement2 = obj.ToXElement<MySmiData>();
                    xelement1.Add((object)xelement2);
                }
                xelement1.Save(this.FilePath);
                if ((File.GetAttributes(this.FilePath) & FileAttributes.Hidden) == FileAttributes.Hidden)
                    return;
                File.SetAttributes(this.FilePath, File.GetAttributes(this.FilePath) | FileAttributes.Hidden);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[{0}] Error: {1}", (object)MethodBase.GetCurrentMethod().Name, (object)ex.Message);
                throw;
            }
        }

        private MySmiData DeSerializer(XElement element)
        {
            return (MySmiData)new XmlSerializer(typeof(MySmiData)).Deserialize(element.CreateReader());
        }
    }
}
