﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.FanSpeedData
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System.ComponentModel;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MyFanSpeedData : INotifyPropertyChanged
    {
        private float m_pwmPercetage;
        private float m_rpm;

        public MyFanSpeedData()
        {
            this.PwmPercetage = 0.0f;
            this.RPM = 0.0f;
        }

        public float PwmPercetage
        {
            get => this.m_pwmPercetage;
            set
            {
                if ((double)this.m_pwmPercetage == (double)value)
                    return;
                this.m_pwmPercetage = value;
                this.onPropertyChanged((object)this, nameof(PwmPercetage));
            }
        }

        public float RPM
        {
            get => this.m_rpm;
            set
            {
                if ((double)this.m_rpm == (double)value)
                    return;
                this.m_rpm = value;
                this.onPropertyChanged((object)this, nameof(RPM));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void onPropertyChanged(object sender, string propertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged(sender, new PropertyChangedEventArgs(propertyName));
        }
    }
}
