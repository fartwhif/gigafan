﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.ITH2RamSmartFan
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Collections.Generic;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyITH2RamSmartFan : MyITH2RamBase
    {
        private const int MAX_TEMPERATURE_LEVEL = 50;
        private const int VAL_TEMPERATURE_STEP = 2;
        private const int VAL_TEMPERATURE_BOUNDARY_BEGIN = 2;
        private const int VAL_TEMPERATURE_BOUNDARY_END = 100;
        private const int VAL_TEMPERATURE_FIXED_MODE_FULL_SPEED = 70;
        private const int VAL_TEMPERATURE_SMART_MODE_ONE_POINT = 0;
        private const int VAL_TEMPERATURE_SMART_MODE_TWO_POINT = 20;
        private const int VAL_TEMPERATURE_SMART_MODE_THREE_POINT = 40;
        private const int VAL_TEMPERATURE_SMART_MODE_FOUR_POINT = 60;
        private const int VAL_TEMPERATURE_SMART_MODE_FIVE_POINT = 80;
        private const int VAL_TEMPERATURE_SMART_MODE_SIX_POINT = 100;
        private const byte MAX_VOLTAGE_INPUTS = 9;
        private const byte MAX_TEMPERATURE_INPUTS = 3;
        private const byte MAX_FAN_TACHOMETER_INPUTS = 3;
        private const byte MAX_FAN_CONTROL_REGISTER = 3;
        private const int MAX_PWM_VALUE = 255;
        private const int MAX_EC_SPACE_BUFFER_SIZE = 255;
        private const int EC_SPACE_OFFSET_BASE = 2304;
        private const int EC_SPACE_OFFSET_RPM1_HIGH = 0;
        private const int EC_SPACE_OFFSET_RPM1_LOW = 1;
        private const int EC_SPACE_OFFSET_RPM2_HIGH = 2;
        private const int EC_SPACE_OFFSET_RPM2_LOW = 3;
        private const int EC_SPACE_OFFSET_RPM3_HIGH = 4;
        private const int EC_SPACE_OFFSET_RPM3_LOW = 5;
        private const int EC_SPACE_OFFSET_TEMPERATURE1 = 6;
        private const int EC_SPACE_OFFSET_TEMPERATURE2 = 7;
        private const int EC_SPACE_OFFSET_TEMPERATURE3 = 8;
        private const int EC_SPACE_OFFSET_DEBUG_TEMPERATURE1 = 9;
        private const int EC_SPACE_OFFSET_DEBUG_TEMPERATURE2 = 10;
        private const int EC_SPACE_OFFSET_DEBUG_TEMPERATURE3 = 11;
        private const int EC_SPACE_OFFSET_FAN_LEVEL11 = 12;
        private const int EC_SPACE_OFFSET_FAN_LEVEL12 = 13;
        private const int EC_SPACE_OFFSET_FAN_LEVEL13 = 14;
        private const int EC_SPACE_OFFSET_FAN_FULL_ON_CONT = 15;
        private const int EC_SPACE_OFFSET_FAN1_TARGET_RPM_HIGH = 16;
        private const int EC_SPACE_OFFSET_FAN1_TARGET_RPM_LOW = 17;
        private const int EC_SPACE_OFFSET_DEBUG_FAN1_RPMT_HIGH = 18;
        private const int EC_SPACE_OFFSET_DEBUG_FAN1_RPMT_LOW = 19;
        private const int EC_SPACE_OFFSET_FAN1_DELAY_ADJUST = 20;
        private const int EC_SPACE_OFFSET_FAN2_TARGET_RPM_HIGH = 21;
        private const int EC_SPACE_OFFSET_FAN2_TARGET_RPM_LOW = 22;
        private const int EC_SPACE_OFFSET_DEBUG_FAN2_RPMT_HIGH = 23;
        private const int EC_SPACE_OFFSET_DEBUG_FAN2_RPMT_LOW = 24;
        private const int EC_SPACE_OFFSET_FAN2_DELAY_ADJUST = 16;
        private const int EC_SPACE_OFFSET_FAN3_TARGET_RPM_HIGH = 26;
        private const int EC_SPACE_OFFSET_FAN3_TARGET_RPM_LOW = 27;
        private const int EC_SPACE_OFFSET_DEBUG_FAN3_RPMT_HIGH = 28;
        private const int EC_SPACE_OFFSET_DEBUG_FAN3_RPMT_LOW = 29;
        private const int EC_SPACE_OFFSET_FAN3_DELAY_ADJUST = 30;
        private const int EC_SPACE_OFFSET_DEBUG_DUTY = 31;
        private const int EC_SPACE_OFFSET_FAN1_RPM_STEP_UP = 32;
        private const int EC_SPACE_OFFSET_FAN1_RPM_STEP_DOWN = 33;
        private const int EC_SPACE_OFFSET_FAN2_RPM_STEP_UP = 34;
        private const int EC_SPACE_OFFSET_FAN2_RPM_STEP_DOWN = 35;
        private const int EC_SPACE_OFFSET_FAN3_RPM_STEP_UP = 36;
        private const int EC_SPACE_OFFSET_FAN3_RPM_STEP_DOWN = 37;
        private const int EC_SPACE_OFFSET_FAN1_CTRL = 38;
        private const int EC_SPACE_OFFSET_FAN2_CTRL = 39;
        private const int EC_SPACE_OFFSET_FAN3_CTRL = 40;
        private const int EC_SPACE_OFFSET_RESERVED1 = 41;
        private const int EC_SPACE_OFFSET_FAN1_LEARNING_AVG_RPM_HIGH = 42;
        private const int EC_SPACE_OFFSET_FAN1_LEARNING_AVG_RPM_LOW = 43;
        private const int EC_SPACE_OFFSET_FAN2_LEARNING_AVG_RPM_HIGH = 44;
        private const int EC_SPACE_OFFSET_FAN2_LEARNING_AVG_RPM_LOW = 45;
        private const int EC_SPACE_OFFSET_FAN3_LEARNING_AVG_RPM_HIGH = 46;
        private const int EC_SPACE_OFFSET_FAN3_LEARNING_AVG_RPM_LOW = 47;
        private const int EC_SPACE_OFFSET_FAN1_RPM_STEP_MIN_HIGH = 48;
        private const int EC_SPACE_OFFSET_FAN1_RPM_STEP_MIN_LOW = 49;
        private const int EC_SPACE_OFFSET_FAN1_RPM_STEP_MAX_HIGH = 50;
        private const int EC_SPACE_OFFSET_FAN1_RPM_STEP_MAX_LOW = 51;
        private const int EC_SPACE_OFFSET_FAN2_RPM_STEP_MIN_HIGH = 52;
        private const int EC_SPACE_OFFSET_FAN2_RPM_STEP_MIN_LOW = 53;
        private const int EC_SPACE_OFFSET_FAN2_RPM_STEP_MAX_HIGH = 54;
        private const int EC_SPACE_OFFSET_FAN2_RPM_STEP_MAX_LOW = 55;
        private const int EC_SPACE_OFFSET_FAN3_RPM_STEP_MIN_HIGH = 56;
        private const int EC_SPACE_OFFSET_FAN3_RPM_STEP_MIN_LOW = 57;
        private const int EC_SPACE_OFFSET_FAN3_RPM_STEP_MAX_HIGH = 58;
        private const int EC_SPACE_OFFSET_FAN3_RPM_STEP_MAX_LOW = 59;
        private const int EC_SPACE_OFFSET_FAN1_LEARNING_TIMER = 60;
        private const int EC_SPACE_OFFSET_FAN2_LEARNING_TIMER = 61;
        private const int EC_SPACE_OFFSET_FAN3_LEARNING_TIMER = 62;
        private const int EC_SPACE_OFFSET_RESERVED2 = 63;
        private const int EC_SPACE_OFFSET_TEMPERATURE_START = 64;
        private const int EC_SPACE_OFFSET_TEMPERATURE_GAP = 65;
        private const int EC_SPACE_OFFSET_RESERVED3 = 66;
        private const int EC_SPACE_OFFSET_SMARTFAN_FEATURE_CONTROL = 71;
        private const int EC_SPACE_OFFSET_FAN1_DUTY_STEP1_BASE = 80;
        private const int EC_SPACE_OFFSET_RESERVED4 = 130;
        private const int EC_SPACE_OFFSET_FAN2_DUTY_STEP1_BASE = 136;
        private const int EC_SPACE_OFFSET_RESERVED5 = 186;
        private const int EC_SPACE_OFFSET_FAN3_DUTY_STEP1_BASE = 192;
        private const int EC_SPACE_OFFSET_RESERVED6 = 242;
        protected List<float> m_fanSpeeds;
        protected List<float> m_temperatures;

        public MyITH2RamSmartFan(
          MyChip chip,
          ushort baseAddress,
          ushort gpioAddress,
          ushort h2ramAddressBits,
          byte version)
        {
            bool flag = false;
            if (chip == MyChip.IT8790F || chip == MyChip.IT8791E)
                flag = true;
            if (!flag)
                throw new NotSupportedException();
            this.BaseAddress = baseAddress;
            this.chip = chip;
            this.Version = version;
            this.AddressRegister = (ushort)((uint)baseAddress + 5U);
            this.DataRegister = (ushort)((uint)baseAddress + 6U);
            this.GpioAddress = gpioAddress;
            this.Supported = false;
            bool valid;
            byte num = this.ReadByte((byte)88, out valid);
            if (!valid || num != (byte)144 || ((int)this.ReadByte((byte)0, out valid) & 16) == 0 || !valid)
                return;
            this.H2RamAddressBits = h2ramAddressBits;
            this.H2RamBaseAddress = (uint)(-16777216 | (int)h2ramAddressBits << 16) + 2304U;
            this.H2RamPhysicalAddress = 0UL;
            this.FanControlRegisterCount = 3;
            this.ActualFanControlRegisterCount = 3;
            this.FanTachometerCount = this.ActualFanControlRegisterCount;
            this.TemperatureCount = 3;
            this.VoltageCount = 0;
            this.MaxPwmValue = (int)byte.MaxValue;
            this.Supported = true;
            int pMainVersion = 0;
            int pSubVersion = 0;
            this.RetrieveFirmwareVersion(out pMainVersion, out pSubVersion);
            this.FirmwareMainVersion = pMainVersion;
            this.FirmwareSubVersion = pSubVersion;
        }

        public int FirmwareMainVersion { get; protected set; }

        public int FirmwareSubVersion { get; protected set; }

        public int MaxPwmValue { get; protected set; }

        public int FanControlRegisterCount { get; protected set; }

        public int ActualFanControlRegisterCount { get; protected set; }

        public int FanTachometerCount { get; protected set; }

        public int VoltageCount { get; protected set; }

        public int TemperatureCount { get; protected set; }

        public int GpioCount { get; protected set; }

        public float VoltageGain { get; protected set; }

        public bool Has16bitFanCounter { get; protected set; }

        public void Update()
        {
            float num = 0.0f;
            if (!this.Supported)
                throw new NotSupportedException();
            try
            {
                this.EnableShareMemory(true);
                if (this.m_temperatures == null)
                    this.m_temperatures = new List<float>();
                if (this.m_temperatures.Count > 0)
                    this.m_temperatures.Clear();
                for (int sensorlIndex = 0; sensorlIndex < this.TemperatureCount; ++sensorlIndex)
                {
                    this.RetrieveTemperature(sensorlIndex, out num);
                    this.m_temperatures.Add(num);
                }
                if (this.m_fanSpeeds == null)
                    this.m_fanSpeeds = new List<float>();
                if (this.m_fanSpeeds.Count > 0)
                    this.m_fanSpeeds.Clear();
                for (int fanCtrlIndex = 0; fanCtrlIndex < this.FanTachometerCount; ++fanCtrlIndex)
                {
                    this.RetrieveFanSpeed(fanCtrlIndex, out num);
                    if ((double)num == 5.0)
                        num = 0.0f;
                    this.m_fanSpeeds.Add(num);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void GetFanSmartGuardianConfig(
          int fanCtrlIndex,
          int slopeCount,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            if (!this.Supported)
                throw new NotSupportedException();
            try
            {
                this.EnableShareMemory(true);
                switch (slopeCount)
                {
                    case 1:
                        this.RetrieveFanSmartGuardianConfigFor1Slope(fanCtrlIndex, ref pSmartGuardianFanConfigs);
                        break;
                    case 2:
                        this.RetrieveFanSmartGuardianConfigFor2Slope(fanCtrlIndex, ref pSmartGuardianFanConfigs);
                        break;
                    case 3:
                        this.RetrieveFanSmartGuardianConfigFor3Slope(fanCtrlIndex, ref pSmartGuardianFanConfigs);
                        break;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void SetFanSmartGuardianConfig(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (!this.Supported)
                throw new NotSupportedException();
            try
            {
                switch (smartGuardianFanConfigs.Count)
                {
                    case 1:
                        this.EnableShareMemory(true);
                        this.UpdateFanSmartGuardianConfigFor1Slope(fanCtrlIndex, ref smartGuardianFanConfigs);
                        break;
                    case 2:
                        this.EnableShareMemory(true);
                        this.UpdateFanSmartGuardianConfigFor2Slope(fanCtrlIndex, ref smartGuardianFanConfigs);
                        break;
                    case 3:
                        this.EnableShareMemory(true);
                        this.UpdateFanSmartGuardianConfigFor3Slope(fanCtrlIndex, ref smartGuardianFanConfigs);
                        break;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void SetFanSmartGuardianConfig(int fanCtrlIndex, int dutyCycleValue)
        {
            if (!this.Supported)
                throw new NotSupportedException();
            try
            {
                this.EnableShareMemory(true);
                this.UpdateFanSmartGuardianConfig(fanCtrlIndex, dutyCycleValue);
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void SetCalibrationPwm(int dutyCycleValue)
        {
            if (!this.Supported)
                throw new NotSupportedException();
            try
            {
                this.EnableShareMemory(true);
                int dutyCycleValue1 = dutyCycleValue;
                if (dutyCycleValue1 == 0)
                    dutyCycleValue1 = 1;
                this.UpdateFanCalibrationPwm(dutyCycleValue1);
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void ResetCalibrationPwm()
        {
            if (!this.Supported)
                throw new NotSupportedException();
            try
            {
                this.EnableShareMemory(true);
                this.UpdateFanCalibrationPwm(0);
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void StartLearningMode(bool bFindMaximum, bool bFindMinimum)
        {
            if (!this.Supported)
                throw new NotSupportedException();
            try
            {
                this.EnableShareMemory(true);
                for (int fanCtrlIndex = 0; fanCtrlIndex < this.FanControlRegisterCount; ++fanCtrlIndex)
                    this.StartLearningMode(fanCtrlIndex, bFindMaximum, bFindMinimum);
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void StartLearningMode(int fanCtrlIndex, bool bFindMaximum, bool bFindMinimum)
        {
            int spaceOffset = 0;
            if (!this.Supported)
                throw new NotSupportedException();
            if (fanCtrlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            if (!bFindMaximum && !bFindMinimum)
                return;
            switch (fanCtrlIndex)
            {
                case 0:
                    spaceOffset = 38;
                    break;
                case 1:
                    spaceOffset = 39;
                    break;
                case 2:
                    spaceOffset = 40;
                    break;
            }
            try
            {
                byte num = (byte)((bFindMinimum ? 1 : 0) | (bFindMaximum ? 2 : 0));
                this.WriteByteEx(spaceOffset, num);
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void SetTemperatureStartForDutyCycleArray(int temperatureStart)
        {
            if (!this.Supported)
                throw new NotSupportedException();
            try
            {
                this.EnableShareMemory(true);
                this.WriteByteEx(64, (byte)temperatureStart);
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void SetTemperatureGapForDutyCycleArray(int temperatureGap)
        {
            if (!this.Supported)
                throw new NotSupportedException();
            if (temperatureGap == 0)
                throw new ArgumentException();
            try
            {
                this.WriteByteEx(65, (byte)temperatureGap);
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        public void GetTemperatureInputSelection(
          int fanCtrlIndex,
          ref MyFanTemperatureInput pTemperatureInputSelection)
        {
            if (!this.Supported)
                throw new NotSupportedException();
            switch (fanCtrlIndex)
            {
                case 0:
                case 1:
                    pTemperatureInputSelection = MyFanTemperatureInput.CPU;
                    break;
                case 2:
                    pTemperatureInputSelection = MyFanTemperatureInput.SYSTEM;
                    break;
            }
        }

        public void GetFanHardwareMonitorDatas(ref List<float> pHardwareMonitorDatas)
        {
            if (this.m_fanSpeeds == null || pHardwareMonitorDatas == null || this.m_fanSpeeds.Count <= 0)
                return;
            if (pHardwareMonitorDatas.Count > 0)
                pHardwareMonitorDatas.Clear();
            foreach (float fanSpeed in this.m_fanSpeeds)
                pHardwareMonitorDatas.Add(fanSpeed);
        }

        public void GetTemperatureHardwareMonitorDatas(ref List<float> pHardwareMonitorDatas)
        {
            if (this.m_temperatures == null || pHardwareMonitorDatas == null || this.m_temperatures.Count <= 0)
                return;
            if (pHardwareMonitorDatas.Count > 0)
                pHardwareMonitorDatas.Clear();
            foreach (float temperature in this.m_temperatures)
                pHardwareMonitorDatas.Add(temperature);
        }

        public void GetVoltageHardwareMonitorDatas(ref List<float> pHardwareMonitorDatas)
        {
        }

        public void EnableSmartFanControl(bool bEnable) => this.EnableEC4SmartFanControl(bEnable);

        private void EnableShareMemory(bool bEnable)
        {
            this.EnableShareMemory(bEnable, (uint)byte.MaxValue);
        }

        private void RetrieveTemperature(int sensorlIndex, out float currentTemperature)
        {
            int spaceOffset = 0;
            bool valid = false;
            currentTemperature = 0.0f;
            if (sensorlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            switch (sensorlIndex)
            {
                case 0:
                    spaceOffset = 6;
                    break;
                case 1:
                    spaceOffset = 7;
                    break;
                case 2:
                    spaceOffset = 8;
                    break;
            }
            byte num = this.ReadByteEx(spaceOffset, out valid);
            if (!valid)
                return;
            currentTemperature = (float)num;
        }

        private void RetrieveFanSpeed(int fanCtrlIndex, out float fanSpeed)
        {
            int spaceOffset1 = 0;
            int spaceOffset2 = 0;
            bool valid = false;
            fanSpeed = 0.0f;
            if (fanCtrlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            switch (fanCtrlIndex)
            {
                case 0:
                    spaceOffset1 = 0;
                    spaceOffset2 = 1;
                    break;
                case 1:
                    spaceOffset1 = 2;
                    spaceOffset2 = 3;
                    break;
                case 2:
                    spaceOffset1 = 4;
                    spaceOffset2 = 5;
                    break;
            }
            byte num1 = this.ReadByteEx(spaceOffset1, out valid);
            if (!valid)
                return;
            byte num2 = this.ReadByteEx(spaceOffset2, out valid);
            if (!valid)
                return;
            fanSpeed = (float)((int)num1 << 8 | (int)num2);
        }

        private void RetrieveFanSmartGuardianConfigFor1Slope(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            int num1 = 0;
            bool valid = false;
            if (fanCtrlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            if (pSmartGuardianFanConfigs.Count > 0)
                pSmartGuardianFanConfigs.Clear();
            switch (fanCtrlIndex)
            {
                case 0:
                    num1 = 80;
                    break;
                case 1:
                    num1 = 136;
                    break;
                case 2:
                    num1 = 192;
                    break;
            }
            int num2 = 9;
            byte num3 = this.ReadByteEx(num1 + num2, out valid);
            MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
            guardianFanConfig.StartPWM = num3;
            guardianFanConfig.TemperatureLimitValueOfFanOff = (byte)0;
            guardianFanConfig.TemperatureLimitValueOfFanStart = (byte)20;
            guardianFanConfig.DeltaTemperature = (byte)3;
            guardianFanConfig.TargetZoneBoundary = (byte)0;
            guardianFanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)40;
            int num4 = 29;
            byte num5 = this.ReadByteEx(num1 + num4, out valid);
            float num6 = 20f;
            float num7 = (float)num3;
            float num8 = 60f;
            float num9 = (float)(((double)num5 - (double)num7) / ((double)num8 - (double)num6));
            guardianFanConfig.Slope = num9;
            pSmartGuardianFanConfigs.Add(guardianFanConfig);
        }

        private void RetrieveFanSmartGuardianConfigFor2Slope(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            int num1 = 0;
            bool valid = false;
            byte num2 = 0;
            float num3 = 0.0f;
            float num4 = 0.0f;
            float num5 = 0.0f;
            float num6 = 0.0f;
            float num7 = 0.0f;
            if (fanCtrlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            if (pSmartGuardianFanConfigs.Count > 0)
                pSmartGuardianFanConfigs.Clear();
            switch (fanCtrlIndex)
            {
                case 0:
                    num1 = 80;
                    break;
                case 1:
                    num1 = 136;
                    break;
                case 2:
                    num1 = 192;
                    break;
            }
            int num8 = 9;
            byte num9 = this.ReadByteEx(num1 + num8, out valid);
            MySmartGuardianFanConfig guardianFanConfig1 = new MySmartGuardianFanConfig();
            guardianFanConfig1.StartPWM = num9;
            guardianFanConfig1.TemperatureLimitValueOfFanOff = (byte)0;
            guardianFanConfig1.TemperatureLimitValueOfFanStart = (byte)20;
            guardianFanConfig1.DeltaTemperature = (byte)3;
            guardianFanConfig1.TargetZoneBoundary = (byte)0;
            guardianFanConfig1.TemperatureLimitValueOfFanFullSpeed = (byte)40;
            int num10 = 19;
            int spaceOffset = num1 + num10;
            byte num11 = this.ReadByteEx(spaceOffset, out valid);
            float num12 = 20f;
            float num13 = (float)num9;
            float num14 = 40f;
            float num15 = (float)(((double)num11 - (double)num13) / ((double)num14 - (double)num12));
            guardianFanConfig1.Slope = num15;
            double num16;
            num6 = (float)(num16 = 0.0);
            num5 = (float)num16;
            num4 = (float)num16;
            num3 = (float)num16;
            num7 = (float)num16;
            byte num17 = num11;
            num2 = (byte)0;
            MySmartGuardianFanConfig guardianFanConfig2 = new MySmartGuardianFanConfig();
            guardianFanConfig2.StartPWM = num17;
            guardianFanConfig2.TemperatureLimitValueOfFanOff = (byte)0;
            guardianFanConfig2.TemperatureLimitValueOfFanStart = (byte)40;
            guardianFanConfig2.DeltaTemperature = (byte)3;
            guardianFanConfig2.TargetZoneBoundary = (byte)0;
            guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed = (byte)60;
            byte num18 = this.ReadByteEx(spaceOffset, out valid);
            float num19 = 40f;
            float num20 = (float)num17;
            float num21 = 60f;
            float num22 = (float)(((double)num18 - (double)num20) / ((double)num21 - (double)num19));
            guardianFanConfig2.Slope = num22;
            pSmartGuardianFanConfigs.Add(guardianFanConfig1);
            pSmartGuardianFanConfigs.Add(guardianFanConfig2);
        }

        private void RetrieveFanSmartGuardianConfigFor3Slope(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            int num1 = 0;
            bool valid = false;
            byte num2 = 0;
            float num3 = 0.0f;
            float num4 = 0.0f;
            float num5 = 0.0f;
            float num6 = 0.0f;
            float num7 = 0.0f;
            if (fanCtrlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            if (pSmartGuardianFanConfigs.Count > 0)
                pSmartGuardianFanConfigs.Clear();
            switch (fanCtrlIndex)
            {
                case 0:
                    num1 = 80;
                    break;
                case 1:
                    num1 = 136;
                    break;
                case 2:
                    num1 = 192;
                    break;
            }
            int num8 = 9;
            byte num9 = this.ReadByteEx(num1 + num8, out valid);
            MySmartGuardianFanConfig guardianFanConfig1 = new MySmartGuardianFanConfig();
            guardianFanConfig1.StartPWM = num9;
            guardianFanConfig1.TemperatureLimitValueOfFanOff = (byte)0;
            guardianFanConfig1.TemperatureLimitValueOfFanStart = (byte)20;
            guardianFanConfig1.DeltaTemperature = (byte)3;
            guardianFanConfig1.TargetZoneBoundary = (byte)0;
            guardianFanConfig1.TemperatureLimitValueOfFanFullSpeed = (byte)40;
            int num10 = 19;
            byte num11 = this.ReadByteEx(num1 + num10, out valid);
            float num12 = 20f;
            float num13 = (float)num9;
            float num14 = 40f;
            float num15 = (float)(((double)num11 - (double)num13) / ((double)num14 - (double)num12));
            guardianFanConfig1.Slope = num15;
            double num16;
            num6 = (float)(num16 = 0.0);
            num5 = (float)num16;
            num4 = (float)num16;
            num3 = (float)num16;
            num7 = (float)num16;
            byte num17 = num11;
            num2 = (byte)0;
            MySmartGuardianFanConfig guardianFanConfig2 = new MySmartGuardianFanConfig();
            guardianFanConfig2.StartPWM = num17;
            guardianFanConfig2.TemperatureLimitValueOfFanOff = (byte)0;
            guardianFanConfig2.TemperatureLimitValueOfFanStart = (byte)40;
            guardianFanConfig2.DeltaTemperature = (byte)3;
            guardianFanConfig2.TargetZoneBoundary = (byte)0;
            guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed = (byte)60;
            int num18 = 29;
            byte num19 = this.ReadByteEx(num1 + num18, out valid);
            float num20 = 40f;
            float num21 = (float)num17;
            float num22 = 60f;
            float num23 = (float)(((double)num19 - (double)num21) / ((double)num22 - (double)num20));
            guardianFanConfig2.Slope = num23;
            double num24;
            num6 = (float)(num24 = 0.0);
            num5 = (float)num24;
            num4 = (float)num24;
            num3 = (float)num24;
            num7 = (float)num24;
            byte num25 = num19;
            num2 = (byte)0;
            MySmartGuardianFanConfig guardianFanConfig3 = new MySmartGuardianFanConfig();
            guardianFanConfig3.StartPWM = num25;
            guardianFanConfig3.TemperatureLimitValueOfFanOff = (byte)0;
            guardianFanConfig3.TemperatureLimitValueOfFanStart = (byte)60;
            guardianFanConfig3.DeltaTemperature = (byte)3;
            guardianFanConfig3.TargetZoneBoundary = (byte)0;
            guardianFanConfig3.TemperatureLimitValueOfFanFullSpeed = (byte)80;
            int num26 = 39;
            byte num27 = this.ReadByteEx(num1 + num26, out valid);
            float num28 = 60f;
            float num29 = (float)num25;
            float num30 = 80f;
            float num31 = (float)(((double)num27 - (double)num29) / ((double)num30 - (double)num28));
            guardianFanConfig3.Slope = num31;
            pSmartGuardianFanConfigs.Add(guardianFanConfig1);
            pSmartGuardianFanConfigs.Add(guardianFanConfig2);
            pSmartGuardianFanConfigs.Add(guardianFanConfig3);
        }

        private void UpdateFanSmartGuardianConfigFor1Slope(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            int num1 = 0;
            int num2 = 0;
            float num3 = 0.0f;
            if (fanCtrlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            if (smartGuardianFanConfigs.Count != 1)
                return;
            MySmartGuardianFanConfig guardianFanConfig = smartGuardianFanConfigs[0];
            if (guardianFanConfig.TemperatureLimitValueOfFanStart < (byte)2)
                throw new ArgumentException();
            if (guardianFanConfig.TemperatureLimitValueOfFanFullSpeed > (byte)100)
                throw new ArgumentException();
            switch (fanCtrlIndex)
            {
                case 0:
                    num1 = 80;
                    break;
                case 1:
                    num1 = 136;
                    break;
                case 2:
                    num1 = 192;
                    break;
            }
            if (guardianFanConfig.TemperatureLimitValueOfFanStart > (byte)2)
            {
                int num4 = ((int)guardianFanConfig.TemperatureLimitValueOfFanStart - 2) / 2;
                int num5 = (100 - (int)guardianFanConfig.TemperatureLimitValueOfFanFullSpeed) / 2;
                float slope = guardianFanConfig.Slope;
                int num6 = 2;
                for (int index = 0; index <= num4; ++index)
                {
                    num3 = (float)guardianFanConfig.StartPWM;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
                for (int index = 0; index <= num5; ++index)
                {
                    float num7 = index == 0 ? (float)guardianFanConfig.StartPWM : num3;
                    num3 = slope * (float)num6 + num7;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
            }
            else
            {
                float slope = guardianFanConfig.Slope;
                int num8 = 2;
                int num9 = ((int)guardianFanConfig.TemperatureLimitValueOfFanFullSpeed - (int)guardianFanConfig.TemperatureLimitValueOfFanStart) / 2;
                for (int index = 0; index < num9; ++index)
                {
                    float num10 = index == 0 ? (float)guardianFanConfig.StartPWM : num3;
                    num3 = slope * (float)num8 + num10;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
            }
        }

        private void UpdateFanSmartGuardianConfigFor2Slope(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            int num1 = 0;
            int num2 = 0;
            float num3 = 0.0f;
            if (fanCtrlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            if (smartGuardianFanConfigs.Count != 2)
                return;
            MySmartGuardianFanConfig guardianFanConfig1 = smartGuardianFanConfigs[0];
            if (guardianFanConfig1.TemperatureLimitValueOfFanStart < (byte)2)
                throw new ArgumentException();
            MySmartGuardianFanConfig guardianFanConfig2 = smartGuardianFanConfigs[1];
            if (guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed > (byte)100)
                throw new ArgumentException();
            switch (fanCtrlIndex)
            {
                case 0:
                    num1 = 80;
                    break;
                case 1:
                    num1 = 136;
                    break;
                case 2:
                    num1 = 192;
                    break;
            }
            if (guardianFanConfig1.TemperatureLimitValueOfFanStart > (byte)2)
            {
                int num4 = ((int)guardianFanConfig1.TemperatureLimitValueOfFanStart - 2) / 2;
                int num5 = ((int)guardianFanConfig1.TemperatureLimitValueOfFanFullSpeed - (int)guardianFanConfig1.TemperatureLimitValueOfFanStart) / 2;
                float slope = guardianFanConfig1.Slope;
                int num6 = 2;
                for (int index = 0; index < num4; ++index)
                {
                    num3 = (float)guardianFanConfig1.StartPWM;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
                for (int index = 0; index < num5; ++index)
                {
                    float num7 = index == 0 ? (float)guardianFanConfig1.StartPWM : num3;
                    num3 = slope * (float)num6 + num7;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
            }
            else
            {
                float slope = guardianFanConfig1.Slope;
                int num8 = 2;
                int num9 = ((int)guardianFanConfig1.TemperatureLimitValueOfFanFullSpeed - (int)guardianFanConfig1.TemperatureLimitValueOfFanStart) / 2;
                for (int index = 0; index < num9; ++index)
                {
                    float num10 = index == 0 ? (float)guardianFanConfig1.StartPWM : num3;
                    num3 = slope * (float)num8 + num10;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
            }
            if (guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed < (byte)100)
            {
                int num11 = ((int)guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed - (int)guardianFanConfig2.TemperatureLimitValueOfFanStart) / 2;
                int num12 = (100 - (int)guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed) / 2;
                float slope = guardianFanConfig2.Slope;
                int num13 = 2;
                for (int index = 0; index < num11; ++index)
                {
                    float num14 = index == 0 ? (float)guardianFanConfig2.StartPWM : num3;
                    num3 = slope * (float)num13 + num14;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
                for (int index = 0; index < num12; ++index)
                {
                    float maxValue = (float)byte.MaxValue;
                    this.WriteByteEx(num1 + num2++, (byte)maxValue);
                }
            }
            else
            {
                float slope = guardianFanConfig2.Slope;
                int num15 = 2;
                int num16 = (100 - (int)guardianFanConfig2.TemperatureLimitValueOfFanStart) / 2;
                for (int index = 0; index < num16; ++index)
                {
                    float num17 = index == 0 ? (float)guardianFanConfig2.StartPWM : num3;
                    num3 = slope * (float)num15 + num17;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
            }
        }

        private void UpdateFanSmartGuardianConfigFor3Slope(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            int num1 = 0;
            int num2 = 0;
            float num3 = 0.0f;
            if (fanCtrlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            if (smartGuardianFanConfigs.Count != 3)
                return;
            MySmartGuardianFanConfig guardianFanConfig1 = smartGuardianFanConfigs[0];
            if (guardianFanConfig1.TemperatureLimitValueOfFanStart < (byte)2)
                throw new ArgumentException();
            MySmartGuardianFanConfig guardianFanConfig2 = smartGuardianFanConfigs[1];
            if (guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed > (byte)100)
                throw new ArgumentException();
            MySmartGuardianFanConfig guardianFanConfig3 = smartGuardianFanConfigs[2];
            if (guardianFanConfig3.TemperatureLimitValueOfFanFullSpeed > (byte)100)
                throw new ArgumentException();
            switch (fanCtrlIndex)
            {
                case 0:
                    num1 = 80;
                    break;
                case 1:
                    num1 = 136;
                    break;
                case 2:
                    num1 = 192;
                    break;
            }
            if (guardianFanConfig1.TemperatureLimitValueOfFanStart > (byte)2)
            {
                int num4 = ((int)guardianFanConfig1.TemperatureLimitValueOfFanStart - 2) / 2;
                int num5 = ((int)guardianFanConfig1.TemperatureLimitValueOfFanFullSpeed - (int)guardianFanConfig1.TemperatureLimitValueOfFanStart) / 2;
                float slope = guardianFanConfig1.Slope;
                int num6 = 2;
                for (int index = 0; index <= num4; ++index)
                {
                    num3 = (float)guardianFanConfig1.StartPWM;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
                for (int index = 0; index <= num5; ++index)
                {
                    float num7 = index == 0 ? (float)guardianFanConfig1.StartPWM : num3;
                    num3 = slope * (float)num6 + num7;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
            }
            else
            {
                float slope = guardianFanConfig1.Slope;
                int num8 = 2;
                int num9 = ((int)guardianFanConfig1.TemperatureLimitValueOfFanFullSpeed - (int)guardianFanConfig1.TemperatureLimitValueOfFanStart) / 2;
                for (int index = 0; index <= num9; ++index)
                {
                    float num10 = index == 0 ? (float)guardianFanConfig1.StartPWM : num3;
                    num3 = slope * (float)num8 + num10;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
            }
            if (guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed < (byte)100)
            {
                float slope = guardianFanConfig2.Slope;
                int num11 = 2;
                int num12 = ((int)guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed - (int)guardianFanConfig2.TemperatureLimitValueOfFanStart - 2) / 2;
                for (int index = 0; index <= num12; ++index)
                {
                    float num13 = index == 0 ? (float)guardianFanConfig2.StartPWM : num3;
                    num3 = slope * (float)num11 + num13;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
            }
            if (guardianFanConfig3.TemperatureLimitValueOfFanFullSpeed < (byte)100)
            {
                int num14 = ((int)guardianFanConfig3.TemperatureLimitValueOfFanFullSpeed - (int)guardianFanConfig3.TemperatureLimitValueOfFanStart - 2) / 2;
                int num15 = (100 - (int)guardianFanConfig3.TemperatureLimitValueOfFanFullSpeed - 2) / 2;
                float slope = guardianFanConfig3.Slope;
                int num16 = 2;
                for (int index = 0; index <= num14; ++index)
                {
                    float num17 = index == 0 ? (float)guardianFanConfig3.StartPWM : num3;
                    num3 = slope * (float)num16 + num17;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
                for (int index = 0; index < num15; ++index)
                {
                    float maxValue = (float)byte.MaxValue;
                    this.WriteByteEx(num1 + num2++, (byte)maxValue);
                }
            }
            else
            {
                float slope = guardianFanConfig3.Slope;
                int num18 = 2;
                int num19 = (100 - (int)guardianFanConfig3.TemperatureLimitValueOfFanStart - 2) / 2;
                for (int index = 0; index < num19; ++index)
                {
                    float num20 = index == 0 ? (float)guardianFanConfig3.StartPWM : num3;
                    num3 = slope * (float)num18 + num20;
                    this.WriteByteEx(num1 + num2++, (byte)num3);
                }
            }
        }

        private void UpdateFanSmartGuardianConfig(int fanCtrlIndex, int dutyCycleValue)
        {
            int num1 = 0;
            if (fanCtrlIndex >= this.FanControlRegisterCount)
                throw new IndexOutOfRangeException();
            if (dutyCycleValue > (int)byte.MaxValue)
                throw new ArgumentException();
            switch (fanCtrlIndex)
            {
                case 0:
                    num1 = 80;
                    break;
                case 1:
                    num1 = 136;
                    break;
                case 2:
                    num1 = 192;
                    break;
            }
            int num2 = 0;
            int num3;
            for (num3 = 2; num3 <= 70; num3 += 2)
            {
                this.WriteByteEx(num1 + num2, (byte)dutyCycleValue);
                ++num2;
            }
            for (; num3 <= 100; num3 += 2)
            {
                this.WriteByteEx(num1 + num2, byte.MaxValue);
                ++num2;
            }
        }

        private void UpdateFanCalibrationPwm(int dutyCycleValue)
        {
            if (dutyCycleValue > (int)byte.MaxValue)
                throw new ArgumentException();
            this.WriteByteEx(31, (byte)dutyCycleValue);
        }

        private void RetrieveFirmwareVersion(out int pMainVersion, out int pSubVersion)
        {
            pMainVersion = 0;
            pSubVersion = 0;
            try
            {
                MyITH2RamFirmwareVersion ramFirmwareVersion = new MyITH2RamFirmwareVersion(this.chip, this.BaseAddress, this.GpioAddress, this.H2RamAddressBits, this.Version);
                if (!ramFirmwareVersion.Supported || ramFirmwareVersion.MainVersion == 0 && ramFirmwareVersion.SubVersion == 0)
                    return;
                pMainVersion = ramFirmwareVersion.MainVersion;
                pSubVersion = ramFirmwareVersion.SubVersion;
            }
            catch
            {
                pMainVersion = 0;
                pSubVersion = 0;
            }
        }

        private void EnableEC4SmartFanControl(bool bEnable)
        {
            bool valid = false;
            if (this.FirmwareMainVersion == 0)
            {
                if (this.FirmwareSubVersion == 0)
                    return;
            }
            try
            {
                this.EnableShareMemory(true);
                int spaceOffset = 71;
                byte num = this.ReadByteEx(spaceOffset, out valid);
                if (!valid)
                    return;
                if (bEnable)
                {
                    if (num == (byte)1)
                        return;
                    this.WriteByteEx(spaceOffset, (byte)1);
                }
                else
                {
                    if (num == (byte)0)
                        return;
                    this.WriteByteEx(spaceOffset, (byte)0);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
        }

        private bool CheckEnableEC4SmartFanControl()
        {
            bool flag = false;
            bool valid = false;
            if (this.FirmwareMainVersion == 0)
            {
                if (this.FirmwareSubVersion == 0)
                    return true;
            }
            try
            {
                this.EnableShareMemory(true);
                int spaceOffset = 71;
                byte num = this.ReadByteEx(spaceOffset, out valid);
                if (!valid)
                    return flag;
                if (flag)
                {
                    if (num == (byte)1)
                    {
                        flag = true;
                        return flag;
                    }
                    this.WriteByteEx(spaceOffset, (byte)1);
                }
                else
                {
                    if (num == (byte)0)
                    {
                        flag = false;
                        return flag;
                    }
                    this.WriteByteEx(spaceOffset, (byte)0);
                }
            }
            catch
            {
                flag = false;
            }
            finally
            {
                this.EnableShareMemory(false);
            }
            return flag;
        }
    }
}
