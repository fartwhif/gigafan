﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.FanCooler
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll


using System;
using System.Collections.Generic;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal abstract class MyFanCooler
    {
        protected const byte CONFIGURATION_CONTROL_REGISTER = 2;
        protected const byte DEVCIE_SELECT_REGISTER = 7;
        protected const byte CHIP_ID_REGISTER = 32;
        protected const byte CHIP_REVISION_REGISTER = 33;
        protected const byte BASE_ADDRESS_REGISTER = 96;
        protected const byte VAL_REGISTER_DEFAULT_UNKNOW = 128;
        protected const int VAL_FIXED_MODE_TEMPERATURE_FULLSPEED = 70;
        protected const int VAL_MAX_TEMPERATURE_FULLSPEED = 127;
        protected const int VAL_DEFAULT_INCREASE_PWM_STEP = 51;
        protected const int VAL_MAX_IT87XXFINDER_RETRY_TIMES = 6;
        protected const int VAL_MAX_IT87XXFINDER_RETRY_WAITTIME = 1000;
        protected const int VAL_MAX_IT8790FINDER_RETRY_TIMES = 2;
        protected readonly ushort[] REGISTER_PORTS = new ushort[2]
        {
      (ushort) 46,
      (ushort) 78
        };
        protected readonly ushort[] VALUE_PORTS = new ushort[2]
        {
      (ushort) 47,
      (ushort) 79
        };
        protected MyIT87XX m_iteMgr;

        public int FanControlCount { get; protected set; }

        public int FanCount { get; protected set; }

        public int TemperatureCount { get; protected set; }

        public int MaxPwmValue { get; protected set; }

        public bool IsSupported { get; protected set; }

        public bool UsingExtendedTemperature { get; set; }

        public int MaxSlopeCount { get; protected set; }

        public abstract void SetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs);

        public abstract void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs);

        public abstract void SetProtectValue(
          int fanControlIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs);

        public abstract void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs);

        public abstract void GetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs);

        public abstract void SetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfig smartGuardianFanConfig);

        public abstract void GetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfig pSmartGuardianFanConfig);

        public abstract void GetCpuFanSpeed(out float fanSpeed);

        public abstract void GetFanSpeed(int fanIndex, out float fanSpeed);

        public abstract void GetFanSpeed(int fanIndex, bool bSwitchFanIndex, out float fanSpeed);

        public abstract void GetFanDisplayName(int fanIndex, out string pDisplayName);

        public abstract void GetFanControlDisplayName(int fanControlIndex, out string pDisplayName);

        public abstract void SetCalibrationPwm(int fanControlIndex, byte pwmPercentage);

        public abstract void ResetCalibrationPwm();

        public abstract void SetFanSpeedFixedMode(int fanControlIndex, byte pwmPercentage);

        public abstract bool IsRequireSoftwareMonitor(int fanControlIndex);

        public abstract void GetTemperature(int temperatureIndex, out float targetTemperature);

        public abstract void GetTemperatureInputSelection(
          int fanControlIndex,
          ref MyFanTemperatureInput pTemperatureInputSelection);

        public abstract void GetTemperatureDisplayName(int temperatureIndex, out string pDisplayName);

        public abstract void GetHardwareMonitorDatas(
          ref List<float> pTemperatureDatas,
          ref List<float> pFanSpeedDatas);

        public void ConvertPwm2Percetage(int dutyCycle, out int dutyCyclePercetage)
        {
            dutyCyclePercetage = 0;
            if (dutyCycle == 0)
                return;
            float num = (float)this.MaxPwmValue / 100f;
            float d = (float)dutyCycle / num;
            dutyCyclePercetage = (int)Math.Floor((double)d);
        }

        public void ConvertPercetage2Pwm(int dutyCyclePercetage, out int dutyCycle)
        {
            dutyCycle = 0;
            if (dutyCyclePercetage == 0)
                return;
            float num = (float)this.MaxPwmValue / 100f;
            float d = (float)dutyCyclePercetage * num;
            dutyCycle = (int)Math.Floor((double)d);
        }

        protected void CalculateSlope(float x1, float y1, float x2, float y2, out float pSlope)
        {
            pSlope = 0.0f;
            if ((double)x2 == (double)x1 || (double)y2 == (double)y1)
                return;
            float num = (float)(((double)y2 - (double)y1) / ((double)x2 - (double)x1));
            pSlope = num;
        }

        protected void CalculatePwm(float slope, float x1, float y1, float x2, out float pY2)
        {
            pY2 = 0.0f;
            if ((double)slope == 0.0)
                pY2 = y1;
            else
                pY2 = slope * (x2 - x1) + y1;
        }

        protected void CalculateExtendedTemperature(
          float slope,
          float x1,
          float y1,
          float extendedPwmValue,
          out float pX2)
        {
            pX2 = 0.0f;
            if ((double)slope == 0.0)
            {
                pX2 = x1;
            }
            else
            {
                float num = y1 + extendedPwmValue;
                if ((double)num > (double)byte.MaxValue)
                    num = (float)byte.MaxValue;
                pX2 = (num - y1) / slope + x1;
            }
        }

        protected void CalculateTemperatureLimit4FanStart(
          float slope,
          float y2,
          float y1,
          float x2,
          out float pX1)
        {
            pX1 = 0.0f;
            if ((double)slope == 0.0)
                pX1 = x2;
            else if ((double)y2 == (double)y1)
            {
                pX1 = x2;
            }
            else
            {
                float num = x2 - (y2 - y1) / slope;
                pX1 = num;
            }
        }

        protected void RetrieveProtectValueForExtendedTemperature(
          ref MySmartGuardianFanConfig originalFanConfig,
          ref MySmartGuardianFanConfig newFanConfig)
        {
            float pY2 = 0.0f;
            float pX2 = 0.0f;
            this.CalculatePwm(originalFanConfig.Slope, (float)originalFanConfig.TemperatureLimitValueOfFanStart, (float)originalFanConfig.StartPWM, (float)originalFanConfig.TemperatureLimitValueOfFanFullSpeed, out pY2);
            this.CalculateExtendedTemperature(originalFanConfig.Slope, (float)originalFanConfig.TemperatureLimitValueOfFanFullSpeed, pY2, 51f, out pX2);
            if ((double)pX2 > (double)sbyte.MaxValue)
                pX2 = (float)sbyte.MaxValue;
            newFanConfig.TemperatureLimitValueOfFanOff = originalFanConfig.TemperatureLimitValueOfFanOff;
            newFanConfig.TemperatureLimitValueOfFanStart = originalFanConfig.TemperatureLimitValueOfFanStart;
            newFanConfig.TemperatureLimitValueOfFanFullSpeed = originalFanConfig.TemperatureLimitValueOfFanFullSpeed;
            newFanConfig.StartPWM = originalFanConfig.StartPWM;
            newFanConfig.Slope = originalFanConfig.Slope;
            newFanConfig.DeltaTemperature = originalFanConfig.DeltaTemperature;
            newFanConfig.TargetZoneBoundary = originalFanConfig.TargetZoneBoundary;
            newFanConfig.FanSpeed = originalFanConfig.FanSpeed;
            newFanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)pX2;
        }

        protected void RetrieveProtectValueForExtendedTemperature(
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs,
          ref MySmartGuardianFanConfig pExtendedSmartGuardianFanConfig)
        {
            float pX1 = 0.0f;
            if (smartGuardianFanConfigs.Count == 0 || slopeIndex >= smartGuardianFanConfigs.Count)
                return;
            if (smartGuardianFanConfigs.Count == 1)
            {
                MySmartGuardianFanConfig guardianFanConfig = smartGuardianFanConfigs[slopeIndex];
                pExtendedSmartGuardianFanConfig.TemperatureLimitValueOfFanOff = guardianFanConfig.TemperatureLimitValueOfFanOff;
                pExtendedSmartGuardianFanConfig.TemperatureLimitValueOfFanStart = guardianFanConfig.TemperatureLimitValueOfFanStart;
                pExtendedSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed = guardianFanConfig.TemperatureLimitValueOfFanFullSpeed;
                pExtendedSmartGuardianFanConfig.StartPWM = guardianFanConfig.StartPWM;
                pExtendedSmartGuardianFanConfig.Slope = guardianFanConfig.Slope;
                pExtendedSmartGuardianFanConfig.DeltaTemperature = guardianFanConfig.DeltaTemperature;
                pExtendedSmartGuardianFanConfig.TargetZoneBoundary = guardianFanConfig.TargetZoneBoundary;
                pExtendedSmartGuardianFanConfig.FanSpeed = guardianFanConfig.FanSpeed;
            }
            else
            {
                MySmartGuardianFanConfig guardianFanConfig1 = smartGuardianFanConfigs[slopeIndex];
                MySmartGuardianFanConfig guardianFanConfig2 = smartGuardianFanConfigs[0];
                int index = smartGuardianFanConfigs.Count - 1;
                MySmartGuardianFanConfig guardianFanConfig3 = smartGuardianFanConfigs[index];
                if (slopeIndex == 0)
                {
                    pExtendedSmartGuardianFanConfig.TemperatureLimitValueOfFanOff = guardianFanConfig1.TemperatureLimitValueOfFanOff;
                    pExtendedSmartGuardianFanConfig.TemperatureLimitValueOfFanStart = guardianFanConfig1.TemperatureLimitValueOfFanStart;
                    pExtendedSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed = guardianFanConfig3.TemperatureLimitValueOfFanFullSpeed;
                    pExtendedSmartGuardianFanConfig.StartPWM = guardianFanConfig1.StartPWM;
                    pExtendedSmartGuardianFanConfig.Slope = guardianFanConfig1.Slope;
                    pExtendedSmartGuardianFanConfig.DeltaTemperature = guardianFanConfig1.DeltaTemperature;
                    pExtendedSmartGuardianFanConfig.TargetZoneBoundary = guardianFanConfig1.TargetZoneBoundary;
                    pExtendedSmartGuardianFanConfig.FanSpeed = guardianFanConfig1.FanSpeed;
                }
                else
                {
                    this.CalculateTemperatureLimit4FanStart(guardianFanConfig1.Slope, (float)guardianFanConfig1.StartPWM, (float)guardianFanConfig2.StartPWM, (float)guardianFanConfig1.TemperatureLimitValueOfFanStart, out pX1);
                    pExtendedSmartGuardianFanConfig.TemperatureLimitValueOfFanOff = guardianFanConfig1.TemperatureLimitValueOfFanOff;
                    pExtendedSmartGuardianFanConfig.TemperatureLimitValueOfFanStart = Convert.ToByte(Math.Round((double)pX1, 0));
                    pExtendedSmartGuardianFanConfig.TemperatureLimitValueOfFanFullSpeed = guardianFanConfig3.TemperatureLimitValueOfFanFullSpeed;
                    pExtendedSmartGuardianFanConfig.StartPWM = guardianFanConfig2.StartPWM;
                    pExtendedSmartGuardianFanConfig.Slope = guardianFanConfig1.Slope;
                    pExtendedSmartGuardianFanConfig.DeltaTemperature = guardianFanConfig1.DeltaTemperature;
                    pExtendedSmartGuardianFanConfig.TargetZoneBoundary = guardianFanConfig1.TargetZoneBoundary;
                    pExtendedSmartGuardianFanConfig.FanSpeed = guardianFanConfig1.FanSpeed;
                }
            }
        }

        protected void RetrieveProtectValue(
          ref MySmartGuardianFanConfig originalFanConfig,
          int slopeCount,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            if (pSmartGuardianFanConfigs.Count > 0)
                pSmartGuardianFanConfigs.Clear();
            try
            {
                switch (slopeCount)
                {
                    case 2:
                        this.RetrieveProtectValueFor2Slope(ref originalFanConfig, ref pSmartGuardianFanConfigs);
                        break;
                    case 3:
                        this.RetrieveProtectValueFor3Slope(ref originalFanConfig, ref pSmartGuardianFanConfigs);
                        break;
                    default:
                        pSmartGuardianFanConfigs.Add(new MySmartGuardianFanConfig()
                        {
                            DeltaTemperature = originalFanConfig.DeltaTemperature,
                            FanSpeed = originalFanConfig.FanSpeed,
                            Slope = originalFanConfig.Slope,
                            StartPWM = originalFanConfig.StartPWM,
                            TargetZoneBoundary = originalFanConfig.TargetZoneBoundary,
                            TemperatureLimitValueOfFanFullSpeed = originalFanConfig.TemperatureLimitValueOfFanFullSpeed,
                            TemperatureLimitValueOfFanOff = originalFanConfig.TemperatureLimitValueOfFanOff,
                            TemperatureLimitValueOfFanStart = originalFanConfig.TemperatureLimitValueOfFanStart
                        });
                        break;
                }
            }
            catch
            {
                throw;
            }
        }

        protected void RetrieveProtectValueFor2Slope(
          ref MySmartGuardianFanConfig originalFanConfig,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            if (pSmartGuardianFanConfigs.Count > 0)
                pSmartGuardianFanConfigs.Clear();
            MySmartGuardianFanConfig guardianFanConfig1 = new MySmartGuardianFanConfig();
            guardianFanConfig1.TemperatureLimitValueOfFanOff = originalFanConfig.TemperatureLimitValueOfFanOff;
            guardianFanConfig1.TemperatureLimitValueOfFanStart = originalFanConfig.TemperatureLimitValueOfFanStart;
            guardianFanConfig1.TemperatureLimitValueOfFanFullSpeed = originalFanConfig.TemperatureLimitValueOfFanFullSpeed;
            guardianFanConfig1.StartPWM = originalFanConfig.StartPWM;
            guardianFanConfig1.Slope = originalFanConfig.Slope;
            guardianFanConfig1.DeltaTemperature = originalFanConfig.DeltaTemperature;
            guardianFanConfig1.TargetZoneBoundary = originalFanConfig.TargetZoneBoundary;
            MySmartGuardianFanConfig guardianFanConfig2 = new MySmartGuardianFanConfig();
            guardianFanConfig2.TemperatureLimitValueOfFanOff = originalFanConfig.TemperatureLimitValueOfFanOff;
            guardianFanConfig2.TemperatureLimitValueOfFanStart = originalFanConfig.TemperatureLimitValueOfFanFullSpeed;
            guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed = originalFanConfig.TemperatureLimitValueOfFanFullSpeed;
            guardianFanConfig2.Slope = 0.0f;
            guardianFanConfig2.DeltaTemperature = originalFanConfig.DeltaTemperature;
            guardianFanConfig2.TargetZoneBoundary = originalFanConfig.TargetZoneBoundary;
            double a = (double)originalFanConfig.Slope * (double)((int)originalFanConfig.TemperatureLimitValueOfFanFullSpeed - (int)originalFanConfig.TemperatureLimitValueOfFanStart) + (double)originalFanConfig.StartPWM;
            guardianFanConfig2.StartPWM = (byte)Math.Round(a);
            pSmartGuardianFanConfigs.Add(guardianFanConfig1);
            pSmartGuardianFanConfigs.Add(guardianFanConfig2);
        }

        protected void RetrieveProtectValueFor3Slope(
          ref MySmartGuardianFanConfig originalFanConfig,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            float pSlope = 0.0f;
            if (pSmartGuardianFanConfigs.Count > 0)
                pSmartGuardianFanConfigs.Clear();
            MySmartGuardianFanConfig guardianFanConfig1 = new MySmartGuardianFanConfig();
            MySmartGuardianFanConfig guardianFanConfig2 = new MySmartGuardianFanConfig();
            MySmartGuardianFanConfig guardianFanConfig3 = new MySmartGuardianFanConfig();
            if (originalFanConfig.TemperatureLimitValueOfFanOff == (byte)0)
            {
                if (originalFanConfig.TemperatureLimitValueOfFanStart == (byte)0)
                {
                    if (originalFanConfig.TemperatureLimitValueOfFanFullSpeed == (byte)0)
                    {
                        if (originalFanConfig.StartPWM == (byte)0)
                        {
                            if ((double)originalFanConfig.Slope == 0.0)
                            {
                                if (originalFanConfig.DeltaTemperature == (byte)0)
                                {
                                    if (originalFanConfig.TargetZoneBoundary == (byte)0)
                                    {
                                        pSmartGuardianFanConfigs.Add(guardianFanConfig1);
                                        pSmartGuardianFanConfigs.Add(guardianFanConfig2);
                                        pSmartGuardianFanConfigs.Add(guardianFanConfig3);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            try
            {
                guardianFanConfig1.TemperatureLimitValueOfFanOff = originalFanConfig.TemperatureLimitValueOfFanOff;
                guardianFanConfig1.TemperatureLimitValueOfFanStart = originalFanConfig.TemperatureLimitValueOfFanStart;
                guardianFanConfig1.TemperatureLimitValueOfFanFullSpeed = originalFanConfig.TemperatureLimitValueOfFanFullSpeed;
                guardianFanConfig1.StartPWM = originalFanConfig.StartPWM;
                guardianFanConfig1.Slope = originalFanConfig.Slope;
                guardianFanConfig1.DeltaTemperature = originalFanConfig.DeltaTemperature;
                guardianFanConfig1.TargetZoneBoundary = originalFanConfig.TargetZoneBoundary;
                int num1 = (int)originalFanConfig.TemperatureLimitValueOfFanFullSpeed + 4;
                guardianFanConfig2.TemperatureLimitValueOfFanOff = originalFanConfig.TemperatureLimitValueOfFanOff;
                guardianFanConfig2.TemperatureLimitValueOfFanStart = originalFanConfig.TemperatureLimitValueOfFanFullSpeed;
                guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed = (byte)num1;
                guardianFanConfig2.DeltaTemperature = originalFanConfig.DeltaTemperature;
                guardianFanConfig2.TargetZoneBoundary = originalFanConfig.TargetZoneBoundary;
                float num2 = originalFanConfig.Slope * (float)((int)originalFanConfig.TemperatureLimitValueOfFanFullSpeed - (int)originalFanConfig.TemperatureLimitValueOfFanStart) + (float)originalFanConfig.StartPWM;
                guardianFanConfig2.StartPWM = (byte)Math.Round((double)num2);
                float y2 = num2 + 4f;
                if ((double)y2 > (double)byte.MaxValue)
                    y2 = (float)byte.MaxValue;
                this.CalculateSlope((float)guardianFanConfig2.TemperatureLimitValueOfFanStart, num2, (float)guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed, y2, out pSlope);
                guardianFanConfig2.Slope = pSlope;
                guardianFanConfig3.TemperatureLimitValueOfFanOff = originalFanConfig.TemperatureLimitValueOfFanOff;
                guardianFanConfig3.TemperatureLimitValueOfFanStart = (byte)num1;
                int num3 = num1 + 4;
                guardianFanConfig3.TemperatureLimitValueOfFanFullSpeed = (byte)num3;
                guardianFanConfig3.DeltaTemperature = originalFanConfig.DeltaTemperature;
                guardianFanConfig3.TargetZoneBoundary = originalFanConfig.TargetZoneBoundary;
                float a = num2 + 4f;
                if ((double)a > (double)byte.MaxValue)
                    a = (float)byte.MaxValue;
                guardianFanConfig3.StartPWM = (byte)Math.Round((double)a);
                this.CalculateSlope((float)guardianFanConfig3.TemperatureLimitValueOfFanStart, (float)guardianFanConfig3.StartPWM, (float)guardianFanConfig3.TemperatureLimitValueOfFanFullSpeed, (float)byte.MaxValue, out pSlope);
                guardianFanConfig3.Slope = pSlope;
                pSmartGuardianFanConfigs.Add(guardianFanConfig1);
                pSmartGuardianFanConfigs.Add(guardianFanConfig2);
                pSmartGuardianFanConfigs.Add(guardianFanConfig3);
            }
            catch
            {
                throw;
            }
        }
    }
}
