﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.IT8728
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Collections.Generic;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyIT8728 : MyIT87XX
    {
        public MyIT8728(MyChip chip, ushort baseAddress, ushort gpioAddress, byte version)
        {
            if (chip != MyChip.IT8728F)
                throw new NotSupportedException();
            this.BaseAddress = baseAddress;
            this.chip = chip;
            this.Version = version;
            this.AddressRegister = (ushort)((uint)baseAddress + 5U);
            this.DataRegister = (ushort)((uint)baseAddress + 6U);
            this.GpioAddress = gpioAddress;
            bool valid;
            byte num = this.ReadByte((byte)88, out valid);
            if (!valid || num != (byte)144 || ((int)this.ReadByte((byte)0, out valid) & 16) == 0 || !valid)
                return;
            this.VoltageGain = 0.012f;
            this.Has16bitFanCounter = true;
            this.GpioCount = 0;
            this.FanControlRegisterCount = 3;
            this.ActualFanControlRegisterCount = this.RetrieveFanTachometerAmountWithoutFollowSelection();
            this.FanTachometerCount = this.RetrieveFanTachometerAmount();
            this.TemperatureCount = 3;
            this.VoltageCount = 7;
            this.MaxPwmValue = (int)byte.MaxValue;
            this.InitiObjects();
        }

        private void InitiObjects()
        {
            if (this.m_fanSpeeds == null)
                this.m_fanSpeeds = new List<float>();
            if (this.m_fanSpeeds.Count > 0)
                this.m_fanSpeeds.Clear();
            if (this.m_voltages == null)
                this.m_voltages = new List<float>();
            if (this.m_voltages.Count > 0)
                this.m_voltages.Clear();
            if (this.m_temperatures == null)
                this.m_temperatures = new List<float>();
            if (this.m_temperatures.Count > 0)
                this.m_temperatures.Clear();
            if (this.m_controlValues == null)
                this.m_controlValues = new List<float>();
            if (this.m_controlValues.Count > 0)
                this.m_controlValues.Clear();
            if (this.m_fanTachometerReadingLsbRegisters == null)
                this.m_fanTachometerReadingLsbRegisters = new List<int>();
            if (this.m_fanTachometerReadingLsbRegisters.Count > 0)
                this.m_fanTachometerReadingLsbRegisters.Clear();
            this.m_fanTachometerReadingLsbRegisters.Add(13);
            this.m_fanTachometerReadingLsbRegisters.Add(14);
            this.m_fanTachometerReadingLsbRegisters.Add(15);
            this.m_fanTachometerReadingLsbRegisters.Add(128);
            this.m_fanTachometerReadingLsbRegisters.Add(130);
            if (this.m_fanTachometerReadingMsbRegisters == null)
                this.m_fanTachometerReadingMsbRegisters = new List<int>();
            if (this.m_fanTachometerReadingMsbRegisters.Count > 0)
                this.m_fanTachometerReadingMsbRegisters.Clear();
            this.m_fanTachometerReadingMsbRegisters.Add(24);
            this.m_fanTachometerReadingMsbRegisters.Add(25);
            this.m_fanTachometerReadingMsbRegisters.Add(26);
            this.m_fanTachometerReadingMsbRegisters.Add(129);
            this.m_fanTachometerReadingMsbRegisters.Add(131);
            if (this.m_pwmControlRegisters == null)
                this.m_pwmControlRegisters = new List<int>();
            if (this.m_pwmControlRegisters.Count > 0)
                this.m_pwmControlRegisters.Clear();
            this.m_pwmControlRegisters.Add(21);
            this.m_pwmControlRegisters.Add(22);
            this.m_pwmControlRegisters.Add(23);
            if (this.m_voltageReadingRegisters == null)
                this.m_voltageReadingRegisters = new List<int>();
            if (this.m_voltageReadingRegisters.Count > 0)
                this.m_voltageReadingRegisters.Clear();
            this.m_voltageReadingRegisters.Add(32);
            this.m_voltageReadingRegisters.Add(33);
            this.m_voltageReadingRegisters.Add(34);
            this.m_voltageReadingRegisters.Add(35);
            this.m_voltageReadingRegisters.Add(36);
            this.m_voltageReadingRegisters.Add(37);
            this.m_voltageReadingRegisters.Add(38);
            if (this.m_temperatureReadingRegisters == null)
                this.m_temperatureReadingRegisters = new List<int>();
            if (this.m_temperatureReadingRegisters.Count > 0)
                this.m_temperatureReadingRegisters.Clear();
            this.m_temperatureReadingRegisters.Add(41);
            this.m_temperatureReadingRegisters.Add(42);
            this.m_temperatureReadingRegisters.Add(43);
            if (this.m_smartGuardianAutomaticModeRegisters == null)
                this.m_smartGuardianAutomaticModeRegisters = new List<MySmartGuardianAutomaticModeRegister>();
            if (this.m_smartGuardianAutomaticModeRegisters.Count > 0)
                this.m_smartGuardianAutomaticModeRegisters.Clear();
            this.m_smartGuardianAutomaticModeRegisters.Add(new MySmartGuardianAutomaticModeRegister()
            {
                TemperatureLimitValueOfOff = (byte)96,
                TemperatureLimitValueOfFanStart = (byte)97,
                TemperatureLimitValueOfFullSpeed = (byte)98,
                StartPwm = (byte)99,
                Control = (byte)100,
                DeltaTemperature = (byte)101,
                TargetZone = (byte)102
            });
            this.m_smartGuardianAutomaticModeRegisters.Add(new MySmartGuardianAutomaticModeRegister()
            {
                TemperatureLimitValueOfOff = (byte)104,
                TemperatureLimitValueOfFanStart = (byte)105,
                TemperatureLimitValueOfFullSpeed = (byte)106,
                StartPwm = (byte)107,
                Control = (byte)108,
                DeltaTemperature = (byte)109,
                TargetZone = (byte)110
            });
            this.m_smartGuardianAutomaticModeRegisters.Add(new MySmartGuardianAutomaticModeRegister()
            {
                TemperatureLimitValueOfOff = (byte)112,
                TemperatureLimitValueOfFanStart = (byte)113,
                TemperatureLimitValueOfFullSpeed = (byte)114,
                StartPwm = (byte)115,
                Control = (byte)116,
                DeltaTemperature = (byte)117,
                TargetZone = (byte)118
            });
        }

        private int RetrieveFanTachometerAmountWithoutFollowSelection()
        {
            int num = 0;
            bool fanTac1Enable = false;
            bool fanTac2Enable = false;
            bool fanTac3Enable = false;
            try
            {
                this.RetrieveFanTachometerEnableStatus(out fanTac1Enable, out fanTac2Enable, out fanTac3Enable);
                if (fanTac1Enable)
                    ++num;
                if (fanTac2Enable)
                    ++num;
                if (fanTac3Enable)
                    ++num;
            }
            catch
            {
                throw;
            }
            return num;
        }
    }
}
