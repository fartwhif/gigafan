﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.SmartFanConfigFile
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll


using System.IO;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MySmartFanConfigFile : MyXmlBaseFile
    {
        public MySmartFanConfigFile() => this.FilePath = string.Empty;

        public MySmartFanConfigFile(string filePath) => this.FilePath = filePath;

        public void Read(ref MySmartFanConfig pFanConfig)
        {
            if (this.FilePath.Length == 0)
                throw new FileNotFoundException();
            if (!File.Exists(this.FilePath))
                throw new FileNotFoundException();
            try
            {
                pFanConfig = MyXmlGenerater.SerializeObjectFromXML<MySmartFanConfig>(this.FilePath);
            }
            catch
            {
                throw;
            }
        }

        public void Write(ref MySmartFanConfig pFanConfig)
        {
            if (this.FilePath.Length == 0)
                throw new FileNotFoundException();
            if (File.Exists(this.FilePath))
                File.Delete(this.FilePath);
            try
            {
                MyXmlGenerater.SerializeObjectToXML<MySmartFanConfig>(pFanConfig, this.FilePath);
                if ((File.GetAttributes(this.FilePath) & FileAttributes.Hidden) == FileAttributes.Hidden)
                    return;
                File.SetAttributes(this.FilePath, File.GetAttributes(this.FilePath) | FileAttributes.Hidden);
            }
            catch
            {
                throw;
            }
        }
    }
}
