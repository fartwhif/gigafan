﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.FanControl
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.IO;


namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MyFanControl : MyIFanProfile, MyIFanCalibrate, MyIFanCalibrateFile, IDisposable
    {
        private const uint VAL_FLAG_STOP = 0;
        private const uint VAL_FLAG_START = 1;
        private const string REG_SUBKEY_ROOT_COMPANY = "SOFTWARE\\GIGABYTE";
        private const string REG_SUBKEY_DEFAULT_ROOT_FOLDER = "ThermalConsole";
        private const string REG_FAN_CALIBRATE_STATUS = "FanCalibrateStatus";
        private MyFanCooler m_fanMgr;
        private MyGenericCPU m_processorInfo;
        private MyMotherboardHealthIdentifierBase m_mbHealthIdentifier;

        public MyFanControl() => this.InitObjects("ThermalConsole");

        public string ProductName { get; protected set; }

        public int FanControlCount => this.m_fanMgr.FanControlCount;

        public int FanCount => this.m_fanMgr.FanCount;

        public int TemperatureCount => this.m_fanMgr.TemperatureCount;

        public int MaxPwmValue => this.m_fanMgr.MaxPwmValue;

        public bool IsSupported => this.m_fanMgr.IsSupported;

        public bool UsingExtendedTemperature
        {
            get => this.m_fanMgr.UsingExtendedTemperature;
            set => this.m_fanMgr.UsingExtendedTemperature = value;
        }

        public uint HealthNumber { get; protected set; }

        public MyProcessorVendors ProcessorVendor
        {
            get => this.m_processorInfo == null ? MyProcessorVendors.Intel : this.m_processorInfo.Vendor;
        }


        public void Dispose()
        {
            m_fanMgr = null;
            m_processorInfo = null;
            m_mbHealthIdentifier = null;
            ProductName = null;
            HealthNumber = 0;
        }

        public void SetProtectValue(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.m_fanMgr.SetProtectValue(fanCtrlIndex, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public void SetProtectValue(
          int fanCtrlIndex,
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.m_fanMgr.SetProtectValue(fanCtrlIndex, slopeIndex, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public void SetProtectValue(
          int fanCtrlIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.m_fanMgr.SetProtectValue(fanCtrlIndex, bCalibrateMode, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public void SetProtectValue(
          int fanCtrlIndex,
          int slopeIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.m_fanMgr.SetProtectValue(fanCtrlIndex, slopeIndex, bCalibrateMode, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public void GetProtectValue(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            try
            {
                this.m_fanMgr.GetProtectValue(fanCtrlIndex, ref pSmartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public void SetProtectValue(int fanCtrlIndex, ref MySmartGuardianFanConfig smartGuardianFanConfig)
        {
            try
            {
                this.m_fanMgr.SetProtectValue(fanCtrlIndex, ref smartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }

        public void GetProtectValue(
          int fanCtrlIndex,
          ref MySmartGuardianFanConfig pSmartGuardianFanConfig)
        {
            try
            {
                this.m_fanMgr.GetProtectValue(fanCtrlIndex, ref pSmartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }

        public void GetSlopeIndex(
          float currentTemperature,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs,
          ref int pSmartGuardianFanConfigIndex)
        {
            pSmartGuardianFanConfigIndex = 0;
            if (smartGuardianFanConfigs.Count == 0)
                return;
            for (int index = smartGuardianFanConfigs.Count - 1; index > 0; --index)
            {
                if ((double)currentTemperature > (double)smartGuardianFanConfigs[index].TemperatureLimitValueOfFanStart)
                {
                    pSmartGuardianFanConfigIndex = index;
                    break;
                }
            }
        }

        public void GetCpuFanSpeed(out float fanSpeed)
        {
            try
            {
                this.m_fanMgr.GetCpuFanSpeed(out fanSpeed);
            }
            catch
            {
                throw;
            }
        }

        public void GetFanSpeed(int fanIndex, out float fanSpeed)
        {
            try
            {
                this.m_fanMgr.GetFanSpeed(fanIndex, out fanSpeed);
            }
            catch
            {
                throw;
            }
        }

        public void GetFanSpeed(int fanIndex, bool bSwitchFanIndex, out float fanSpeed)
        {
            try
            {
                this.m_fanMgr.GetFanSpeed(fanIndex, bSwitchFanIndex, out fanSpeed);
            }
            catch
            {
                throw;
            }
        }

        public void GetFanDisplayName(int fanIndex, out string displayName)
        {
            try
            {
                this.m_fanMgr.GetFanDisplayName(fanIndex, out displayName);
            }
            catch
            {
                throw;
            }
        }

        public void GetFanControlDisplayName(int fanCtrlIndex, out string displayName)
        {
            try
            {
                this.m_fanMgr.GetFanControlDisplayName(fanCtrlIndex, out displayName);
            }
            catch
            {
                throw;
            }
        }

        public void GetTemperature(int temperatureIndex, out float targetTemperature)
        {
            try
            {
                this.m_fanMgr.GetTemperature(temperatureIndex, out targetTemperature);
            }
            catch
            {
                throw;
            }
        }

        public bool IsFanStop(int fanCtrlIndex)
        {
            float fanSpeed = 0.0f;
            try
            {
                this.m_fanMgr.GetFanSpeed(fanCtrlIndex, out fanSpeed);
                if ((double)fanSpeed == 0.0)
                    this.m_fanMgr.GetFanSpeed(fanCtrlIndex, true, out fanSpeed);
                return (double)fanSpeed == 0.0;
            }
            catch
            {
                throw;
            }
        }

        public void GetTemperatureDisplayName(int temperatureIndex, out string pDisplayName)
        {
            this.m_fanMgr.GetTemperatureDisplayName(temperatureIndex, out pDisplayName);
        }

        public void GetHardwareMonitorDatas(
          ref List<float> pTemperatureDatas,
          ref List<float> pFanSpeedDatas)
        {
            this.m_fanMgr.GetHardwareMonitorDatas(ref pTemperatureDatas, ref pFanSpeedDatas);
        }

        public void SetCalibrationPwm(int fanCtrlIndex, byte pwmPercentage)
        {
            try
            {
                this.m_fanMgr.SetCalibrationPwm(fanCtrlIndex, pwmPercentage);
            }
            catch
            {
                throw;
            }
        }

        public void ResetCalibrationPwm()
        {
            try
            {
                this.m_fanMgr.ResetCalibrationPwm();
            }
            catch
            {
                throw;
            }
        }

        public void SetFanSpeedFixedMode(int fanCtrlIndex, byte pwmPercentage)
        {
            try
            {
                this.m_fanMgr.SetFanSpeedFixedMode(fanCtrlIndex, pwmPercentage);
            }
            catch
            {
                throw;
            }
        }

        public void ConvertPwm2Percetage(int dutyCycle, out int dutyCyclePercetage)
        {
            try
            {
                this.m_fanMgr.ConvertPwm2Percetage(dutyCycle, out dutyCyclePercetage);
            }
            catch
            {
                throw;
            }
        }

        public void ConvertPercetage2Pwm(int dutyCyclePercetage, out int dutyCycle)
        {
            try
            {
                this.m_fanMgr.ConvertPercetage2Pwm(dutyCyclePercetage, out dutyCycle);
            }
            catch
            {
                throw;
            }
        }

        public void EnableCalibrateStatus(bool bEnable)
        {
            new MyRegistryKeyHandle().WriteDwordValue(RegistryHive.LocalMachine, string.Format("{0}\\{1}", (object)"SOFTWARE\\GIGABYTE", (object)this.ProductName), "FanCalibrateStatus", bEnable ? 1U : 0U);
        }

        public bool IsEnableCalibrateStatus()
        {
            uint pDwordValue = 0;
            new MyRegistryKeyHandle().ReadDwordValue(RegistryHive.LocalMachine, string.Format("{0}\\{1}", (object)"SOFTWARE\\GIGABYTE", (object)this.ProductName), "FanCalibrateStatus", out pDwordValue);
            return pDwordValue == 1U;
        }

        public bool IsProfileExist()
        {
            return new MyFanControlProfileEx()
            {
                FanControlCount = this.FanControlCount
            }.IsProfileExist();
        }

        public bool IsProfileExist(int fanControlIndex)
        {
            return new MyFanControlProfileEx()
            {
                FanControlCount = this.FanControlCount
            }.IsProfileExist(fanControlIndex);
        }

        public bool IsFanConfigChange4Profile(int fanControlIndex, ref MySmartFanConfig currentFanConfig)
        {
            bool flag1 = false;
            if (fanControlIndex > this.FanControlCount || currentFanConfig.SmartModeConfig.Count == 0)
                return flag1;
            MyFanControlProfileEx controlProfileEx = new MyFanControlProfileEx();
            controlProfileEx.FanControlCount = this.FanControlCount;
            if (!controlProfileEx.IsProfileExist(fanControlIndex))
                return flag1;
            MySmartFanConfig pFanConfig = new MySmartFanConfig();
            controlProfileEx.ReadFanConfig(fanControlIndex, ref pFanConfig);
            bool flag2 = true;
            if (currentFanConfig.Mode != pFanConfig.Mode)
                return flag2;
            bool flag3 = false;
            for (int index = 0; index < pFanConfig.SmartModeConfig.Count; ++index)
            {
                MySmartGuardianFanConfig guardianFanConfig1 = currentFanConfig.SmartModeConfig[index];
                MySmartGuardianFanConfig guardianFanConfig2 = pFanConfig.SmartModeConfig[index];
                if ((double)guardianFanConfig1.Slope != (double)guardianFanConfig2.Slope)
                {
                    flag3 = true;
                    break;
                }
                if ((int)guardianFanConfig1.StartPWM != (int)guardianFanConfig2.StartPWM)
                {
                    flag3 = true;
                    break;
                }
                if ((int)guardianFanConfig1.TemperatureLimitValueOfFanFullSpeed != (int)guardianFanConfig2.TemperatureLimitValueOfFanFullSpeed)
                {
                    flag3 = true;
                    break;
                }
                if ((int)guardianFanConfig1.TemperatureLimitValueOfFanOff != (int)guardianFanConfig2.TemperatureLimitValueOfFanOff)
                {
                    flag3 = true;
                    break;
                }
                if ((int)guardianFanConfig1.TemperatureLimitValueOfFanStart != (int)guardianFanConfig2.TemperatureLimitValueOfFanStart)
                {
                    flag3 = true;
                    break;
                }
            }
            if (flag3)
                return flag3;
            bool flag4 = true;
            MySmartGuardianFanConfig fixedModeConfig1 = currentFanConfig.FixedModeConfig;
            MySmartGuardianFanConfig fixedModeConfig2 = pFanConfig.FixedModeConfig;
            return ((double)fixedModeConfig1.Slope != (double)fixedModeConfig2.Slope || (int)fixedModeConfig1.StartPWM != (int)fixedModeConfig2.StartPWM || (int)fixedModeConfig1.TemperatureLimitValueOfFanFullSpeed != (int)fixedModeConfig2.TemperatureLimitValueOfFanFullSpeed || (int)fixedModeConfig1.TemperatureLimitValueOfFanOff != (int)fixedModeConfig2.TemperatureLimitValueOfFanOff || (int)fixedModeConfig1.TemperatureLimitValueOfFanStart != (int)fixedModeConfig2.TemperatureLimitValueOfFanStart) && flag4;
        }

        public void CreateNewProfiles()
        {
            for (int fanControlIndex = 0; fanControlIndex < this.FanControlCount; ++fanControlIndex)
                this.CreateNewProfile(fanControlIndex);
        }

        public void CreateNewProfile(int fanControlIndex)
        {
            MyFanControlProfileEx controlProfileEx = new MyFanControlProfileEx();
            controlProfileEx.FanControlCount = this.FanControlCount;
            if (this.FanControlCount == 0)
                throw new ArgumentException();
            string pDisplayName = string.Empty;
            MyFanTemperatureInput pTemperatureInputSelection = MyFanTemperatureInput.Unknown;
            MySmartGuardianFanConfigCollection configCollection = new MySmartGuardianFanConfigCollection();
            try
            {
                this.m_fanMgr.GetProtectValue(fanControlIndex, ref configCollection);
                bool bSwHardwareMonitor = this.m_fanMgr.IsRequireSoftwareMonitor(fanControlIndex);
                this.m_fanMgr.GetFanControlDisplayName(fanControlIndex, out pDisplayName);
                this.m_fanMgr.GetTemperatureInputSelection(fanControlIndex, ref pTemperatureInputSelection);
                controlProfileEx.CreateNewProfile(fanControlIndex, bSwHardwareMonitor, pDisplayName, pTemperatureInputSelection, ref configCollection);
            }
            catch
            {
                throw;
            }
        }

        public void GetProfileLastWriteTime(int fanControlIndex, out DateTime lastWriteTime)
        {
            new MyFanControlProfileEx()
            {
                FanControlCount = this.FanControlCount
            }.GetLastWriteTime(fanControlIndex, out lastWriteTime);
        }

        public void ReadFanConfigFromProfile(int fanControlIndex, ref MySmartFanConfig pFanConfig)
        {
            new MyFanControlProfileEx()
            {
                FanControlCount = this.FanControlCount
            }.ReadFanConfig(fanControlIndex, ref pFanConfig);
        }

        public void WriteFanConfigInProfile(int fanControlIndex, ref MySmartFanConfig pFanConfig)
        {
            pFanConfig.SwHardwareMonitor = this.m_fanMgr.IsRequireSoftwareMonitor(fanControlIndex);
            new MyFanControlProfileEx()
            {
                FanControlCount = this.FanControlCount
            }.WriteFanConfig(fanControlIndex, ref pFanConfig);
        }

        public bool IsCalibrateFileExist()
        {
            return new MyFanControlCalibrateFile()
            {
                FanControlCount = this.FanControlCount
            }.IsCalibrateFileExist();
        }

        public bool IsCalibrateFileExist(int fanControlIndex)
        {
            return new MyFanControlCalibrateFile()
            {
                FanControlCount = this.FanControlCount
            }.IsCalibrateFileExist(fanControlIndex);
        }

        public void ReadCalibrateData(int fanControlIndex, ref MyFanSpeedDataCollection pFanSpeedDatas)
        {
            MyFanControlCalibrateFile controlCalibrateFile = new MyFanControlCalibrateFile();
            try
            {
                controlCalibrateFile.FanControlCount = this.FanControlCount;
                controlCalibrateFile.ReadCalibrateData(fanControlIndex, ref pFanSpeedDatas);
            }
            catch
            {
                if (controlCalibrateFile.IsCalibrateFileExist(fanControlIndex))
                    controlCalibrateFile.DeleteCalibrateData(fanControlIndex);
                throw new FileNotFoundException();
            }
        }

        public void WriteCalibrateData(int fanControlIndex, ref MyFanSpeedDataCollection pFanSpeedDatas)
        {
            new MyFanControlCalibrateFile()
            {
                FanControlCount = this.FanControlCount
            }.WriteCalibrateData(fanControlIndex, ref pFanSpeedDatas);
        }

        public void DeleteCalibrateData(int fanControlIndex)
        {
            new MyFanControlCalibrateFile()
            {
                FanControlCount = this.FanControlCount
            }.DeleteCalibrateData(fanControlIndex);
        }

        private void InitObjects(string productName)
        {
            this.ProductName = productName;
            if (this.m_processorInfo == null)
                this.m_processorInfo = new MyGenericCPU();
            if (this.m_processorInfo.Vendor == MyProcessorVendors.Intel)
                this.InitIntelObjects();
            else
                this.InitAmdObjects();
        }

        private void InitIntelObjects()
        {
            try
            {
                uint pGroupNumber = 0;
                if (this.m_processorInfo.ProcessorSignature >= 198336U)
                {
                    if (this.m_mbHealthIdentifier == null)
                        this.m_mbHealthIdentifier = (MyMotherboardHealthIdentifierBase)new MyMotherboardHealthIdentifier();
                    this.m_mbHealthIdentifier.GetGroupNumber(out pGroupNumber);
                    uint healthNumber = MyMotherboardHealthIdentifierMask.ToHealthNumber(pGroupNumber);
                    MyMotherboardHealthIdentification healthid = this.m_mbHealthIdentifier.IsSupport(healthNumber) ? (MyMotherboardHealthIdentification)healthNumber : MyMotherboardHealthIdentification.MHID_Unknown;
                    this.HealthNumber = healthNumber;
                    this.m_fanMgr = healthid != MyMotherboardHealthIdentification.MHID_Unknown ? (MyFanCooler)new MyFanRegulatorEx2(healthid) : (MyFanCooler)new MyFanRegulatorEx();
                }
                else
                    this.m_fanMgr = (MyFanCooler)new MyFanRegulator();
                this.m_fanMgr.UsingExtendedTemperature = false;
            }
            catch
            {
                throw;
            }
        }

        private void InitAmdObjects()
        {
            try
            {
                if (this.m_mbHealthIdentifier == null)
                    this.m_mbHealthIdentifier = (MyMotherboardHealthIdentifierBase)new MyAmdMotherboardHealthIdentifier();
                uint pGroupNumber;
                this.m_mbHealthIdentifier.GetGroupNumber(out pGroupNumber);
                uint healthNumber = MyMotherboardHealthIdentifierMask.ToHealthNumber(pGroupNumber);
                MyAmdMotherboardHealthIdentification healthid = this.m_mbHealthIdentifier.IsSupport(healthNumber) ? (MyAmdMotherboardHealthIdentification)healthNumber : MyAmdMotherboardHealthIdentification.MHID_Unknown;
                this.HealthNumber = healthNumber;
                if (healthid == MyAmdMotherboardHealthIdentification.MHID_Unknown)
                    this.m_fanMgr = (MyFanCooler)new MyAmdGeneralFanRegulator();
                else
                    this.m_fanMgr = (MyFanCooler)new MyAmdFanRegulator(healthid);
            }
            catch
            {
                throw;
            }
        }
    }
}
