﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.SmartFanConfig
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MySmartFanConfig
    {
        public string Title { get; set; }

        public MyFanTemperatureInput TemperatureInput { get; set; }

        public MyFanConfigMode Mode { get; set; }

        public bool SwHardwareMonitor { get; set; }

        public MySmartGuardianFanConfig FixedModeConfig { get; set; }

        public MySmartGuardianFanConfigCollection SmartModeConfig { get; set; }
    }
}
