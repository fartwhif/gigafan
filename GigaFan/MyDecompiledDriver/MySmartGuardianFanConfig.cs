﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.SmartGuardianFanConfig
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public class MySmartGuardianFanConfig
    {
        public MySmartGuardianFanConfig()
        {
            this.TemperatureLimitValueOfFanOff = (byte)0;
            this.TemperatureLimitValueOfFanStart = (byte)0;
            this.TemperatureLimitValueOfFanFullSpeed = (byte)0;
            this.StartPWM = (byte)0;
            this.Slope = 0.0f;
            this.DeltaTemperature = (byte)0;
            this.TargetZoneBoundary = (byte)0;
            this.FanSpeed = 0;
        }

        public byte TemperatureLimitValueOfFanOff { get; set; }

        public byte TemperatureLimitValueOfFanStart { get; set; }

        public byte TemperatureLimitValueOfFanFullSpeed { get; set; }

        public byte StartPWM { get; set; }

        public float Slope { get; set; }

        public byte DeltaTemperature { get; set; }

        public byte TargetZoneBoundary { get; set; }

        public int FanSpeed { get; set; }
    }
}
