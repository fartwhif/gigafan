﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.MotherboardHealthIdentifierBase
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal abstract class MyMotherboardHealthIdentifierBase
    {
        protected MyMotherboard m_motherboardInfo;
        protected MyEasySmartFanComWrapper m_smartFanMgr;

        public MyMotherboardHealthIdentifierBase() => this.Vendor = MyProcessorVendors.Intel;

        public MyProcessorVendors Vendor { get; protected set; }

        public void GetGroupNumber(out uint pGroupNumber) => this.RetrieveGroupNumber(out pGroupNumber);

        public virtual bool IsSupport(uint groupNumber) => false;

        protected void RetrieveGroupNumber(out uint pGroupNumber)
        {
            try
            {
                if (this.m_smartFanMgr == null)
                    this.m_smartFanMgr = new MyEasySmartFanComWrapper();
                this.m_smartFanMgr.GetGroupNumber(out pGroupNumber);
            }
            catch
            {
                pGroupNumber = 0U;
            }
        }
    }
}
