﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.Chip
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal enum MyChip : ushort
    {
        Unknown = 0,
        IT8620E = 34336, // 0x8620
        IT8628E = 34344, // 0x8628
        IT8728F = 34600, // 0x8728
        IT8791E = 34611, // 0x8733
        IT8790F = 34704, // 0x8790
    }
}
