﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.FanSpeedDataFile
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll



using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Linq;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyFanSpeedDataFile
    {
        private string m_filePath = string.Empty;

        public MyFanSpeedDataFile() => this.m_filePath = string.Empty;

        public MyFanSpeedDataFile(string filePath) => this.m_filePath = filePath;

        public string FilePath
        {
            get => this.m_filePath;
            set => this.m_filePath = value;
        }

        public bool FileExist => this.FilePath.Length != 0 && File.Exists(this.FilePath);

        public void Read(ref MyFanSpeedDataCollection pFanSpeedDatas)
        {
            if (!this.FileExist)
                throw new FileNotFoundException();
            if (pFanSpeedDatas.Count > 0)
                pFanSpeedDatas.Clear();
            try
            {
                foreach (XElement element in XElement.Load(this.FilePath).Elements())
                {
                    MyFanSpeedData fanSpeedData = element.FromXElement<MyFanSpeedData>();
                    pFanSpeedDatas.Add(fanSpeedData);
                }
            }
            catch
            {
                throw;
            }
        }

        public void Write(ref MyFanSpeedDataCollection pFanSpeedDatas)
        {
            if (this.FilePath.Length == 0)
                throw new FileNotFoundException();
            if (File.Exists(this.FilePath))
                File.Delete(this.FilePath);
            try
            {
                XElement xelement1 = new XElement((XName)"FanSpeedDatas");
                foreach (object obj in (Collection<MyFanSpeedData>)pFanSpeedDatas)
                {
                    XElement xelement2 = obj.ToXElement<MyFanSpeedData>();
                    xelement1.Add((object)xelement2);
                }
                xelement1.Save(this.FilePath);
            }
            catch
            {
                throw;
            }
        }

        public void Delete()
        {
            if (this.FilePath.Length == 0 || !File.Exists(this.FilePath))
                return;
            File.Delete(this.FilePath);
        }
    }
}
