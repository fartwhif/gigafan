﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Processor.ProcessorAffinity
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll


using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyProcessorAffinity
    {
        private uint m_orgProcessAffinityMask;
        private uint m_orgSystemAffinityMask;

        public MyProcessorAffinity()
        {
            this.m_orgProcessAffinityMask = 0U;
            this.m_orgSystemAffinityMask = 0U;
            try
            {
                this.RetrieveProcessAffinityMask(out this.m_orgProcessAffinityMask, out this.m_orgSystemAffinityMask);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        ~MyProcessorAffinity() => this.RestoreProcessAffinityMask();

        public void SetProcessToOneOfProcessors(int processorIndex)
        {
            uint processAffinityMask = 1;
            if (processorIndex > 0)
                processAffinityMask = (uint)(1 << processorIndex);
            try
            {
                this.UpdateProcessAffinityMask(processAffinityMask);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void SetProcessAffinityMask(uint processAffinityMask)
        {
            try
            {
                this.UpdateProcessAffinityMask(processAffinityMask);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void GetProcessAffinityMask(out uint pProcessAffinityMask, out uint pSystemAffinityMask)
        {
            pProcessAffinityMask = pSystemAffinityMask = 0U;
            try
            {
                this.RetrieveProcessAffinityMask(out pProcessAffinityMask, out pSystemAffinityMask);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void RestoreProcessAffinityMask()
        {
            try
            {
                this.UpdateProcessAffinityMask(this.OriginalProcessAffinityMask);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public uint OriginalProcessAffinityMask => this.m_orgProcessAffinityMask;

        public uint OriginalSystemAffinityMask => this.m_orgSystemAffinityMask;

        private void RetrieveProcessAffinityMask(
          out uint pProcessAffinityMask,
          out uint pSystemAffinityMask)
        {
            pProcessAffinityMask = pSystemAffinityMask = 0U;
            UIntPtr lpProcessAffinityMask = new UIntPtr(1U);
            UIntPtr lpSystemAffinityMask = new UIntPtr(1U);
            IntPtr handle = Process.GetCurrentProcess().Handle;
            try
            {
                if (!MyNativeKernel32Api.GetProcessAffinityMask(handle, out lpProcessAffinityMask, out lpSystemAffinityMask))
                {
                    Console.WriteLine("The last Win32 Error was: " + (object)Marshal.GetLastWin32Error());
                    throw new Win32Exception(Marshal.GetLastWin32Error(), "Error");
                }
                pProcessAffinityMask = lpProcessAffinityMask.ToUInt32();
                pSystemAffinityMask = lpSystemAffinityMask.ToUInt32();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void UpdateProcessAffinityMask(uint processAffinityMask)
        {
            if (processAffinityMask == 0U)
                throw new ArgumentException();
            IntPtr handle = Process.GetCurrentProcess().Handle;
            UIntPtr dwProcessAffinityMask = new UIntPtr(processAffinityMask);
            try
            {
                if (!MyNativeKernel32Api.SetProcessAffinityMask(handle, dwProcessAffinityMask))
                {
                    Console.WriteLine("The last Win32 Error was: " + (object)Marshal.GetLastWin32Error());
                    throw new Win32Exception(Marshal.GetLastWin32Error(), "Error");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
