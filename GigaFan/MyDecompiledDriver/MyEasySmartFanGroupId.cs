﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EasyFunctions.EasySmartFanGroupId
// Assembly: Gigabyte.EasyFunctions, Version=7.2.0.24, Culture=neutral, PublicKeyToken=null
// MVID: 78B279F9-3DF2-44F7-BFAE-CD74B7F9DD3F
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EasyFunctions.dll

using System;
using System.Reflection;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyEasySmartFanGroupId : MyEasyFunctionHeader
    {
        private const string STR_EZSMARTFAN_SIGNATURE = "$EZVOLT$";
        private const uint CMD_INDEX_EZSMARTFAN = 100;

        public MyEasySmartFanGroupId()
        {
            try
            {
                if (!this.m_smiDataParser.FileExists)
                {
                    if (!this.RetrieveSmiPort(ref this.m_smiPort))
                        return;
                    this.m_supported = false;
                    this.m_bufferPhysicalAddress = 0U;
                }
                else
                {
                    SmiDataCollection pSmiDatas = new SmiDataCollection();
                    MySmiData pSmiData = new MySmiData();
                    this.m_smiDataParser.Read(ref pSmiDatas);
                    this.RetrieveSmiData("$EZVOLT$", ref pSmiDatas, ref pSmiData);
                    this.m_smiPort = pSmiData.Port;
                    this.m_supported = pSmiData.Supported;
                    this.m_bufferPhysicalAddress = pSmiData.BufferPhysicalAddress;
                }
                if (this.RetrieveHeaderInfo(100U, this.m_smiPort, this.m_bufferPhysicalAddress, "$EZVOLT$", out this.m_ezsHeader))
                    ;
            }
            catch (Exception ex)
            {
                if (ex.GetType() != typeof(InvalidCastException))
                    Console.WriteLine("[{0}] Error: {1}", (object)MethodBase.GetCurrentMethod().Name, (object)ex.Message);
                throw;
            }
        }

        public bool GetGroupNumber(out uint pGroupNumber)
        {
            try
            {
                return this.RetrieveGroupNumber(100U, this.m_smiPort, out pGroupNumber);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[{0}] Error: {1}", (object)MethodBase.GetCurrentMethod().Name, (object)ex.Message);
                throw;
            }
        }

        protected bool RetrieveGroupNumber(uint functionIndex, int smiPort, out uint pGroupNumber)
        {
            bool flag1 = false;
            pGroupNumber = 0U;
            if (smiPort == 0)
                return flag1;
            try
            {
                uint eax = 45824U + functionIndex;
                int num;
                uint flag2 = (uint)(num = 0);
                uint edi = (uint)num;
                uint esi = (uint)num;
                uint ecx = (uint)num;
                uint ebx = (uint)num;
                uint edx = (uint)smiPort;
                if (!MyNativeYccMethods.SMICmd(ref eax, ref ebx, ref ecx, ref edx, ref esi, ref edi, ref flag2) || eax != 0U)
                    return flag1;
                pGroupNumber = ebx;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("[{0}] Error: {1}", (object)MethodBase.GetCurrentMethod().Name, (object)ex.Message);
                throw;
            }
        }
    }
}
