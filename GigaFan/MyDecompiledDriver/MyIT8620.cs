﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.IT8620
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Collections.Generic;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyIT8620 : MyIT87XX
    {
        protected const byte PROCHOT_CONTROL_REGISTER = 103;

        public MyIT8620(MyChip chip, ushort baseAddress, ushort gpioAddress, byte version)
        {
            bool flag;
            switch (chip)
            {
                case MyChip.IT8620E:
                case MyChip.IT8628E:
                    flag = true;
                    break;
                default:
                    flag = false;
                    break;
            }
            if (!flag)
                throw new NotSupportedException();
            this.BaseAddress = baseAddress;
            this.chip = chip;
            this.Version = version;
            this.AddressRegister = (ushort)((uint)baseAddress + 5U);
            this.DataRegister = (ushort)((uint)baseAddress + 6U);
            this.GpioAddress = gpioAddress;
            bool valid;
            byte num = this.ReadByte((byte)88, out valid);
            if (!valid || num != (byte)144 || ((int)this.ReadByte((byte)0, out valid) & 16) == 0 || !valid)
                return;
            this.VoltageGain = 0.012f;
            this.Has16bitFanCounter = true;
            this.GpioCount = 0;
            this.FanControlRegisterCount = 5;
            this.ActualFanControlRegisterCount = this.RetrieveFanTachometerAmount();
            this.FanTachometerCount = this.ActualFanControlRegisterCount;
            this.TemperatureCount = 3;
            this.VoltageCount = 7;
            this.MaxPwmValue = (int)byte.MaxValue;
            this.InitiObjects();
        }

        public void EnableFanControl4ToAutomaticOperation(bool bEnable)
        {
            try
            {
                this.SetupFanControl4ToAutomaticOperation(bEnable);
                MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
                if (bEnable)
                {
                    this.GetFanSmartGuardianConfig(0, ref fanConfig);
                }
                else
                {
                    fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                    fanConfig.TemperatureLimitValueOfFanStart = (byte)0;
                    fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)130;
                    fanConfig.StartPWM = (byte)0;
                    fanConfig.Slope = 0.0f;
                    fanConfig.DeltaTemperature = (byte)0;
                    fanConfig.TargetZoneBoundary = (byte)0;
                    fanConfig.FanSpeed = 0;
                }
                this.SetFanSmartGuardianConfig(3, ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public void EnableFanControl5ToAutomaticOperation(bool bEnable)
        {
            try
            {
                this.SetupFanControl5ToAutomaticOperation(bEnable);
                MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
                if (bEnable)
                {
                    this.GetFanSmartGuardianConfig(0, ref fanConfig);
                }
                else
                {
                    fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                    fanConfig.TemperatureLimitValueOfFanStart = (byte)0;
                    fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)130;
                    fanConfig.StartPWM = (byte)0;
                    fanConfig.Slope = 0.0f;
                    fanConfig.DeltaTemperature = (byte)0;
                    fanConfig.TargetZoneBoundary = (byte)0;
                    fanConfig.FanSpeed = 0;
                }
                this.SetFanSmartGuardianConfig(4, ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        protected void SetupFanControl4ToAutomaticOperation(bool bEnable)
        {
            bool valid = false;
            if (this.m_pwmControlRegisters.Count == 0)
                return;
            try
            {
                byte register = Convert.ToByte(this.m_pwmControlRegisters[3]);
                byte num1 = this.ReadByte(register, out valid);
                if (!valid)
                    return;
                bool flag;
                if (bEnable)
                {
                    byte num2 = (byte)((uint)(byte)((uint)num1 & 123U) | 64U);
                    flag = this.WriteByte(register, num2);
                }
                else
                {
                    byte num3 = (byte)((uint)(byte)((uint)num1 | 132U) & 64U);
                    flag = this.WriteByte(register, num3);
                }
            }
            catch
            {
                throw;
            }
        }

        protected void SetupFanControl5ToAutomaticOperation(bool bEnable)
        {
            bool valid = false;
            if (this.m_pwmControlRegisters.Count == 0)
                return;
            try
            {
                byte num1 = this.ReadByte((byte)103, out valid);
                if (!valid)
                    return;
                byte register = Convert.ToByte(this.m_pwmControlRegisters[4]);
                byte num2 = this.ReadByte(register, out valid);
                if (!valid)
                    return;
                if (bEnable)
                {
                    if (!this.WriteByte((byte)103, Convert.ToByte((int)num1 & 63)))
                        return;
                    byte num3 = (byte)((uint)(byte)((uint)num2 & 123U) | 64U);
                    if (this.WriteByte(register, num3))
                        ;
                }
                else
                {
                    if (!this.WriteByte((byte)103, Convert.ToByte((int)num1 | 192)))
                        return;
                    byte num4 = (byte)((uint)(byte)((uint)num2 & 187U) | 128U);
                    if (this.WriteByte(register, num4))
                        ;
                }
            }
            catch
            {
                throw;
            }
        }

        private void InitiObjects()
        {
            if (this.m_fanSpeeds == null)
                this.m_fanSpeeds = new List<float>();
            if (this.m_fanSpeeds.Count > 0)
                this.m_fanSpeeds.Clear();
            if (this.m_voltages == null)
                this.m_voltages = new List<float>();
            if (this.m_voltages.Count > 0)
                this.m_voltages.Clear();
            if (this.m_temperatures == null)
                this.m_temperatures = new List<float>();
            if (this.m_temperatures.Count > 0)
                this.m_temperatures.Clear();
            if (this.m_controlValues == null)
                this.m_controlValues = new List<float>();
            if (this.m_controlValues.Count > 0)
                this.m_controlValues.Clear();
            if (this.m_fanTachometerReadingLsbRegisters == null)
                this.m_fanTachometerReadingLsbRegisters = new List<int>();
            if (this.m_fanTachometerReadingLsbRegisters.Count > 0)
                this.m_fanTachometerReadingLsbRegisters.Clear();
            this.m_fanTachometerReadingLsbRegisters.Add(13);
            this.m_fanTachometerReadingLsbRegisters.Add(14);
            this.m_fanTachometerReadingLsbRegisters.Add(15);
            this.m_fanTachometerReadingLsbRegisters.Add(128);
            this.m_fanTachometerReadingLsbRegisters.Add(130);
            if (this.m_fanTachometerReadingMsbRegisters == null)
                this.m_fanTachometerReadingMsbRegisters = new List<int>();
            if (this.m_fanTachometerReadingMsbRegisters.Count > 0)
                this.m_fanTachometerReadingMsbRegisters.Clear();
            this.m_fanTachometerReadingMsbRegisters.Add(24);
            this.m_fanTachometerReadingMsbRegisters.Add(25);
            this.m_fanTachometerReadingMsbRegisters.Add(26);
            this.m_fanTachometerReadingMsbRegisters.Add(129);
            this.m_fanTachometerReadingMsbRegisters.Add(131);
            if (this.m_pwmControlRegisters == null)
                this.m_pwmControlRegisters = new List<int>();
            if (this.m_pwmControlRegisters.Count > 0)
                this.m_pwmControlRegisters.Clear();
            this.m_pwmControlRegisters.Add(21);
            this.m_pwmControlRegisters.Add(22);
            this.m_pwmControlRegisters.Add(23);
            this.m_pwmControlRegisters.Add((int)sbyte.MaxValue);
            this.m_pwmControlRegisters.Add(167);
            this.m_pwmControlRegisters.Add(175);
            if (this.m_voltageReadingRegisters == null)
                this.m_voltageReadingRegisters = new List<int>();
            if (this.m_voltageReadingRegisters.Count > 0)
                this.m_voltageReadingRegisters.Clear();
            this.m_voltageReadingRegisters.Add(32);
            this.m_voltageReadingRegisters.Add(33);
            this.m_voltageReadingRegisters.Add(34);
            this.m_voltageReadingRegisters.Add(35);
            this.m_voltageReadingRegisters.Add(36);
            this.m_voltageReadingRegisters.Add(37);
            this.m_voltageReadingRegisters.Add(38);
            if (this.m_temperatureReadingRegisters == null)
                this.m_temperatureReadingRegisters = new List<int>();
            if (this.m_temperatureReadingRegisters.Count > 0)
                this.m_temperatureReadingRegisters.Clear();
            this.m_temperatureReadingRegisters.Add(41);
            this.m_temperatureReadingRegisters.Add(42);
            this.m_temperatureReadingRegisters.Add(43);
            if (this.m_smartGuardianAutomaticModeRegisters == null)
                this.m_smartGuardianAutomaticModeRegisters = new List<MySmartGuardianAutomaticModeRegister>();
            if (this.m_smartGuardianAutomaticModeRegisters.Count > 0)
                this.m_smartGuardianAutomaticModeRegisters.Clear();
            this.m_smartGuardianAutomaticModeRegisters.Add(new MySmartGuardianAutomaticModeRegister()
            {
                TemperatureLimitValueOfOff = (byte)96,
                TemperatureLimitValueOfFanStart = (byte)97,
                TemperatureLimitValueOfFullSpeed = (byte)98,
                StartPwm = (byte)99,
                Control = (byte)100,
                DeltaTemperature = (byte)101,
                TargetZone = (byte)102
            });
            this.m_smartGuardianAutomaticModeRegisters.Add(new MySmartGuardianAutomaticModeRegister()
            {
                TemperatureLimitValueOfOff = (byte)104,
                TemperatureLimitValueOfFanStart = (byte)105,
                TemperatureLimitValueOfFullSpeed = (byte)106,
                StartPwm = (byte)107,
                Control = (byte)108,
                DeltaTemperature = (byte)109,
                TargetZone = (byte)110
            });
            this.m_smartGuardianAutomaticModeRegisters.Add(new MySmartGuardianAutomaticModeRegister()
            {
                TemperatureLimitValueOfOff = (byte)112,
                TemperatureLimitValueOfFanStart = (byte)113,
                TemperatureLimitValueOfFullSpeed = (byte)114,
                StartPwm = (byte)115,
                Control = (byte)116,
                DeltaTemperature = (byte)117,
                TargetZone = (byte)118
            });
            this.m_smartGuardianAutomaticModeRegisters.Add(new MySmartGuardianAutomaticModeRegister()
            {
                TemperatureLimitValueOfOff = (byte)120,
                TemperatureLimitValueOfFanStart = (byte)121,
                TemperatureLimitValueOfFullSpeed = (byte)122,
                StartPwm = (byte)123,
                Control = (byte)124,
                DeltaTemperature = (byte)125,
                TargetZone = (byte)126
            });
            this.m_smartGuardianAutomaticModeRegisters.Add(new MySmartGuardianAutomaticModeRegister()
            {
                TemperatureLimitValueOfOff = (byte)160,
                TemperatureLimitValueOfFanStart = (byte)161,
                TemperatureLimitValueOfFullSpeed = (byte)162,
                StartPwm = (byte)163,
                Control = (byte)164,
                DeltaTemperature = (byte)165,
                TargetZone = (byte)166
            });
            this.m_smartGuardianAutomaticModeRegisters.Add(new MySmartGuardianAutomaticModeRegister()
            {
                TemperatureLimitValueOfOff = (byte)168,
                TemperatureLimitValueOfFanStart = (byte)169,
                TemperatureLimitValueOfFullSpeed = (byte)170,
                StartPwm = (byte)171,
                Control = (byte)172,
                DeltaTemperature = (byte)173,
                TargetZone = (byte)174
            });
        }
    }
}
