﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EasyFunctions.EasyFunctionHeader
// Assembly: Gigabyte.EasyFunctions, Version=7.2.0.24, Culture=neutral, PublicKeyToken=null
// MVID: 78B279F9-3DF2-44F7-BFAE-CD74B7F9DD3F
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EasyFunctions.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyEasyFunctionHeader
    {
        private const int MAX_LEN_EASY_FUNCTION_HEADER = 32;
        private const string STR_BDR_SIGNATURE = "$BDR";
        private const int MAX_LEN_EASY_FUNCTION_SIGNATURE = 8;
        protected int m_smiPort;
        protected bool m_supported;
        protected MyEASY_FUNCTION_HEADER m_ezsHeader;
        protected uint m_bufferPhysicalAddress;
        protected SmiDataFile m_smiDataParser = new SmiDataFile();

        public MyEasyFunctionHeader()
        {
            this.m_smiPort = 0;
            this.m_supported = false;
            this.m_bufferPhysicalAddress = 0U;
            this.m_ezsHeader.signature = string.Empty;
            this.m_ezsHeader.bufferLength = 0;
            this.m_ezsHeader.checksum = 0;
            this.m_ezsHeader.revision = 0;
            this.m_ezsHeader.deviceDescriptorCount = 0U;
        }

        public bool Supported => this.m_supported;

        public int SwSmiPort => this.m_smiPort;

        public string Signature => this.m_ezsHeader.signature;

        public int Length => this.m_ezsHeader.bufferLength;

        public int Revision => this.m_ezsHeader.revision;

        public int Checksum => this.m_ezsHeader.checksum;

        public uint DeviceDescriptorCount => this.m_ezsHeader.deviceDescriptorCount;

        public uint BufferPhysicalAddress => this.m_bufferPhysicalAddress;

        public void GetVersion(out EASY_FUNCTION_VERSION pVal)
        {
            pVal.majorVersion = 0;
            pVal.minorVersion = 0;
            if (!this.m_supported)
                throw new NotSupportedException();
            int revision1 = this.m_ezsHeader.revision;
            int num;
            pVal.majorVersion = num = revision1 >> 4;
            int revision2 = this.m_ezsHeader.revision;
            pVal.minorVersion = revision2 & 15;
        }

        protected bool RetrieveSupportStatus(uint functionIndex, int smiPort, out bool pVal)
        {
            bool flag1 = false;
            pVal = false;
            if (smiPort == 0)
                return flag1;
            try
            {
                uint eax = 45056U + functionIndex;
                int num;
                uint flag2 = (uint)(num = 0);
                uint edi = (uint)num;
                uint esi = (uint)num;
                uint ecx = (uint)num;
                uint ebx = (uint)num;
                uint edx = (uint)smiPort;
                if (!MyNativeYccMethods.SMICmd(ref eax, ref ebx, ref ecx, ref edx, ref esi, ref edi, ref flag2))
                    return flag1;
                pVal = eax == 0U;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        protected bool RetrieceAttribute(
          uint functionIndex,
          int smiPort,
          out uint bufferPhysicalAddress)
        {
            bool flag1 = false;
            bufferPhysicalAddress = 0U;
            if (smiPort == 0)
                return flag1;
            if (!this.m_supported)
                return flag1;
            try
            {
                uint eax = 45312U + functionIndex;
                int num;
                uint flag2 = (uint)(num = 0);
                uint edi = (uint)num;
                uint esi = (uint)num;
                uint ecx = (uint)num;
                uint ebx = (uint)num;
                uint edx = (uint)smiPort;
                if (!MyNativeYccMethods.SMICmd(ref eax, ref ebx, ref ecx, ref edx, ref esi, ref edi, ref flag2) || ebx == 0U)
                    return flag1;
                bufferPhysicalAddress = ebx;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        protected bool RetrieveHeaderInfo(
          uint functionIndex,
          int smiPort,
          uint bufferPhysicalAddress,
          string signature,
          out MyEASY_FUNCTION_HEADER ezsHeader)
        {
            bool flag = false;
            ezsHeader.signature = string.Empty;
            ezsHeader.bufferLength = 0;
            ezsHeader.checksum = 0;
            ezsHeader.revision = 0;
            ezsHeader.deviceDescriptorCount = 0U;
            if (smiPort == 0 || bufferPhysicalAddress == 0U)
                return flag;
            if (!this.m_supported)
                return flag;
            byte[] numArray;
            try
            {
                ulong num = MyNativeYccMethods.MapMemEx((long)bufferPhysicalAddress, 32UL);
                if (num == 0UL)
                    return flag;
                numArray = new byte[32];
                this.MarshalCopyRawData(num, numArray, 32);
                MyNativeYccMethods.UnMapMemEx(num);
            }
            catch (Exception ex)
            {
                throw;
            }
            MemoryStream input = new MemoryStream(numArray);
            BinaryReader binaryReader = new BinaryReader((Stream)input);
            try
            {
                string strB = signature;
                string empty = string.Empty;
                string str = new string(binaryReader.ReadChars(8)).TrimEnd(new char[1]);
                if (str.CompareTo(strB) != 0)
                    return flag;
                ezsHeader.signature = str;
                ezsHeader.bufferLength = (int)binaryReader.ReadInt16();
                ezsHeader.revision = (int)binaryReader.ReadByte();
                ezsHeader.checksum = (int)binaryReader.ReadByte();
                binaryReader.ReadBytes(4);
                ezsHeader.deviceDescriptorCount = (uint)binaryReader.ReadUInt16();
                flag = true;
            }
            catch (EndOfStreamException ex)
            {
            }
            catch (IOException ex)
            {
                throw;
            }
            finally
            {
                binaryReader.Close();
                input.Close();
            }
            return flag;
        }

        protected bool RetrieveHeaderInfo(
          uint functionIndex,
          int smiPort,
          string signature,
          uint bufferPhysicalAddress,
          out MyEASY_FUNCTION_HEADER ezsHeader)
        {
            bool flag = false;
            ezsHeader.signature = string.Empty;
            ezsHeader.bufferLength = 0;
            ezsHeader.checksum = 0;
            ezsHeader.revision = 0;
            ezsHeader.deviceDescriptorCount = 0U;
            if (smiPort == 0)
                return flag;
            if (!this.m_supported)
                return flag;
            byte[] numArray;
            try
            {
                ulong num = MyNativeYccMethods.MapMemEx((long)this.m_bufferPhysicalAddress, 32UL);
                if (num == 0UL)
                    return flag;
                numArray = new byte[32];
                this.MarshalCopyRawData(num, numArray, 32);
                MyNativeYccMethods.UnMapMemEx(num);
            }
            catch (Exception ex)
            {
                throw;
            }
            MemoryStream input = new MemoryStream(numArray);
            BinaryReader binaryReader = new BinaryReader((Stream)input);
            try
            {
                string strB = signature;
                string empty = string.Empty;
                string str = new string(binaryReader.ReadChars(8)).TrimEnd(new char[1]);
                if (str.CompareTo(strB) != 0)
                    return flag;
                ezsHeader.signature = str;
                ezsHeader.bufferLength = (int)binaryReader.ReadInt16();
                ezsHeader.revision = (int)binaryReader.ReadByte();
                ezsHeader.checksum = (int)binaryReader.ReadByte();
                binaryReader.ReadBytes(4);
                ezsHeader.deviceDescriptorCount = (uint)binaryReader.ReadUInt16();
                flag = true;
            }
            catch (EndOfStreamException ex)
            {
            }
            catch (IOException ex)
            {
                throw;
            }
            finally
            {
                binaryReader.Close();
                input.Close();
            }
            return flag;
        }

        protected unsafe bool RetrieveSmiPort(ref int pVal)
        {
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            long num1 = 0;
            bool flag1 = false;
            pVal = 0;
            long pVal1 = 0;
            bool flag2 = this.RetrieveFlashPhysicalAddress(out pVal1);
            if (!flag2)
                return flag2;
            bool flag3;
            try
            {
                flag3 = false;
                ulong ptr = MyNativeYccMethods.MapMemEx(pVal1, 2097151UL);
                if (ptr == 0UL)
                    return flag3;
                byte* numPtr1 = (byte*)ptr;
                int num2 = 2097151;
                string str = "$BDR";
                for (int index1 = 0; index1 < num2; ++index1)
                {
                    byte* numPtr2 = numPtr1 + index1;
                    string empty3 = string.Empty;
                    if (*numPtr2 == (byte)36)
                    {
                        for (int index2 = 0; index2 < 4; ++index2)
                        {
                            empty3 += (string)(object)Convert.ToChar(*numPtr2);
                            ++numPtr2;
                        }
                        if (str.CompareTo(empty3) == 0)
                        {
                            num1 = (long)numPtr2[57] | (long)((int)numPtr2[58] << 8);
                            flag1 = true;
                            break;
                        }
                    }
                }
                MyNativeYccMethods.UnMapMemEx(ptr);
            }
            catch (Exception ex)
            {
                if (ex.GetType() != typeof(InvalidCastException))
                    Console.WriteLine(ex.Message);
                throw;
            }
            if (!flag1)
                return flag3;
            pVal = (int)num1;
            return true;
        }

        protected void RetrieveSmiData(
          string signature,
          ref SmiDataCollection pSmiDatas,
          ref MySmiData pSmiData)
        {
            if (signature.Length == 0)
                throw new ArgumentException();
            if (pSmiDatas.Count == 0)
                throw new ArgumentException();
            foreach (MySmiData smiData in (List<MySmiData>)pSmiDatas)
            {
                if (!(smiData.Signature != signature))
                {
                    pSmiData.Signature = smiData.Signature;
                    pSmiData.Supported = smiData.Supported;
                    pSmiData.Port = smiData.Port;
                    pSmiData.BufferPhysicalAddress = smiData.BufferPhysicalAddress;
                    break;
                }
            }
        }

        protected unsafe void MarshalCopyRawData(
          ulong mapmemPhysicalAddress,
          byte[] destination,
          int length)
        {
            Marshal.Copy(new IntPtr((void*)mapmemPhysicalAddress), destination, 0, length);
        }

        private unsafe bool RetrieveFlashPhysicalAddress(out long pVal)
        {
            bool flag = false;
            IntPtr zero = IntPtr.Zero;
            long num1 = 0;
            try
            {
                pVal = 0L;
                ulong ptr = MyNativeYccMethods.MapMemEx(4294967280L, 16UL);
                if (ptr == 0UL)
                    return flag;
                byte* numPtr = (byte*)ptr;
                long num2 = (num1 = (long)numPtr[15] << 24) | (num1 = (long)numPtr[14] << 16) | (num1 = (long)numPtr[13] << 8) | (long)numPtr[12];
                MyNativeYccMethods.UnMapMemEx(ptr);
                pVal = num2;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                pVal = 0L;
                throw;
            }
        }
    }
}
