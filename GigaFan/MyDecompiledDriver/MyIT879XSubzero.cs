﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.IT879XSubzero
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyIT879XSubzero : MySuperIO
    {
        private const int VAL_MAX_SUBZERO_COUNT = 2;
        private const int VAL_MAX_RANGE_SUBZERO = 200;
        private const int VAL_MIN_RANGE_SUBZERO = -200;
        private const int VAL_SUBZERO1_NO_PLUGIN = 65330;
        private const int VAL_SUBZERO2_NO_PLUGIN = 65328;
        private const byte OFFSET_IT879X_FW_MAIN_VERSION = 16;
        private const byte OFFSET_IT879X_FW_SUB_VERSION = 17;
        private const byte OFFSET_IT895X_FW_VERSION0 = 18;
        private const byte OFFSET_IT895X_FW_VERSION1 = 19;
        private const byte OFFSET_IT895X_FW_VERSION2 = 20;
        private const byte OFFSET_IT895X_FW_VERSION3 = 21;
        private const byte OFFSET_IT895X_FW_VERSION4 = 22;
        private const byte OFFSET_SUBZERO1_MSB = 24;
        private const byte OFFSET_SUBZERO1_LSB = 25;
        private const byte OFFSET_SUBZERO2_MSB = 26;
        private const byte OFFSET_SUBZERO2_LSB = 27;

        public MyIT879XSubzero(MyChip targetChipset, uint indexPort, uint dataPort)
        {
            uint it879xVersion = 0;
            ulong it895xVersion = 0;
            int pTemperature1 = 0;
            int pTemperature2 = 0;
            this.Chipset = MyChip.Unknown;
            this.IndexPort = 0U;
            this.DataPort = 0U;
            this.Supported = false;
            this.IT879xFirmwareVersion = 0U;
            this.IT895xFirmwareVersion = 0UL;
            this.Count = 0;
            bool flag;
            switch (targetChipset)
            {
                case MyChip.IT8791E:
                    flag = true;
                    break;
                default:
                    flag = false;
                    break;
            }
            if (!flag || indexPort == 0U || dataPort == 0U)
                return;
            this.Chipset = targetChipset;
            this.IndexPort = indexPort;
            this.DataPort = dataPort;
            ushort uint16_1 = Convert.ToUInt16(indexPort);
            ushort uint16_2 = Convert.ToUInt16(dataPort);
            this.RetrieveFirmwareVersion(uint16_1, uint16_2, out it879xVersion, out it895xVersion);
            if (it879xVersion == 0U && it895xVersion == 0UL)
                return;
            this.IT879xFirmwareVersion = it879xVersion;
            this.IT895xFirmwareVersion = it895xVersion;
            this.RetrieveCurrentTemperature(uint16_1, uint16_2, out pTemperature1, out pTemperature2);
            this.Supported = this.CheckSensorChipsetExist(pTemperature1, pTemperature2);
            if (!this.Supported)
                return;
            this.Count = 2;
        }

        public MyChip Chipset { get; protected set; }

        public uint IndexPort { get; protected set; }

        public uint DataPort { get; protected set; }

        public bool Supported { get; protected set; }

        public uint IT879xFirmwareVersion { get; protected set; }

        public ulong IT895xFirmwareVersion { get; protected set; }

        public int Count { get; protected set; }

        public bool IsSensorConnected(int targetIndex)
        {
            bool flag = false;
            int pTemperature1 = 0;
            int pTemperature2 = 0;
            if (targetIndex >= 2 || !this.Supported)
                return flag;
            this.Read(out pTemperature1, out pTemperature2);
            int targetTemperature = targetIndex != 0 ? pTemperature2 : pTemperature1;
            return this.CheckTemperatureProbePlugIn(targetIndex, targetTemperature);
        }

        public void Read(int targetIndex, out int pTemperature)
        {
            int pTemperature1 = 0;
            int pTemperature2 = 0;
            pTemperature = 0;
            if (targetIndex >= 2)
                return;
            this.Read(out pTemperature1, out pTemperature2);
            pTemperature = targetIndex == 0 ? pTemperature1 : pTemperature2;
        }

        public void Read(out int pTemperature1, out int pTemperature2)
        {
            pTemperature1 = 0;
            pTemperature2 = 0;
            if (!this.Supported)
                return;
            this.RetrieveCurrentTemperature(Convert.ToUInt16(this.IndexPort), Convert.ToUInt16(this.DataPort), out pTemperature1, out pTemperature2);
        }

        private void RetrieveFirmwareVersion(
          ushort indexPort,
          ushort dataPort,
          out uint it879xVersion,
          out ulong it895xVersion)
        {
            byte[] numArray = new byte[8];
            it879xVersion = 0U;
            it895xVersion = 0UL;
            if (indexPort == (ushort)0 || dataPort == (ushort)0)
                return;
            ushort num1 = this.ReadWord(indexPort, dataPort, (byte)16);
            it879xVersion = (uint)num1;
            if (it879xVersion == 0U)
                return;
            try
            {
                byte num2 = this.ReadByte(indexPort, dataPort, (byte)18);
                numArray[4] = num2;
                byte num3 = this.ReadByte(indexPort, dataPort, (byte)19);
                numArray[3] = num3;
                byte num4 = this.ReadByte(indexPort, dataPort, (byte)20);
                numArray[2] = num4;
                byte num5 = this.ReadByte(indexPort, dataPort, (byte)21);
                numArray[1] = num5;
                byte num6 = this.ReadByte(indexPort, dataPort, (byte)22);
                numArray[0] = num6;
                it895xVersion = BitConverter.ToUInt64(numArray, 0);
            }
            catch
            {
                it895xVersion = 0UL;
            }
        }

        private void RetrieveCurrentTemperature(
          ushort indexPort,
          ushort dataPort,
          out int pTemperature1,
          out int pTemperature2)
        {
            pTemperature1 = 0;
            pTemperature2 = 0;
            if (indexPort == (ushort)0)
                return;
            if (dataPort == (ushort)0)
                return;
            try
            {
                byte[] bytes1 = BitConverter.GetBytes(this.ReadWord(indexPort, dataPort, (byte)24));
                pTemperature1 = (int)BitConverter.ToInt16(bytes1, 0);
                byte[] bytes2 = BitConverter.GetBytes(this.ReadWord(indexPort, dataPort, (byte)26));
                pTemperature2 = (int)BitConverter.ToInt16(bytes2, 0);
            }
            catch
            {
                pTemperature1 = 0;
                pTemperature2 = 0;
            }
        }

        private bool CheckSensorChipsetExist(int temperature1, int temperature2)
        {
            bool flag = true;
            if (temperature1 == 0 && temperature2 == 0)
                flag = false;
            return flag;
        }

        private bool CheckTemperatureProbePlugIn(int targetIndex, int targetTemperature)
        {
            bool flag = false;
            switch (targetIndex)
            {
                case 0:
                    flag = targetTemperature != 65330;
                    break;
                case 1:
                    flag = targetTemperature != 65330;
                    break;
                default:
                    return flag;
            }
            return flag;
        }
    }
}
