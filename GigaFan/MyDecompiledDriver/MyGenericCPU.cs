﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Processor.GenericCPU
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyGenericCPU
    {
        protected const uint CPUID_0 = 0;
        protected const uint CPUID_1 = 1;
        protected const string REG_EASYTUNE_KEY = "SOFTWARE\\\\Gigabyte\\\\EasyTune";
        protected const string REG_CPU_NAME = "CPU_NAME";
        protected const int VAL_MAX_BUFFER_SIZE = 256;

        public MyGenericCPU()
        {
            StringBuilder stringBuilder1 = new StringBuilder(256);
            StringBuilder stringBuilder2 = new StringBuilder(256);
            IntPtr zero = IntPtr.Zero;
            string empty = string.Empty;
            MyNativeCpuApi.GBFunc2(-1, 0, stringBuilder1, stringBuilder2);
            this.CpuPort = stringBuilder2.ToString();
            this.UpdateMemberVariables();
        }

        public string Name { get; set; }

        public string CodeName { get; set; }

        public string BrandID { get; set; }

        public string TDP { get; set; }

        public string Socket { get; set; }

        public string Technology { get; set; }

        public string CoreVoltage { get; set; }

        public string Specification { get; set; }

        public string Family { get; set; }

        public string Model { get; set; }

        public string Stepping { get; set; }

        public string ExtFamily { get; set; }

        public string ExtModel { get; set; }

        public string Revision { get; set; }

        public string Instrcutions { get; set; }

        public string L1DataCache { get; set; }

        public string L1Trace { get; set; }

        public string L2Cache { get; set; }

        public string L3Cache { get; set; }

        public int CoreCount { get; set; }

        public int ThreadCount { get; set; }

        public MyProcessorVendors Vendor { get; protected set; }

        public string CpuPort { get; protected set; }

        public uint ProcessorSignature { get; set; }

        public void GetCurrentFrequency(
          out string cpuFrequency,
          out string bclkFrequency,
          out string multipilier)
        {
            try
            {
                this.RetrieveCurrentFrequency(out cpuFrequency, out bclkFrequency, out multipilier);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void UpdateMemberVariables()
        {
            StringBuilder stringBuilder1 = new StringBuilder(256);
            StringBuilder stringBuilder2 = new StringBuilder(256);
            string empty = string.Empty;
            string cpuPort = this.CpuPort;
            stringBuilder1.Append(cpuPort);
            MyNativeCpuApi.GBFunc2(0, 0, stringBuilder1, stringBuilder2);
            this.ThreadCount = int.Parse(stringBuilder2.ToString());
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(1, 0, stringBuilder1, stringBuilder2);
            this.CoreCount = int.Parse(stringBuilder2.ToString());
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(2, 0, stringBuilder1, stringBuilder2);
            this.CodeName = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(7, 0, stringBuilder1, stringBuilder2);
            this.BrandID = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(4, 0, stringBuilder1, stringBuilder2);
            this.Socket = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(3, 0, stringBuilder1, stringBuilder2);
            this.Technology = stringBuilder2.ToString();
            this.CoreVoltage = string.Empty;
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(8, 0, stringBuilder1, stringBuilder2);
            this.Family = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(9, 0, stringBuilder1, stringBuilder2);
            this.Model = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(10, 0, stringBuilder1, stringBuilder2);
            this.Stepping = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(11, 0, stringBuilder1, stringBuilder2);
            this.ExtFamily = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(12, 0, stringBuilder1, stringBuilder2);
            this.ExtModel = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(13, 0, stringBuilder1, stringBuilder2);
            this.Revision = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(14, 0, stringBuilder1, stringBuilder2);
            this.Specification = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(30, 0, stringBuilder1, stringBuilder2);
            this.Instrcutions = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(21, 0, stringBuilder1, stringBuilder2);
            this.L1DataCache = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(22, 0, stringBuilder1, stringBuilder2);
            this.L1Trace = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(23, 0, stringBuilder1, stringBuilder2);
            this.L2Cache = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(24, 0, stringBuilder1, stringBuilder2);
            this.L3Cache = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(31, 0, stringBuilder1, stringBuilder2);
            this.Name = stringBuilder2.ToString();
            stringBuilder1.Clear();
            stringBuilder2.Clear();
            MyNativeCpuApi.GBFunc2(36, 0, stringBuilder1, stringBuilder2);
            this.TDP = stringBuilder2.ToString();
            if (this.Name.Length > 0)
                this.Vendor = this.Name.IndexOf("Intel") >= 0 ? MyProcessorVendors.Intel : MyProcessorVendors.AMD;
            uint pProcessorSignature = 0;
            Opcode.Open();
            this.RetrieveProcessorSignature(out pProcessorSignature);
            Opcode.Close();
            this.ProcessorSignature = pProcessorSignature;
        }

        protected void RetrieveProcessorSignature(out uint pProcessorSignature)
        {
            uint edx = 0;
            ulong threadAffinityMask = 1;
            pProcessorSignature = 0U;
            uint eax;
            if (!Opcode.CpuidTx(1U, 0U, out eax, out uint _, out uint _, out edx, threadAffinityMask))
                return;
            pProcessorSignature = eax;
        }

        protected void RetrieveProcessorSignatureByWmi(out uint pProcessorSignature)
        {
            pProcessorSignature = 0U;
            try
            {
                ManagementObjectCollection instances = new ManagementClass("Win32_Processor").GetInstances();
                string str1 = (string)null;
                using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
                {
                    if (enumerator.MoveNext())
                        str1 = enumerator.Current.Properties["ProcessorId"].Value.ToString();
                }
                string str2 = "0x" + str1.Substring(8, 8);
                pProcessorSignature = Convert.ToUInt32(str2, 16);
            }
            catch (Exception ex)
            {
            }
        }

        protected void RetrieveCurrentFrequency(
          out string cpuFrequency,
          out string bclkFrequency,
          out string multipilier)
        {
            StringBuilder stringBuilder1 = new StringBuilder(256);
            StringBuilder stringBuilder2 = new StringBuilder(256);
            cpuFrequency = bclkFrequency = multipilier = string.Empty;
            try
            {
                string cpuPort = this.CpuPort;
                stringBuilder1.Append(cpuPort);
                MyNativeCpuApi.GBFunc2(0, 0, stringBuilder1, stringBuilder2);
                stringBuilder1.Clear();
                stringBuilder2.Clear();
                MyNativeCpuApi.GBFunc2(25, 0, stringBuilder1, stringBuilder2);
                cpuFrequency = stringBuilder2.ToString() + " MHZ";
                stringBuilder1.Clear();
                stringBuilder2.Clear();
                MyNativeCpuApi.GBFunc2(26, 0, stringBuilder1, stringBuilder2);
                bclkFrequency = stringBuilder2.ToString() + " MHZ";
                stringBuilder1.Clear();
                stringBuilder2.Clear();
                MyNativeCpuApi.GBFunc2(28, 0, stringBuilder1, stringBuilder2);
                multipilier = stringBuilder2.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void RetrieveCurrentFrequencyEx(
          out string cpuFrequency,
          out string bclkFrequency,
          out string multipilier)
        {
            StringBuilder stringBuilder = new StringBuilder(256);
            IntPtr zero = IntPtr.Zero;
            string str = string.Empty;
            cpuFrequency = bclkFrequency = multipilier = string.Empty;
            try
            {
                string cpuPort = this.CpuPort;
                stringBuilder.Append(cpuPort);
                str = Marshal.PtrToStringAnsi(MyNativeCpuApi.GBFunc(0U, 0U, stringBuilder, (byte)0));
                string stringAnsi1 = Marshal.PtrToStringAnsi(MyNativeCpuApi.GBFunc(25U, 0U, stringBuilder, (byte)0));
                cpuFrequency = stringAnsi1 + " MHZ";
                string stringAnsi2 = Marshal.PtrToStringAnsi(MyNativeCpuApi.GBFunc(26U, 0U, stringBuilder, (byte)0));
                bclkFrequency = stringAnsi2 + " MHZ";
                string stringAnsi3 = Marshal.PtrToStringAnsi(MyNativeCpuApi.GBFunc(28U, 0U, stringBuilder, (byte)0));
                multipilier = stringAnsi3;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
