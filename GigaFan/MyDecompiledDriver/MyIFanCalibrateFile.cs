﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.IFanCalibrateFile
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public interface MyIFanCalibrateFile
    {
        bool IsCalibrateFileExist();

        bool IsCalibrateFileExist(int fanControlIndex);

        void ReadCalibrateData(int fanControlIndex, ref MyFanSpeedDataCollection pFanSpeedDatas);

        void WriteCalibrateData(int fanControlIndex, ref MyFanSpeedDataCollection pFanSpeedDatas);

        void DeleteCalibrateData(int fanControlIndex);
    }
}
