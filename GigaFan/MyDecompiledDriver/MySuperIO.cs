﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.SuperIO
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll


namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MySuperIO
    {
        protected const byte DEVCIE_SELECT_REGISTER = 7;

        protected byte ReadByte(ushort registerPort, ushort valuePort, byte register)
        {
            MyNativeYccMethods.gb_outp((uint)registerPort, register);
            return MyNativeYccMethods.gb_inp((uint)valuePort);
        }

        protected void WriteByte(ushort registerPort, ushort valuePort, byte register, byte value)
        {
            MyNativeYccMethods.gb_outp((uint)registerPort, register);
            MyNativeYccMethods.gb_outp((uint)valuePort, value);
        }

        protected ushort ReadWord(ushort registerPort, ushort valuePort, byte register)
        {
            return (ushort)((uint)this.ReadByte(registerPort, valuePort, register) << 8 | (uint)this.ReadByte(registerPort, valuePort, (byte)((uint)register + 1U)));
        }

        protected void Select(ushort registerPort, ushort valuePort, byte logicalDeviceNumber)
        {
            MyNativeYccMethods.gb_outp((uint)registerPort, (byte)7);
            MyNativeYccMethods.gb_outp((uint)valuePort, logicalDeviceNumber);
        }
    }
}
