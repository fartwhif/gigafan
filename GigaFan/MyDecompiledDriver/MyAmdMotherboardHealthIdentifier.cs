﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.Amd.AmdMotherboardHealthIdentifier
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyAmdMotherboardHealthIdentifier : MyMotherboardHealthIdentifierBase
    {
        public MyAmdMotherboardHealthIdentifier() => this.Vendor = MyProcessorVendors.AMD;

        public void GetGroupNumber(
          out MyAmdMotherboardHealthIdentification pGroupNumber)
        {
            uint pGroupNumber1 = 0;
            pGroupNumber = MyAmdMotherboardHealthIdentification.MHID_Unknown;
            try
            {
                this.RetrieveGroupNumber(out pGroupNumber1);
                if (pGroupNumber1 > 0U)
                {
                    if (this.IsSupport(pGroupNumber1))
                    {
                        MyAmdMotherboardHealthIdentification healthid = (MyAmdMotherboardHealthIdentification)pGroupNumber1;
                        this.CheckSupport(healthid);
                        pGroupNumber = healthid;
                    }
                }
            }
            catch
            {
                pGroupNumber = MyAmdMotherboardHealthIdentification.MHID_Unknown;
            }
            if (pGroupNumber != MyAmdMotherboardHealthIdentification.MHID_Unknown)
                return;
            try
            {
                if (this.m_motherboardInfo == null)
                    this.m_motherboardInfo = new MyMotherboard();
                this.RetrieveGroupNumber(this.m_motherboardInfo.Model, out pGroupNumber);
            }
            catch
            {
                pGroupNumber = MyAmdMotherboardHealthIdentification.MHID_Unknown;
            }
        }

        public override bool IsSupport(uint groupNumber)
        {
            try
            {
                return this.CheckSupport((MyAmdMotherboardHealthIdentification)groupNumber);
            }
            catch
            {
                return false;
            }
        }

        public bool IsSupport(MyAmdMotherboardHealthIdentification groupNumber)
        {
            return this.CheckSupport(groupNumber);
        }

        private void RetrieveGroupNumber(
          string motherboardModel,
          out MyAmdMotherboardHealthIdentification pGroupNumber)
        {
            pGroupNumber = MyAmdMotherboardHealthIdentification.MHID_Unknown;
        }

        private bool CheckSupport(MyAmdMotherboardHealthIdentification healthid)
        {
            bool flag;
            switch (healthid)
            {
                case MyAmdMotherboardHealthIdentification.MHID_B001:
                case MyAmdMotherboardHealthIdentification.MHID_B002:
                    flag = true;
                    break;
                default:
                    flag = false;
                    break;
            }
            return flag;
        }
    }
}
