﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Mainboard.Motherboard
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Collections.Generic;
using System.Text;

namespace GigaFan.MyDecompiledEnvironmentControl
{
  internal class MyMotherboard
  {
    private const uint VAL_Z97SERIES = 2361688198;
    private const uint VAL_H97SERIES = 2361819270;
    private const uint VAL_Z87SERIES = 2353299590;
    private const uint VAL_Z85SERIES = 2353430662;
    private const uint VAL_H81SERIES = 2354872454;
    private const uint VAL_H86SERIES = 2353627270;
    private const uint VAL_H87SERIES = 2353692806;
    private const uint VAL_Q85SERIES = 2353823878;
    private const uint VAL_Q87SERIES = 2353954950;
    private const uint VAL_B85SERIES = 2354086022;
    private const uint VAL_X79SERIES = 490832006;
    private const uint VAL_X99SERIES = 2290647174;
    private List<uint> m_autoTuningSupportList;

    public MyMotherboard()
    {
      uint pSeriesNumber = 0;
      MySeriesChipsets pChipsetInfo = MySeriesChipsets.Unknow;
      string pModel;
      string pBiosVersion;
      this.RetrieveBoardInfo(out pModel, out pBiosVersion);
      this.BiosVersion = pBiosVersion;
      this.Model = pModel;
      MyMotherboard.RetrieveMotherboardSeriesNumber(out pSeriesNumber);
      this.SeriesNumber = pSeriesNumber;
      MyMotherboard.RetrieveChipsetInfo(pSeriesNumber, out pChipsetInfo);
      this.Chipset = pChipsetInfo;
      this.InitAutoTuningVariables();
    }

    public string Model { get; protected set; }

    public string BiosVersion { get; protected set; }

    public uint SeriesNumber { get; protected set; }

    public MySeriesChipsets Chipset { get; protected set; }

    public bool IsSupportAutoTuning()
    {
      bool bSupported = false;
      try
      {
        this.CheckAutoTuningSupportStatus(out bSupported);
      }
      catch (Exception ex)
      {
      }
      return bSupported;
    }

    public static void GetSeriesChipsets(ref MySeriesChipsets pChipsetInfo)
    {
      uint pSeriesNumber = 0;
      MySeriesChipsets pChipsetInfo1 = MySeriesChipsets.Unknow;
      pChipsetInfo = MySeriesChipsets.Unknow;
      MyMotherboard.RetrieveMotherboardSeriesNumber(out pSeriesNumber);
      MyMotherboard.RetrieveChipsetInfo(pSeriesNumber, out pChipsetInfo1);
      pChipsetInfo = pChipsetInfo1;
    }

    private void InitAutoTuningVariables()
    {
      if (this.m_autoTuningSupportList == null)
        this.m_autoTuningSupportList = new List<uint>();
      if (this.m_autoTuningSupportList.Count > 0)
        this.m_autoTuningSupportList.Clear();
      this.m_autoTuningSupportList.Add(507805830U);
      this.m_autoTuningSupportList.Add(508133510U);
      this.m_autoTuningSupportList.Add(508199046U);
      this.m_autoTuningSupportList.Add(508002438U);
      this.m_autoTuningSupportList.Add(2353299590U);
      this.m_autoTuningSupportList.Add(2361688198U);
      this.m_autoTuningSupportList.Add(2361819270U);
      this.m_autoTuningSupportList.Add(2290647174U);
    }

    private void RetrieveBiosVersion(out string pBiosVersion)
    {
      StringBuilder stringBuilder1 = new StringBuilder((int) byte.MaxValue);
      StringBuilder stringBuilder2 = new StringBuilder((int) byte.MaxValue);
      pBiosVersion = string.Empty;
      try
      {
        MyNativeMbApi.GBFunc(16, 0, stringBuilder1, stringBuilder2);
        pBiosVersion = stringBuilder2.ToString();
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    private void RetrieveModel(out string pModel)
    {
      StringBuilder stringBuilder1 = new StringBuilder((int) byte.MaxValue);
      StringBuilder stringBuilder2 = new StringBuilder((int) byte.MaxValue);
      pModel = string.Empty;
      try
      {
        MyNativeMbApi.GBFunc(2, 0, stringBuilder1, stringBuilder2);
        pModel = stringBuilder2.ToString();
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    private void RetrieveBoardInfo(out string pModel, out string pBiosVersion)
    {
      new BdrStructure().GetMotherboardInfo(out pModel, out pBiosVersion);
    }

    private static void RetrieveChipsetInfo(uint seriesNumber, out MySeriesChipsets pChipsetInfo)
    {
      pChipsetInfo = MySeriesChipsets.Unknow;
      if (seriesNumber == 0U)
        return;
      if (new MyGenericCPU().Vendor == MyProcessorVendors.Intel)
      {
        switch (seriesNumber)
        {
          case 2290647174:
            pChipsetInfo = MySeriesChipsets.Intel_X99Series;
            break;
          case 2353299590:
          case 2361688198:
            pChipsetInfo = MySeriesChipsets.Intel_Z87Series;
            break;
          case 2353430662:
            pChipsetInfo = MySeriesChipsets.Intel_Z85Series;
            break;
          case 2353627270:
            pChipsetInfo = MySeriesChipsets.Intel_H86Series;
            break;
          case 2353692806:
          case 2361819270:
            pChipsetInfo = MySeriesChipsets.Intel_H87Series;
            break;
          case 2353823878:
            pChipsetInfo = MySeriesChipsets.Intel_Q85Series;
            break;
          case 2353954950:
            pChipsetInfo = MySeriesChipsets.Intel_Q87Series;
            break;
          case 2354086022:
            pChipsetInfo = MySeriesChipsets.Intel_B85Series;
            break;
          case 2354872454:
            pChipsetInfo = MySeriesChipsets.Intel_H81Series;
            break;
        }
      }
      else
        pChipsetInfo = MySeriesChipsets.AMD_A85X;
    }

    private static void RetrieveMotherboardSeriesNumber(out uint pSeriesNumber)
    {
      uint num1 = 2147483648;
      try
      {
        pSeriesNumber = 0U;
        uint num2 = 0;
        uint num3 = 31;
        uint num4 = 0;
        uint num5 = 0;
        MyNativeYccMethods.gb_outpd(3320U, (ulong) (num1 | (uint) ((int) num2 << 16 | (int) num3 << 11 | (int) num4 << 8) | num5));
        uint num6 = MyNativeYccMethods.gb_inpd(3324U);
        if (num6 == 0U)
          return;
        pSeriesNumber = num6;
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    private void CheckAutoTuningSupportStatus(out bool bSupported)
    {
      uint pSeriesNumber = 0;
      bSupported = false;
      if (this.m_autoTuningSupportList.Count == 0)
        return;
      try
      {
        MyMotherboard.RetrieveMotherboardSeriesNumber(out pSeriesNumber);
        foreach (int autoTuningSupport in this.m_autoTuningSupportList)
        {
          if (autoTuningSupport == (int) pSeriesNumber)
          {
            bSupported = true;
            break;
          }
        }
      }
      catch (Exception ex)
      {
        throw;
      }
    }
  }
}
