﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.SmartGuardianAutomaticModeRegister
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MySmartGuardianAutomaticModeRegister
    {
        public byte TemperatureLimitValueOfOff { get; set; }

        public byte TemperatureLimitValueOfFanStart { get; set; }

        public byte TemperatureLimitValueOfFullSpeed { get; set; }

        public byte StartPwm { get; set; }

        public byte Control { get; set; }

        public byte DeltaTemperature { get; set; }

        public byte TargetZone { get; set; }
    }
}
