﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.Intel.FanRegulatorEx2
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Collections.Generic;
using System.Threading;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyFanRegulatorEx2 : MyFanRegulatorEx
    {
        private const int VAL_MAX_EC_FANCONTROL = 3;
        private List<bool> m_fanControlEnableTable;
        private MyMotherboardHealthIdentifier m_mbHealthIdentifier;
        protected MyVirtualToPhysicalIndexTable m_virtual2PhysicalFanTachometerTable;

        public MyFanRegulatorEx2()
          : this(MyMotherboardHealthIdentification.MHID_Unknown)
        {
        }

        public MyFanRegulatorEx2(MyMotherboardHealthIdentification healthid)
        {
            MyMotherboardHealthIdentification pGroupNumber = MyMotherboardHealthIdentification.MHID_Unknown;
            if (healthid == MyMotherboardHealthIdentification.MHID_Unknown)
            {
                if (this.m_mbHealthIdentifier == null)
                    this.m_mbHealthIdentifier = new MyMotherboardHealthIdentifier();
                this.m_mbHealthIdentifier.GetGroupNumber(out pGroupNumber);
            }
            else
                pGroupNumber = healthid;
            this.InitObjects(pGroupNumber);
        }

        public MyMotherboardHealthIdentification HealthNumber { get; protected set; }

        public override void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            if (smartGuardianFanConfigs.Count == 0)
                throw new ArgumentOutOfRangeException();
            if (slopeIndex >= smartGuardianFanConfigs.Count)
                throw new ArgumentOutOfRangeException();
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                        this.m_h2ramMgr.SetFanSmartGuardianConfig(fanControlIndex, ref smartGuardianFanConfigs);
                    else
                        this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), slopeIndex, ref smartGuardianFanConfigs);
                }
                else
                    this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), slopeIndex, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                    {
                        int fanCtrlIndex = fanControlIndex;
                        if (bCalibrateMode)
                            this.m_h2ramMgr.ResetCalibrationPwm();
                        else
                            this.m_h2ramMgr.SetFanSmartGuardianConfig(fanCtrlIndex, ref smartGuardianFanConfigs);
                    }
                    else
                        this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), slopeIndex, ref smartGuardianFanConfigs);
                }
                else
                    this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), slopeIndex, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfig smartGuardianFanConfig)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                    {
                        int fanCtrlIndex = fanControlIndex;
                        MySmartGuardianFanConfigCollection smartGuardianFanConfigs = new MySmartGuardianFanConfigCollection();
                        smartGuardianFanConfigs.Add(smartGuardianFanConfig);
                        this.m_h2ramMgr.SetFanSmartGuardianConfig(fanCtrlIndex, ref smartGuardianFanConfigs);
                    }
                    else
                        this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), ref smartGuardianFanConfig);
                }
                else
                    this.WriteProtectValue2it8620(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref smartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanSpeed(int fanIndex, out float fanSpeed)
        {
            try
            {
                fanSpeed = 0.0f;
                List<float> pHardwareMonitorDatas = new List<float>();
                if (this.H2RamFlag)
                {
                    if (fanIndex < this.m_h2ramMgr.FanTachometerCount)
                    {
                        if (this.m_ite879xMgr != null)
                        {
                            this.m_ite879xMgr.Update();
                            this.m_ite879xMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        }
                        else
                        {
                            this.m_h2ramMgr.Update();
                            this.m_h2ramMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        }
                        if (fanIndex >= pHardwareMonitorDatas.Count)
                            return;
                        fanSpeed = pHardwareMonitorDatas[fanIndex];
                    }
                    else
                    {
                        int index = this.m_virtual2PhysicalFanTachometerTable.Virtual2PhysicalIndex(fanIndex - this.m_h2ramMgr.FanTachometerCount);
                        this.m_iteMgr.Update();
                        this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        if (index >= pHardwareMonitorDatas.Count)
                            return;
                        fanSpeed = pHardwareMonitorDatas[index];
                    }
                }
                else
                {
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    int index = this.m_virtual2PhysicalFanTachometerTable.Virtual2PhysicalIndex(fanIndex);
                    if (index >= pHardwareMonitorDatas.Count)
                        return;
                    fanSpeed = pHardwareMonitorDatas[index];
                }
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanSpeed(int fanIndex, bool bSwitchFanIndex, out float fanSpeed)
        {
            this.GetFanSpeed(fanIndex, out fanSpeed);
        }

        public override void GetFanDisplayName(int fanIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (!this.IsSupported)
                throw new NotSupportedException();
            this.RetrieveFanDisplayName(this.HealthNumber, fanIndex, out pDisplayName);
        }

        public override void GetFanControlDisplayName(int fanControlIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            this.RetrieveFanControlDisplayName(this.HealthNumber, fanControlIndex, out pDisplayName);
        }

        public override void SetCalibrationPwm(int fanControlIndex, byte pwmPercentage)
        {
            MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
            try
            {
                float maxPwmValue = (float)this.m_iteMgr.MaxPwmValue;
                float d = (float)pwmPercentage * (maxPwmValue / 100f);
                float num1 = 50f;
                float num2 = 80f;
                float num3 = (float)(((double)this.m_iteMgr.MaxPwmValue - (double)d) / ((double)num2 - (double)num1));
                fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                fanConfig.TemperatureLimitValueOfFanStart = (byte)num1;
                fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)num2;
                fanConfig.StartPWM = (byte)Math.Floor((double)d);
                fanConfig.Slope = num3;
                fanConfig.DeltaTemperature = (byte)3;
                fanConfig.TargetZoneBoundary = (byte)0;
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                        this.m_h2ramMgr.SetCalibrationPwm((int)fanConfig.StartPWM);
                    else
                        this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), ref fanConfig);
                }
                else
                    this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void SetFanSpeedFixedMode(int fanControlIndex, byte pwmPercentage)
        {
            MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
            try
            {
                float maxPwmValue = (float)this.m_iteMgr.MaxPwmValue;
                float d = (float)pwmPercentage * (maxPwmValue / 100f);
                float num1 = 70f;
                float num2 = 71f;
                float num3 = (float)(((double)this.m_iteMgr.MaxPwmValue - (double)d) / ((double)num2 - (double)num1));
                fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                fanConfig.TemperatureLimitValueOfFanStart = (byte)num1;
                fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)num2;
                fanConfig.StartPWM = (byte)Math.Floor((double)d);
                fanConfig.Slope = (double)num3 == 0.0 ? 0.0f : 1f;
                fanConfig.DeltaTemperature = (byte)3;
                fanConfig.TargetZoneBoundary = (byte)0;
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                        this.m_h2ramMgr.SetFanSmartGuardianConfig(fanControlIndex, (int)fanConfig.StartPWM);
                    else
                        this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount), ref fanConfig);
                }
                else
                    this.m_iteMgr.SetFanSmartGuardianConfig(this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex), ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void GetTemperatureInputSelection(
          int fanControlIndex,
          ref MyFanTemperatureInput pTemperatureInputSelection)
        {
            pTemperatureInputSelection = MyFanTemperatureInput.Unknown;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (fanControlIndex >= this.FanControlCount)
                throw new ArgumentOutOfRangeException();
            try
            {
                if (this.H2RamFlag)
                {
                    if (fanControlIndex < this.m_h2ramMgr.FanControlRegisterCount)
                    {
                        this.m_h2ramMgr.GetTemperatureInputSelection(fanControlIndex, ref pTemperatureInputSelection);
                    }
                    else
                    {
                        int fanControlIndex1 = this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex - this.m_h2ramMgr.FanControlRegisterCount);
                        if (this.HealthNumber == MyMotherboardHealthIdentification.MHID_F001)
                            this.m_iteMgr.GetTemperatureInputSelection(fanControlIndex1, true, ref pTemperatureInputSelection);
                        else
                            this.m_iteMgr.GetTemperatureInputSelection(fanControlIndex1, ref pTemperatureInputSelection);
                        this.PatchTemperatureInputSelection(this.HealthNumber, pTemperatureInputSelection, ref pTemperatureInputSelection);
                    }
                }
                else
                {
                    int fanControlIndex2 = this.m_virtual2PhysicalFanControlTable.Virtual2PhysicalIndex(fanControlIndex);
                    if (this.HealthNumber == MyMotherboardHealthIdentification.MHID_F001)
                        this.m_iteMgr.GetTemperatureInputSelection(fanControlIndex2, true, ref pTemperatureInputSelection);
                    else
                        this.m_iteMgr.GetTemperatureInputSelection(fanControlIndex2, ref pTemperatureInputSelection);
                }
            }
            catch
            {
                throw;
            }
        }

        public override void GetHardwareMonitorDatas(
          ref List<float> pTemperatureDatas,
          ref List<float> pFanSpeedDatas)
        {
            if (!this.IsSupported)
                throw new NotSupportedException();
            try
            {
                this.RetrieveTemperatureHardwareMonitorDatas(ref pTemperatureDatas);
                this.RetrieveFanHardwareMonitorDatas(ref pFanSpeedDatas);
            }
            catch
            {
            }
        }

        protected void InitFanObjects(MyMotherboardHealthIdentification healthNumber)
        {
            if (this.m_iteMgr != null)
                return;
            try
            {
                MyIT87XXFinder it87XxFinder = new MyIT87XXFinder();
                bool flag1;
                bool flag2;
                switch (healthNumber)
                {
                    case MyMotherboardHealthIdentification.MHID_A001:
                    case MyMotherboardHealthIdentification.MHID_A002:
                    case MyMotherboardHealthIdentification.MHID_A003:
                        flag1 = true;
                        flag2 = true;
                        break;
                    case MyMotherboardHealthIdentification.MHID_E008:
                    case MyMotherboardHealthIdentification.MHID_E009:
                        flag1 = true;
                        flag2 = true;
                        break;
                    default:
                        flag1 = true;
                        flag2 = false;
                        break;
                }
                if (flag1)
                {
                    for (int index = 0; index < 6; ++index)
                    {
                        it87XxFinder.Find(true, out this.m_iteMgr);
                        if (this.m_iteMgr == null)
                            Thread.Sleep(1000);
                        else
                            break;
                    }
                }
                if (flag2)
                {
                    for (int index = 0; index < 2; ++index)
                    {
                        it87XxFinder.Find(out this.m_h2ramMgr);
                        if (this.m_h2ramMgr == null)
                            Thread.Sleep(1000);
                        else
                            break;
                    }
                }
                if (this.m_iteMgr == null)
                    throw new NotSupportedException();
                if (this.m_h2ramMgr != null)
                    this.m_ite879xMgr = (MyIT87XX)new MyIT8790(this.m_h2ramMgr.chip, this.m_h2ramMgr.BaseAddress, this.m_h2ramMgr.GpioAddress, this.m_h2ramMgr.Version);
                this.FanControlCount = this.m_iteMgr.ActualFanControlRegisterCount;
                this.IsSupported = true;
                this.MaxPwmValue = this.m_iteMgr.MaxPwmValue;
                this.FanCount += this.m_iteMgr.FanTachometerCount;
                if (this.m_h2ramMgr == null)
                    return;
                this.FanControlCount += this.m_h2ramMgr.FanControlRegisterCount;
                this.H2RamFlag = true;
                this.FanCount += 3;
            }
            catch
            {
                throw;
            }
        }

        protected virtual void InitFanRelativeVariables(MyMotherboardHealthIdentification healthid)
        {
            switch (healthid)
            {
                case MyMotherboardHealthIdentification.MHID_A001:
                    this.FanControlCount = 7;
                    this.FanCount = 7;
                    break;
                case MyMotherboardHealthIdentification.MHID_A002:
                    this.FanControlCount = 6;
                    this.FanCount = 6;
                    break;
                case MyMotherboardHealthIdentification.MHID_A003:
                    this.FanControlCount = 5;
                    this.FanCount = 5;
                    break;
                case MyMotherboardHealthIdentification.MHID_B001:
                case MyMotherboardHealthIdentification.MHID_B002:
                case MyMotherboardHealthIdentification.MHID_B003:
                    this.FanControlCount = 5;
                    this.FanCount = 5;
                    break;
                case MyMotherboardHealthIdentification.MHID_C001:
                    this.FanControlCount = 4;
                    this.FanCount = 4;
                    break;
                case MyMotherboardHealthIdentification.MHID_C002:
                    this.FanControlCount = 3;
                    this.FanCount = 3;
                    break;
                case MyMotherboardHealthIdentification.MHID_C003:
                    this.FanControlCount = 2;
                    this.FanCount = 2;
                    break;
                case MyMotherboardHealthIdentification.MHID_D001:
                    this.FanControlCount = 3;
                    this.FanCount = 4;
                    break;
                case MyMotherboardHealthIdentification.MHID_E001:
                    this.FanControlCount = 2;
                    this.FanCount = 2;
                    break;
                case MyMotherboardHealthIdentification.MHID_E002:
                case MyMotherboardHealthIdentification.MHID_E003:
                    this.FanControlCount = 3;
                    this.FanCount = 3;
                    break;
                case MyMotherboardHealthIdentification.MHID_E004:
                    this.FanControlCount = 4;
                    this.FanCount = 4;
                    break;
                case MyMotherboardHealthIdentification.MHID_E005:
                case MyMotherboardHealthIdentification.MHID_E006:
                case MyMotherboardHealthIdentification.MHID_E007:
                    this.FanControlCount = 5;
                    this.FanCount = 5;
                    break;
                case MyMotherboardHealthIdentification.MHID_E008:
                    this.FanControlCount = 7;
                    this.FanCount = 7;
                    break;
                case MyMotherboardHealthIdentification.MHID_E009:
                    this.FanControlCount = 6;
                    this.FanCount = 6;
                    break;
                case MyMotherboardHealthIdentification.MHID_F001:
                    this.FanControlCount = 4;
                    this.FanCount = 4;
                    break;
            }
        }

        protected virtual void InitFanControlSupportTable(MyMotherboardHealthIdentification healthid)
        {
            if (this.m_fanControlEnableTable == null)
                this.m_fanControlEnableTable = new List<bool>();
            if (this.m_fanControlEnableTable.Count > 0)
                this.m_fanControlEnableTable.Clear();
            if (this.FanControlCount == 0)
                return;
            for (int index = 0; index < this.FanControlCount; ++index)
                this.m_fanControlEnableTable.Add(true);
            switch (healthid)
            {
                case MyMotherboardHealthIdentification.MHID_A001:
                case MyMotherboardHealthIdentification.MHID_A002:
                case MyMotherboardHealthIdentification.MHID_A003:
                    if (this.m_h2ramMgr == null)
                    {
                        for (int index = 0; index < 3; ++index)
                            this.m_fanControlEnableTable[index] = false;
                    }
                    if (this.m_iteMgr != null)
                        break;
                    for (int index = 3; index < this.m_fanControlEnableTable.Count; ++index)
                        this.m_fanControlEnableTable[index] = false;
                    break;
                default:
                    if (this.m_iteMgr != null)
                        break;
                    for (int index = 0; index < this.m_fanControlEnableTable.Count; ++index)
                        this.m_fanControlEnableTable[index] = false;
                    break;
            }
        }

        protected virtual void InitFanMappingTable(MyMotherboardHealthIdentification healthid)
        {
            if (this.m_virtual2PhysicalFanControlTable == null)
                this.m_virtual2PhysicalFanControlTable = new MyVirtualToPhysicalIndexTable();
            if (this.m_virtual2PhysicalFanControlTable.Count > 0)
                this.m_virtual2PhysicalFanControlTable.Clear();
            switch (healthid)
            {
                case MyMotherboardHealthIdentification.MHID_A001:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(3);
                    break;
                case MyMotherboardHealthIdentification.MHID_A002:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    break;
                case MyMotherboardHealthIdentification.MHID_A003:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    break;
                case MyMotherboardHealthIdentification.MHID_B001:
                case MyMotherboardHealthIdentification.MHID_B002:
                case MyMotherboardHealthIdentification.MHID_B003:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(4);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(3);
                    break;
                case MyMotherboardHealthIdentification.MHID_C001:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(3);
                    break;
                case MyMotherboardHealthIdentification.MHID_C002:
                case MyMotherboardHealthIdentification.MHID_D001:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    break;
                case MyMotherboardHealthIdentification.MHID_C003:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    break;
                case MyMotherboardHealthIdentification.MHID_E001:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    break;
                case MyMotherboardHealthIdentification.MHID_E002:
                case MyMotherboardHealthIdentification.MHID_E003:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    break;
                case MyMotherboardHealthIdentification.MHID_E004:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(4);
                    break;
                case MyMotherboardHealthIdentification.MHID_E005:
                case MyMotherboardHealthIdentification.MHID_E006:
                case MyMotherboardHealthIdentification.MHID_E007:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(3);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(4);
                    break;
                case MyMotherboardHealthIdentification.MHID_E008:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(3);
                    break;
                case MyMotherboardHealthIdentification.MHID_E009:
                    this.m_virtual2PhysicalFanControlTable.Add(0);
                    this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    break;
                case MyMotherboardHealthIdentification.MHID_F001:
                    MyFanControlOutputModeSelection fanControlOutputMode1 = MyFanControlOutputModeSelection.SmartGuardianMode;
                    MyFanControlOutputModeSelection fanControlOutputMode2 = MyFanControlOutputModeSelection.SmartGuardianMode;
                    MyFanControlOutputModeSelection fanControlOutputMode3 = MyFanControlOutputModeSelection.SmartGuardianMode;
                    if (this.m_iteMgr != null)
                        this.m_iteMgr.GetFanControlOutputModeSelection(out fanControlOutputMode1, out fanControlOutputMode2, out fanControlOutputMode3);
                    if (fanControlOutputMode1 == MyFanControlOutputModeSelection.SmartGuardianMode && fanControlOutputMode2 == MyFanControlOutputModeSelection.OnOffMode)
                        this.m_virtual2PhysicalFanControlTable.Add(0);
                    else if (fanControlOutputMode1 == MyFanControlOutputModeSelection.OnOffMode && fanControlOutputMode2 == MyFanControlOutputModeSelection.SmartGuardianMode)
                        this.m_virtual2PhysicalFanControlTable.Add(1);
                    else
                        this.m_virtual2PhysicalFanControlTable.Add(1);
                    this.m_virtual2PhysicalFanControlTable.Add(4);
                    this.m_virtual2PhysicalFanControlTable.Add(2);
                    this.m_virtual2PhysicalFanControlTable.Add(3);
                    break;
            }
            if (this.m_virtual2PhysicalFanControlTable.Count == 0)
                throw new NotSupportedException();
        }

        protected virtual void InitFanTachometerTable(MyMotherboardHealthIdentification healthid)
        {
            if (this.m_virtual2PhysicalFanTachometerTable == null)
                this.m_virtual2PhysicalFanTachometerTable = new MyVirtualToPhysicalIndexTable();
            if (this.m_virtual2PhysicalFanTachometerTable.Count > 0)
                this.m_virtual2PhysicalFanTachometerTable.Clear();
            switch (healthid)
            {
                case MyMotherboardHealthIdentification.MHID_A001:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    this.m_virtual2PhysicalFanTachometerTable.Add(3);
                    break;
                case MyMotherboardHealthIdentification.MHID_A002:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    break;
                case MyMotherboardHealthIdentification.MHID_A003:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    break;
                case MyMotherboardHealthIdentification.MHID_B001:
                case MyMotherboardHealthIdentification.MHID_B002:
                case MyMotherboardHealthIdentification.MHID_B003:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(4);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    this.m_virtual2PhysicalFanTachometerTable.Add(3);
                    break;
                case MyMotherboardHealthIdentification.MHID_C001:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    this.m_virtual2PhysicalFanTachometerTable.Add(3);
                    break;
                case MyMotherboardHealthIdentification.MHID_C002:
                case MyMotherboardHealthIdentification.MHID_D001:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    break;
                case MyMotherboardHealthIdentification.MHID_C003:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    break;
                case MyMotherboardHealthIdentification.MHID_E001:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    break;
                case MyMotherboardHealthIdentification.MHID_E002:
                case MyMotherboardHealthIdentification.MHID_E003:
                case MyMotherboardHealthIdentification.MHID_E009:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    break;
                case MyMotherboardHealthIdentification.MHID_E004:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    this.m_virtual2PhysicalFanTachometerTable.Add(5);
                    break;
                case MyMotherboardHealthIdentification.MHID_E005:
                case MyMotherboardHealthIdentification.MHID_E006:
                case MyMotherboardHealthIdentification.MHID_E007:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(3);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    this.m_virtual2PhysicalFanTachometerTable.Add(4);
                    break;
                case MyMotherboardHealthIdentification.MHID_E008:
                    this.m_virtual2PhysicalFanTachometerTable.Add(0);
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    this.m_virtual2PhysicalFanTachometerTable.Add(3);
                    break;
                case MyMotherboardHealthIdentification.MHID_F001:
                    this.m_virtual2PhysicalFanTachometerTable.Add(1);
                    this.m_virtual2PhysicalFanTachometerTable.Add(4);
                    this.m_virtual2PhysicalFanTachometerTable.Add(2);
                    this.m_virtual2PhysicalFanTachometerTable.Add(3);
                    break;
            }
            if (this.m_virtual2PhysicalFanTachometerTable.Count == 0)
                throw new NotSupportedException();
        }

        protected virtual void InitTemperatureMappingTable(MyMotherboardHealthIdentification healthid)
        {
            if (this.m_temperatureInputTable == null)
                this.m_temperatureInputTable = new List<MyFanTemperatureInput>();
            if (this.m_temperatureInputTable.Count > 0)
                this.m_temperatureInputTable.Clear();
            switch (healthid)
            {
                case MyMotherboardHealthIdentification.MHID_A001:
                case MyMotherboardHealthIdentification.MHID_A002:
                case MyMotherboardHealthIdentification.MHID_B001:
                case MyMotherboardHealthIdentification.MHID_B002:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.PCH);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    break;
                case MyMotherboardHealthIdentification.MHID_A003:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM2);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.PCH);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    break;
                case MyMotherboardHealthIdentification.MHID_B003:
                case MyMotherboardHealthIdentification.MHID_C001:
                case MyMotherboardHealthIdentification.MHID_C002:
                case MyMotherboardHealthIdentification.MHID_C003:
                case MyMotherboardHealthIdentification.MHID_D001:
                case MyMotherboardHealthIdentification.MHID_F001:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.Unknown);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    break;
                case MyMotherboardHealthIdentification.MHID_E001:
                case MyMotherboardHealthIdentification.MHID_E002:
                case MyMotherboardHealthIdentification.MHID_E003:
                case MyMotherboardHealthIdentification.MHID_E004:
                case MyMotherboardHealthIdentification.MHID_E005:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.Unknown);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    break;
                case MyMotherboardHealthIdentification.MHID_E006:
                case MyMotherboardHealthIdentification.MHID_E007:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.PCH);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    break;
                case MyMotherboardHealthIdentification.MHID_E008:
                case MyMotherboardHealthIdentification.MHID_E009:
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM2);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.PCH);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.CPU);
                    this.m_temperatureInputTable.Add(MyFanTemperatureInput.SYSTEM);
                    break;
                default:
                    throw new NotSupportedException();
            }
            this.TemperatureCount = this.m_temperatureInputTable.Count;
        }

        protected virtual void RetrieveFanDisplayName(
          MyMotherboardHealthIdentification healthid,
          int fanControlIndex,
          out string pDdisplayName)
        {
            pDdisplayName = string.Empty;
            if (healthid == MyMotherboardHealthIdentification.MHID_Unknown)
                return;
            switch (healthid)
            {
                case MyMotherboardHealthIdentification.MHID_A001:
                case MyMotherboardHealthIdentification.MHID_A002:
                case MyMotherboardHealthIdentification.MHID_A003:
                case MyMotherboardHealthIdentification.MHID_B001:
                case MyMotherboardHealthIdentification.MHID_B002:
                case MyMotherboardHealthIdentification.MHID_B003:
                    this.RetrieveFanControlDisplayName(fanControlIndex, out pDdisplayName);
                    break;
                case MyMotherboardHealthIdentification.MHID_C001:
                case MyMotherboardHealthIdentification.MHID_C002:
                case MyMotherboardHealthIdentification.MHID_C003:
                case MyMotherboardHealthIdentification.MHID_F001:
                    this.RetrieveFanControlDisplayName4NonProcessorOption(fanControlIndex, out pDdisplayName);
                    break;
                case MyMotherboardHealthIdentification.MHID_D001:
                    this.RetrieveFanDisplayName4It8728(fanControlIndex, out pDdisplayName);
                    break;
            }
        }

        protected virtual void RetrieveFanDisplayName4It8728(int fanlIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            switch (fanlIndex)
            {
                case 0:
                    pDisplayName = "CPU Fan";
                    break;
                case 1:
                case 2:
                case 3:
                    pDisplayName = string.Format("System Fan {0}", (object)fanlIndex);
                    break;
                case 4:
                    pDisplayName = "CPU OPT";
                    break;
                default:
                    pDisplayName = "Unknown Fan";
                    break;
            }
        }

        protected virtual void RetrieveFanControlDisplayName(
          MyMotherboardHealthIdentification healthid,
          int fanControlIndex,
          out string pDdisplayName)
        {
            pDdisplayName = string.Empty;
            if (healthid == MyMotherboardHealthIdentification.MHID_Unknown)
                return;
            switch (healthid)
            {
                case MyMotherboardHealthIdentification.MHID_A001:
                case MyMotherboardHealthIdentification.MHID_A002:
                case MyMotherboardHealthIdentification.MHID_A003:
                case MyMotherboardHealthIdentification.MHID_B001:
                case MyMotherboardHealthIdentification.MHID_B002:
                case MyMotherboardHealthIdentification.MHID_B003:
                case MyMotherboardHealthIdentification.MHID_E005:
                case MyMotherboardHealthIdentification.MHID_E006:
                case MyMotherboardHealthIdentification.MHID_E007:
                case MyMotherboardHealthIdentification.MHID_E008:
                case MyMotherboardHealthIdentification.MHID_E009:
                    this.RetrieveFanControlDisplayName(fanControlIndex, out pDdisplayName);
                    break;
                case MyMotherboardHealthIdentification.MHID_C001:
                case MyMotherboardHealthIdentification.MHID_C002:
                case MyMotherboardHealthIdentification.MHID_C003:
                case MyMotherboardHealthIdentification.MHID_E001:
                case MyMotherboardHealthIdentification.MHID_E002:
                case MyMotherboardHealthIdentification.MHID_E003:
                case MyMotherboardHealthIdentification.MHID_E004:
                case MyMotherboardHealthIdentification.MHID_F001:
                    this.RetrieveFanControlDisplayName4NonProcessorOption(fanControlIndex, out pDdisplayName);
                    break;
                case MyMotherboardHealthIdentification.MHID_D001:
                    this.RetrieveFanControlDisplayName4It8728(fanControlIndex, out pDdisplayName);
                    break;
            }
        }

        protected virtual void RetrieveFanControlDisplayName(
          int fanControlIndex,
          out string pDisplayName)
        {
            pDisplayName = string.Empty;
            switch (fanControlIndex)
            {
                case 0:
                    pDisplayName = "CPU Fan";
                    break;
                case 1:
                    pDisplayName = "CPU OPT";
                    break;
                default:
                    int num = fanControlIndex - 1;
                    pDisplayName = string.Format("System Fan {0}", (object)num);
                    break;
            }
        }

        protected virtual void RetrieveFanControlDisplayName4NonProcessorOption(
          int fanControlIndex,
          out string pDdisplayName)
        {
            pDdisplayName = string.Empty;
            if (fanControlIndex == 0)
            {
                pDdisplayName = "CPU Fan";
            }
            else
            {
                int num = fanControlIndex;
                pDdisplayName = string.Format("System Fan {0}", (object)num);
            }
        }

        protected virtual void RetrieveFanControlDisplayName4It8728(
          int fanControlIndex,
          out string pDdisplayName)
        {
            pDdisplayName = string.Empty;
            switch (fanControlIndex)
            {
                case 0:
                    pDdisplayName = this.FanCount == 5 ? "CPU Fan & OPT" : (pDdisplayName = "CPU Fan");
                    break;
                case 1:
                    pDdisplayName = "System Fan 1";
                    break;
                case 2:
                    pDdisplayName = this.FanCount < 4 ? "System Fan 2" : "System Fan 2 & 3";
                    break;
                default:
                    pDdisplayName = "System Fan";
                    break;
            }
        }

        protected override void RetrieveTemperatureHardwareMonitorDatas(
          ref List<float> pTemperatureDatas)
        {
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (pTemperatureDatas.Count > 0)
                pTemperatureDatas.Clear();
            try
            {
                List<float> pHardwareMonitorDatas = new List<float>();
                if (this.H2RamFlag)
                {
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    for (int index = 0; index < pHardwareMonitorDatas.Count - 1; ++index)
                    {
                        if (index != 1 || this.m_temperatureInputTable[index] != MyFanTemperatureInput.Unknown)
                            pTemperatureDatas.Add(pHardwareMonitorDatas[index]);
                    }
                    if (this.m_ite879xMgr != null)
                    {
                        this.m_ite879xMgr.Update();
                        this.m_ite879xMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    }
                    else
                    {
                        this.m_h2ramMgr.Update();
                        this.m_h2ramMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    }
                    if (this.HealthNumber == MyMotherboardHealthIdentification.MHID_A003)
                    {
                        if (pHardwareMonitorDatas.Count <= 0)
                            return;
                        if (this.m_ite879xMgr != null)
                        {
                            pTemperatureDatas.Add(pHardwareMonitorDatas[0]);
                            pTemperatureDatas.Add(pHardwareMonitorDatas[1]);
                        }
                        else
                        {
                            pTemperatureDatas.Add(pHardwareMonitorDatas[0]);
                            pTemperatureDatas.Add(pHardwareMonitorDatas[2]);
                        }
                    }
                    else
                    {
                        if (pHardwareMonitorDatas.Count <= 0)
                            return;
                        pTemperatureDatas.Add(pHardwareMonitorDatas[0]);
                    }
                }
                else
                {
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    for (int index = 0; index < this.m_temperatureInputTable.Count; ++index)
                    {
                        if (index != 1 || this.m_temperatureInputTable[index] != MyFanTemperatureInput.Unknown)
                            pTemperatureDatas.Add(pHardwareMonitorDatas[index]);
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        protected virtual void RetrieveTemperatureHardwareMonitorDatasEx(
          ref List<float> pTemperatureDatas)
        {
            if (pTemperatureDatas.Count > 0)
                pTemperatureDatas.Clear();
            List<float> pHardwareMonitorDatas = new List<float>();
            this.m_iteMgr.Update();
            this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
            for (int index = 0; index < this.m_temperatureInputTable.Count; ++index)
            {
                if (index != 1 || this.m_temperatureInputTable[index] != MyFanTemperatureInput.Unknown)
                    pTemperatureDatas.Add(pHardwareMonitorDatas[index]);
            }
        }

        protected override void RetrieveFanHardwareMonitorDatas(ref List<float> pFanSpeeds)
        {
            if (pFanSpeeds.Count > 0)
                pFanSpeeds.Clear();
            try
            {
                List<float> pHardwareMonitorDatas = new List<float>();
                if (this.H2RamFlag)
                {
                    if (this.m_ite879xMgr != null)
                    {
                        this.m_ite879xMgr.Update();
                        this.m_ite879xMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        for (int index = 0; index < this.m_h2ramMgr.FanTachometerCount; ++index)
                            pFanSpeeds.Add(pHardwareMonitorDatas[index]);
                    }
                    else
                    {
                        this.m_h2ramMgr.Update();
                        this.m_h2ramMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                        for (int index = 0; index < pHardwareMonitorDatas.Count; ++index)
                            pFanSpeeds.Add(pHardwareMonitorDatas[index]);
                    }
                    this.m_iteMgr.Update();
                    this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                    for (int virtualIndex = 0; virtualIndex < this.m_virtual2PhysicalFanTachometerTable.Count; ++virtualIndex)
                    {
                        int index = this.m_virtual2PhysicalFanTachometerTable.Virtual2PhysicalIndex(virtualIndex);
                        float num = pHardwareMonitorDatas[index];
                        pFanSpeeds.Add(num);
                    }
                }
                else if (this.m_iteMgr.chip == MyChip.IT8728F)
                    this.RetrieveFanHardwareMonitorDatas4IT8728F(ref pFanSpeeds);
                else
                    this.RetrieveFanHardwareMonitorDatas4IT8620E(ref pFanSpeeds);
            }
            catch
            {
                throw;
            }
        }

        protected void RetrieveFanHardwareMonitorDatas4IT8728F(ref List<float> pFanSpeeds)
        {
            if (pFanSpeeds.Count > 0)
                pFanSpeeds.Clear();
            try
            {
                List<float> pHardwareMonitorDatas = new List<float>();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                if (pHardwareMonitorDatas.Count == 0)
                    return;
                for (int virtualIndex = 0; virtualIndex < this.m_virtual2PhysicalFanTachometerTable.Count; ++virtualIndex)
                {
                    int index = this.m_virtual2PhysicalFanTachometerTable.Virtual2PhysicalIndex(virtualIndex);
                    float num = pHardwareMonitorDatas[index];
                    pFanSpeeds.Add(num);
                }
                if (this.m_iteMgr.chip != MyChip.IT8728F || this.FanCount <= 3)
                    return;
                for (int index = 3; index < this.FanCount; ++index)
                {
                    float num = pHardwareMonitorDatas[index];
                    pFanSpeeds.Add(num);
                }
            }
            catch
            {
                throw;
            }
        }

        protected void RetrieveFanHardwareMonitorDatas4IT8620E(ref List<float> pFanSpeeds)
        {
            if (pFanSpeeds.Count > 0)
                pFanSpeeds.Clear();
            try
            {
                List<float> pHardwareMonitorDatas = new List<float>();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                if (pHardwareMonitorDatas.Count == 0)
                    return;
                for (int virtualIndex = 0; virtualIndex < this.m_virtual2PhysicalFanTachometerTable.Count; ++virtualIndex)
                {
                    int index = this.m_virtual2PhysicalFanTachometerTable.Virtual2PhysicalIndex(virtualIndex);
                    float num = pHardwareMonitorDatas[index];
                    pFanSpeeds.Add(num);
                }
            }
            catch
            {
                throw;
            }
        }

        private void InitObjects(MyMotherboardHealthIdentification healthNumber)
        {
            if (this.m_mbHealthIdentifier == null)
                this.m_mbHealthIdentifier = new MyMotherboardHealthIdentifier();
            this.HealthNumber = this.m_mbHealthIdentifier.IsSupport(healthNumber) ? healthNumber : throw new NotSupportedException();
            this.InitFanObjects(this.HealthNumber);
            this.InitFanMappingTable(this.HealthNumber);
            this.InitFanTachometerTable(this.HealthNumber);
            this.InitTemperatureMappingTable(this.HealthNumber);
            this.InitFanRelativeVariables(this.HealthNumber);
        }

        private void PatchTemperatureInputSelection(
          MyMotherboardHealthIdentification healthNumber,
          MyFanTemperatureInput sourceInputSelection,
          ref MyFanTemperatureInput destinationInputSelection)
        {
            if (healthNumber == MyMotherboardHealthIdentification.MHID_A003)
            {
                if (sourceInputSelection == MyFanTemperatureInput.SYSTEM)
                    destinationInputSelection = MyFanTemperatureInput.SYSTEM2;
                else
                    destinationInputSelection = sourceInputSelection;
            }
            else
                destinationInputSelection = sourceInputSelection;
        }

        private void PatchTemperatureInputSelection4H2Ram(
          MyMotherboardHealthIdentification healthNumber,
          MyFanTemperatureInput sourceInputSelection,
          ref MyFanTemperatureInput destinationInputSelection)
        {
            destinationInputSelection = sourceInputSelection;
        }

        private void WriteProtectValue2it8620(
          int fanControlIndex,
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                int fanControlIndex1 = fanControlIndex;
                if (this.UsingExtendedTemperature)
                {
                    MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
                    MySmartGuardianFanConfig originalFanConfig = smartGuardianFanConfigs[slopeIndex];
                    this.RetrieveProtectValueForExtendedTemperature(ref originalFanConfig, ref guardianFanConfig);
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref guardianFanConfig);
                }
                else
                {
                    MySmartGuardianFanConfig fanConfig = smartGuardianFanConfigs[slopeIndex];
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref fanConfig);
                }
            }
            catch
            {
                throw;
            }
        }

        private void WriteProtectValue2it8620(
          int fanControlIndex,
          ref MySmartGuardianFanConfig smartGuardianFanConfig)
        {
            try
            {
                int fanControlIndex1 = fanControlIndex;
                if (this.UsingExtendedTemperature)
                {
                    MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
                    this.RetrieveProtectValueForExtendedTemperature(ref smartGuardianFanConfig, ref guardianFanConfig);
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref guardianFanConfig);
                }
                else
                    this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref smartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }
    }
}
