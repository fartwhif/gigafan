﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.Intel.FanRegulator
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyFanRegulator : MyFanCooler
    {
        protected bool[] m_fanControlMode;
        protected int[] m_fanControlTable;

        public MyFanRegulator()
        {
            this.IsSupported = false;
            this.FanControlCount = 0;
            this.FanCount = 0;
            this.TemperatureCount = 0;
            this.UsingExtendedTemperature = false;
            this.MaxSlopeCount = 3;
            this.InitObjects();
        }

        public override void SetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.SetProtectValue(fanControlIndex, 0, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (slopeIndex >= smartGuardianFanConfigs.Count)
                throw new ArgumentOutOfRangeException();
            try
            {
                int fanControlIndex1 = this.TranslateIT87FanControlIndex(fanControlIndex);
                MySmartGuardianFanConfig fanConfig = smartGuardianFanConfigs[slopeIndex];
                this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            try
            {
                this.SetProtectValue(fanControlIndex, 0, bCalibrateMode, ref smartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          int slopeIndex,
          bool bCalibrateMode,
          ref MySmartGuardianFanConfigCollection smartGuardianFanConfigs)
        {
            if (slopeIndex >= smartGuardianFanConfigs.Count)
                throw new ArgumentOutOfRangeException();
            try
            {
                int fanControlIndex1 = this.TranslateIT87FanControlIndex(fanControlIndex);
                MySmartGuardianFanConfig fanConfig = smartGuardianFanConfigs[slopeIndex];
                this.m_iteMgr.SetFanSmartGuardianConfig(fanControlIndex1, ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void GetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfigCollection pSmartGuardianFanConfigs)
        {
            MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
            try
            {
                this.m_iteMgr.GetFanSmartGuardianConfig(this.TranslateIT87FanControlIndex(fanControlIndex), ref guardianFanConfig);
                this.RetrieveProtectValue(ref guardianFanConfig, this.MaxSlopeCount, ref pSmartGuardianFanConfigs);
            }
            catch
            {
                throw;
            }
        }

        public override void SetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfig smartGuardianFanConfig)
        {
            try
            {
                this.m_iteMgr.SetFanSmartGuardianConfig(this.TranslateIT87FanControlIndex(fanControlIndex), ref smartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void GetProtectValue(
          int fanControlIndex,
          ref MySmartGuardianFanConfig pSmartGuardianFanConfig)
        {
            try
            {
                this.m_iteMgr.GetFanSmartGuardianConfig((int)(byte)this.TranslateIT87FanControlIndex(fanControlIndex), ref pSmartGuardianFanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void GetCpuFanSpeed(out float fanSpeed)
        {
            fanSpeed = 0.0f;
            if (!this.IsSupported)
                throw new NotSupportedException();
            try
            {
                List<float> pFanSpeedDatas = new List<float>();
                List<float> pVoltageDatas = new List<float>();
                List<float> pTemperatureDatas = new List<float>();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetHardwareMonitorDatas(ref pFanSpeedDatas, ref pVoltageDatas, ref pTemperatureDatas);
                if (pFanSpeedDatas.Count <= 0)
                    return;
                fanSpeed = pFanSpeedDatas[0];
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanSpeed(int fanIndex, out float fanSpeed)
        {
            fanSpeed = 0.0f;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (fanIndex >= this.m_iteMgr.FanTachometerCount)
                throw new IndexOutOfRangeException();
            try
            {
                List<float> pHardwareMonitorDatas = new List<float>();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetFanHardwareMonitorDatas(ref pHardwareMonitorDatas);
                if (fanIndex >= pHardwareMonitorDatas.Count)
                    return;
                fanSpeed = pHardwareMonitorDatas[fanIndex];
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanSpeed(int fanIndex, bool bSwitchFanIndex, out float fanSpeed)
        {
            try
            {
                this.GetFanSpeed(fanIndex, out fanSpeed);
            }
            catch
            {
                throw;
            }
        }

        public override void GetFanDisplayName(int fanIndex, out string pDisplayName)
        {
            pDisplayName = string.Empty;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (this.TranslateIT87FanControlIndex(fanIndex) == 0)
                pDisplayName = "CPU Fan";
            else
                pDisplayName = string.Format("System Fan {0}", (object)fanIndex);
        }

        public override void GetFanControlDisplayName(int fanControlIndex, out string displayName)
        {
            displayName = string.Empty;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (this.TranslateIT87FanControlIndex(fanControlIndex) == 0)
                displayName = "CPU Fan";
            else
                displayName = string.Format("System Fan {0}", (object)fanControlIndex);
        }

        public override void SetCalibrationPwm(int fanControlIndex, byte pwmPercentage)
        {
            MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
            byte fanControlIndex1 = (byte)this.TranslateIT87FanControlIndex(fanControlIndex);
            try
            {
                float maxPwmValue = (float)this.m_iteMgr.MaxPwmValue;
                float d = (float)pwmPercentage * (maxPwmValue / 100f);
                float num1 = 50f;
                float num2 = 80f;
                float num3 = (float)(((double)this.m_iteMgr.MaxPwmValue - (double)d) / ((double)num2 - (double)num1));
                fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                fanConfig.TemperatureLimitValueOfFanStart = (byte)num1;
                fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)num2;
                fanConfig.StartPWM = (byte)Math.Floor((double)d);
                fanConfig.Slope = num3;
                fanConfig.DeltaTemperature = (byte)3;
                fanConfig.TargetZoneBoundary = (byte)0;
                this.m_iteMgr.SetFanSmartGuardianConfig((int)fanControlIndex1, ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override void ResetCalibrationPwm()
        {
        }

        public override void SetFanSpeedFixedMode(int fanControlIndex, byte pwmPercentage)
        {
            MySmartGuardianFanConfig fanConfig = new MySmartGuardianFanConfig();
            byte fanControlIndex1 = (byte)this.TranslateIT87FanControlIndex(fanControlIndex);
            try
            {
                float maxPwmValue = (float)this.m_iteMgr.MaxPwmValue;
                float d = (float)pwmPercentage * (maxPwmValue / 100f);
                float num1 = 70f;
                float num2 = 71f;
                float num3 = (float)(((double)this.m_iteMgr.MaxPwmValue - (double)d) / ((double)num2 - (double)num1));
                fanConfig.TemperatureLimitValueOfFanOff = (byte)0;
                fanConfig.TemperatureLimitValueOfFanStart = (byte)num1;
                fanConfig.TemperatureLimitValueOfFanFullSpeed = (byte)num2;
                fanConfig.StartPWM = (byte)Math.Floor((double)d);
                fanConfig.Slope = (double)num3 == 0.0 ? 0.0f : 1f;
                fanConfig.DeltaTemperature = (byte)3;
                fanConfig.TargetZoneBoundary = (byte)0;
                this.m_iteMgr.SetFanSmartGuardianConfig((int)fanControlIndex1, ref fanConfig);
            }
            catch
            {
                throw;
            }
        }

        public override bool IsRequireSoftwareMonitor(int fanControlIndex) => true;

        public override void GetTemperature(int temperatureIndex, out float targetTemperature)
        {
            targetTemperature = 0.0f;
            if (!this.IsSupported)
                throw new NotSupportedException();
            if (temperatureIndex >= this.m_iteMgr.TemperatureCount)
                throw new IndexOutOfRangeException();
            List<float> pHardwareMonitorDatas = new List<float>();
            try
            {
                this.m_iteMgr.Update();
                this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pHardwareMonitorDatas);
                if (temperatureIndex >= pHardwareMonitorDatas.Count)
                    return;
                targetTemperature = pHardwareMonitorDatas[temperatureIndex];
            }
            catch
            {
                throw;
            }
        }

        public override void GetTemperatureInputSelection(
          int fanControlIndex,
          ref MyFanTemperatureInput pTemperatureInputSelection)
        {
            MySmartGuardianFanConfig guardianFanConfig = new MySmartGuardianFanConfig();
            try
            {
                this.m_iteMgr.GetTemperatureInputSelection(this.TranslateIT87FanControlIndex(fanControlIndex), ref pTemperatureInputSelection);
            }
            catch
            {
                throw;
            }
        }

        public override void GetTemperatureDisplayName(int temperatureIndex, out string pDisplayName)
        {
            if (temperatureIndex == 2)
                pDisplayName = "CPU";
            else if (temperatureIndex == 1)
                pDisplayName = "PCH";
            else
                pDisplayName = "SYSTEM";
        }

        public override void GetHardwareMonitorDatas(
          ref List<float> pTemperatureDatas,
          ref List<float> pFanSpeedDatas)
        {
            if (pTemperatureDatas.Count > 0)
                pTemperatureDatas.Clear();
            try
            {
                if (pTemperatureDatas.Count > 0)
                    pTemperatureDatas.Clear();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetTemperatureHardwareMonitorDatas(ref pTemperatureDatas);
            }
            catch
            {
            }
            try
            {
                if (pFanSpeedDatas.Count > 0)
                    pFanSpeedDatas.Clear();
                this.m_iteMgr.Update();
                this.m_iteMgr.GetFanHardwareMonitorDatas(ref pFanSpeedDatas);
            }
            catch
            {
            }
        }

        protected virtual void InitObjects()
        {
            int length = 0;
            try
            {
                MyIT87XXFinder it87XxFinder = new MyIT87XXFinder();
                for (int index = 0; index < 6; ++index)
                {
                    it87XxFinder.Find(true, out this.m_iteMgr);
                    if (this.m_iteMgr == null)
                        Thread.Sleep(1000);
                    else
                        break;
                }
                this.m_fanControlMode = this.m_iteMgr != null ? new bool[this.m_iteMgr.FanControlRegisterCount] : throw new NotSupportedException();
                for (byte fanControlIndex = 0; (int)fanControlIndex < this.m_iteMgr.FanControlRegisterCount; ++fanControlIndex)
                {
                    MyFanControlOutputModeSelection modeSelection = MyFanControlOutputModeSelection.OnOffMode;
                    bool pbEnable = false;
                    this.m_iteMgr.GetFanTachometerEnableStatus((int)fanControlIndex, out pbEnable);
                    if (pbEnable)
                    {
                        this.m_iteMgr.GetFanControlOutputModeSelection((int)fanControlIndex, out modeSelection);
                        if (modeSelection == MyFanControlOutputModeSelection.SmartGuardianMode)
                        {
                            this.m_fanControlMode[(int)fanControlIndex] = true;
                            ++length;
                        }
                        else
                            this.m_fanControlMode[(int)fanControlIndex] = false;
                    }
                }
                this.IsSupported = true;
                this.MaxPwmValue = this.m_iteMgr.MaxPwmValue;
                int pTemperatureCount = 0;
                this.RetrieveTemperatureCount(ref this.m_fanControlMode, out pTemperatureCount);
                this.TemperatureCount = pTemperatureCount > 0 ? pTemperatureCount : this.m_iteMgr.TemperatureCount;
                this.m_fanControlTable = new int[length];
                this.FanControlCount = length;
                this.FanCount = length;
                bool fanTac4Enable = false;
                bool fanTac5Enable = false;
                this.m_iteMgr.GetFanTachometer4And5EnableStatus(out fanTac4Enable, out fanTac5Enable);
                if (fanTac4Enable)
                    ++this.FanCount;
                if (fanTac5Enable)
                    ++this.FanCount;
                this.CreateIT87FanControlTable(ref this.m_fanControlMode, ref this.m_fanControlTable);
            }
            catch
            {
                throw;
            }
        }

        protected virtual void CreateIT87FanControlTable(
          ref bool[] fanControlMode,
          ref int[] fanControlTable)
        {
            if (((IEnumerable<bool>)fanControlMode).Count<bool>() == 0)
                throw new ArgumentException();
            if (((IEnumerable<int>)fanControlTable).Count<int>() == 0)
                throw new ArgumentException();
            int num = 3;
            int index1 = 0;
            int index2 = 0;
            for (; index1 < num; ++index1)
            {
                if (!fanControlMode[index1])
                {
                    if (index1 == 0)
                    {
                        fanControlTable[index2] = 2;
                        ++index2;
                    }
                }
                else
                {
                    fanControlTable[index2] = index1;
                    ++index2;
                    if (index2 >= ((IEnumerable<int>)fanControlTable).Count<int>())
                        break;
                }
            }
        }

        protected virtual int TranslateIT87FanControlIndex(int fanControlIndex)
        {
            if (fanControlIndex >= this.FanControlCount)
                throw new IndexOutOfRangeException();
            return fanControlIndex < ((IEnumerable<int>)this.m_fanControlTable).Count<int>() ? this.m_fanControlTable[fanControlIndex] : throw new IndexOutOfRangeException();
        }

        private void RetrieveTemperatureCount(ref bool[] fanControlMode, out int pTemperatureCount)
        {
            pTemperatureCount = 0;
            int num = ((IEnumerable<bool>)fanControlMode).Count<bool>();
            if (num == 0 || this.m_iteMgr == null)
                return;
            List<MyFanTemperatureInput> source1 = new List<MyFanTemperatureInput>();
            for (int fanControlIndex = 0; fanControlIndex < num; ++fanControlIndex)
            {
                if (fanControlMode[fanControlIndex])
                {
                    MyFanTemperatureInput pTemperatureInputSelection = MyFanTemperatureInput.Unknown;
                    this.m_iteMgr.GetTemperatureInputSelection(fanControlIndex, ref pTemperatureInputSelection);
                    if (pTemperatureInputSelection != MyFanTemperatureInput.Unknown)
                        source1.Add(pTemperatureInputSelection);
                }
            }
            if (source1.Count == 0)
                return;
            IEnumerable source2 = (IEnumerable)source1.Distinct<MyFanTemperatureInput>();
            pTemperatureCount = source2.Count();
        }
    }
}
