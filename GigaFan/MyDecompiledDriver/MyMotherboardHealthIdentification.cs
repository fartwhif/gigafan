﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.Sensor.MotherboardHealthIdentification
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public enum MyMotherboardHealthIdentification
    {
        MHID_Unknown = 0,
        MHID_A001 = 40961, // 0x0000A001
        MHID_A002 = 40962, // 0x0000A002
        MHID_A003 = 40963, // 0x0000A003
        MHID_B001 = 45057, // 0x0000B001
        MHID_B002 = 45058, // 0x0000B002
        MHID_B003 = 45059, // 0x0000B003
        MHID_C001 = 49153, // 0x0000C001
        MHID_C002 = 49154, // 0x0000C002
        MHID_C003 = 49155, // 0x0000C003
        MHID_D001 = 53249, // 0x0000D001
        MHID_E001 = 57345, // 0x0000E001
        MHID_E002 = 57346, // 0x0000E002
        MHID_E003 = 57347, // 0x0000E003
        MHID_E004 = 57348, // 0x0000E004
        MHID_E005 = 57349, // 0x0000E005
        MHID_E006 = 57350, // 0x0000E006
        MHID_E007 = 57351, // 0x0000E007
        MHID_E008 = 57352, // 0x0000E008
        MHID_E009 = 57353, // 0x0000E009
        MHID_F001 = 61441, // 0x0000F001
    }
}
