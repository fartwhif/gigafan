﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.IFanProfile
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public interface MyIFanProfile
    {
        bool IsProfileExist();

        bool IsProfileExist(int fanControlIndex);

        bool IsFanConfigChange4Profile(int fanControlIndex, ref MySmartFanConfig currentFanConfig);

        void CreateNewProfiles();

        void CreateNewProfile(int fanControlIndex);

        void GetProfileLastWriteTime(int fanControlIndex, out DateTime lastWriteTime);

        void ReadFanConfigFromProfile(int fanControlIndex, ref MySmartFanConfig pFanConfig);

        void WriteFanConfigInProfile(int fanControlIndex, ref MySmartFanConfig pFanConfig);
    }
}
