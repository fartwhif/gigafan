﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.CoolingDevice.Fan.Amd.AmdGeneralFanRegulator
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

using System;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal class MyAmdGeneralFanRegulator : MyFanRegulator
    {
        public override void GetFanControlDisplayName(int fanControlIndex, out string displayName)
        {
            displayName = string.Empty;
            if (!this.IsSupported)
                throw new NotSupportedException();
            try
            {
                this.TranslateIT87FanControlIndex(fanControlIndex);
                if (fanControlIndex == 0)
                    displayName = "CPU Fan";
                else
                    displayName = string.Format("System Fan {0}", (object)fanControlIndex);
            }
            catch
            {
                displayName = "System Fan";
            }
        }
    }
}
