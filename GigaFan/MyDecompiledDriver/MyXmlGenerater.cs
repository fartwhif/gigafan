﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EasyFunctions.XML.XmlGenerater
// Assembly: Gigabyte.EasyFunctions, Version=7.2.0.24, Culture=neutral, PublicKeyToken=null
// MVID: 78B279F9-3DF2-44F7-BFAE-CD74B7F9DD3F
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EasyFunctions.dll

using System;
using System.IO;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal static class MyXmlGenerater
    {
        public static XElement ToXElement<T>(this object obj)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (TextWriter textWriter = (TextWriter)new StreamWriter((Stream)memoryStream))
                {
                    new XmlSerializer(typeof(T)).Serialize(textWriter, obj);
                    return XElement.Parse(Encoding.UTF8.GetString(memoryStream.ToArray()));
                }
            }
        }

        public static T FromXElement<T>(this XElement xElement)
        {
            using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xElement.ToString())))
                return (T)new XmlSerializer(typeof(T)).Deserialize((Stream)memoryStream);
        }

        public static string XmlSerializeToString(this object objectInstance)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(objectInstance.GetType());
            StringBuilder sb = new StringBuilder();
            using (TextWriter textWriter = (TextWriter)new StringWriter(sb))
                xmlSerializer.Serialize(textWriter, objectInstance);
            return sb.ToString();
        }

        public static T XmlDeserializeFromString<T>(string objectData)
        {
            return (T)MyXmlGenerater.XmlDeserializeFromString(objectData, typeof(T));
        }

        public static object XmlDeserializeFromString(string objectData, Type type)
        {
            using (TextReader textReader = (TextReader)new StringReader(objectData))
                return new XmlSerializer(type).Deserialize(textReader);
        }


        public static string ToXmlString<T>(this T input)
        {
            using (StringWriter writer = new StringWriter())
            {
                input.ToXml<T>(writer);
                return writer.ToString();
            }
        }

        public static void ToXml<T>(this T objectToSerialize, Stream stream)
        {
            new XmlSerializer(typeof(T)).Serialize(stream, (object)objectToSerialize);
        }

        public static void ToXml<T>(this T objectToSerialize, StringWriter writer)
        {
            new XmlSerializer(typeof(T)).Serialize((TextWriter)writer, (object)objectToSerialize);
        }

        public static void SerializeObjectToXML<T>(T item, string FilePath)
        {
            using (StreamWriter streamWriter = new StreamWriter(FilePath))
                new XmlSerializer(typeof(T)).Serialize((TextWriter)streamWriter, (object)item);
        }

        public static T SerializeObjectFromXML<T>(string FilePath)
        {
            using (StreamReader streamReader = new StreamReader(FilePath))
                return (T)new XmlSerializer(typeof(T)).Deserialize((TextReader)streamReader);
        }
    }
}
