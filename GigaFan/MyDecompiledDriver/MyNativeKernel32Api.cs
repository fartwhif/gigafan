﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.NativeAPI.NativeKernel32Api
// Assembly: Gigabyte, Version=7.2.0.22, Culture=neutral, PublicKeyToken=null
// MVID: FE603752-DF9C-4680-B734-106AEDD3F213
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.dll

using System;
using System.Runtime.InteropServices;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    [Serializable]
    public class MyNativeKernel32Api
    {
        [NonSerialized]
        public const string ModuleName = "kernel32.dll";

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool GetProcessAffinityMask(
          IntPtr hProcess,
          out UIntPtr lpProcessAffinityMask,
          out UIntPtr lpSystemAffinityMask);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetProcessAffinityMask(IntPtr hProcess, UIntPtr dwProcessAffinityMask);

        [DllImport("kernel32.dll")]
        public static extern UIntPtr SetThreadAffinityMask(IntPtr handle, UIntPtr mask);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetCurrentThread();

        [DllImport("kernel32.dll")]
        public static extern IntPtr VirtualAlloc(
          IntPtr lpAddress,
          UIntPtr dwSize,
          MyNativeKernel32Api.AllocationType flAllocationType,
          MyNativeKernel32Api.MemoryProtection flProtect);

        [DllImport("kernel32.dll")]
        public static extern bool VirtualFree(
          IntPtr lpAddress,
          UIntPtr dwSize,
          MyNativeKernel32Api.FreeType dwFreeType);

        [Flags]
        public enum AllocationType : uint
        {
            COMMIT = 4096, // 0x00001000
            RESERVE = 8192, // 0x00002000
            RESET = 524288, // 0x00080000
            LARGE_PAGES = 536870912, // 0x20000000
            PHYSICAL = 4194304, // 0x00400000
            TOP_DOWN = 1048576, // 0x00100000
            WRITE_WATCH = 2097152, // 0x00200000
        }

        [Flags]
        public enum MemoryProtection : uint
        {
            EXECUTE = 16, // 0x00000010
            EXECUTE_READ = 32, // 0x00000020
            EXECUTE_READWRITE = 64, // 0x00000040
            EXECUTE_WRITECOPY = 128, // 0x00000080
            NOACCESS = 1,
            READONLY = 2,
            READWRITE = 4,
            WRITECOPY = 8,
            GUARD = 256, // 0x00000100
            NOCACHE = 512, // 0x00000200
            WRITECOMBINE = 1024, // 0x00000400
        }

        [Flags]
        public enum FreeType
        {
            DECOMMIT = 16384, // 0x00004000
            RELEASE = 32768, // 0x00008000
        }
    }
}
