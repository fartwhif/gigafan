﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.Utilities.BitWiseHelper
// Assembly: Gigabyte, Version=7.2.0.22, Culture=neutral, PublicKeyToken=null
// MVID: FE603752-DF9C-4680-B734-106AEDD3F213
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public static class MyBitWiseHelper
    {
        public static int ZeroBit(int value, int position) => value & ~(1 << position);

        public static int BitSet(int value, int position) => value | ~(1 << position);

        public static bool IsBitSet(int value, int pos) => (value & 1 << pos) != 0;

        public static string GetIntBinaryString(int n)
        {
            char[] chArray = new char[32];
            int index1 = 31;
            for (int index2 = 0; index2 < 32; ++index2)
            {
                chArray[index1] = (n & 1 << index2) == 0 ? '0' : '1';
                --index1;
            }
            return new string(chArray);
        }

        public static uint ZeroBit(uint value, int position)
        {
            uint num = (uint)(1 << position);
            return value & ~num;
        }

        public static uint BitSet(uint value, int position)
        {
            uint num = (uint)(1 << position);
            return value | num;
        }

        public static bool IsBitSet(uint value, int pos) => ((long)value & (long)(1 << pos)) != 0L;
    }
}
