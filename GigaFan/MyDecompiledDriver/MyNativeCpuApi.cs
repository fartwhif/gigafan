﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.NativeAPI.NativeCpuApi
// Assembly: Gigabyte, Version=7.2.0.22, Culture=neutral, PublicKeyToken=null
// MVID: FE603752-DF9C-4680-B734-106AEDD3F213
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.dll

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace GigaFan.MyDecompiledEnvironmentControl
{
    public static class MyNativeCpuApi
    {
        [DllImport("MFCCPU.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GBFunc(uint Var1, uint Var2, StringBuilder Param1, byte Param2);

        [DllImport("MFCCPU.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GBFunc2(
          int Var1,
          int Var2,
          [MarshalAs(UnmanagedType.LPStr)] StringBuilder Param1,
          [MarshalAs(UnmanagedType.LPStr)] StringBuilder Param2);
    }
}
