﻿// Decompiled with JetBrains decompiler
// Type: Gigabyte.EnvironemntControl.BusProtocol.PciFunctionAddress
// Assembly: Gigabyte.EnvironemntControl, Version=7.2.0.72, Culture=neutral, PublicKeyToken=null
// MVID: FB396ABB-93D0-4458-AB2B-E0101B87623E
// Assembly location: C:\Program Files (x86)\Gigabyte\EasyTune\Gigabyte.EnvironemntControl.dll

namespace GigaFan.MyDecompiledEnvironmentControl
{
    internal static class MyPciFunctionAddress
    {
        public static uint ToAddress(
          uint busNumber,
          uint deviceNumber,
          uint functionNumber,
          uint registerOffset)
        {
            return (uint)((int)busNumber << 16 | ((int)deviceNumber & 31) << 11 | ((int)functionNumber & 7) << 8 | (int)registerOffset & (int)byte.MaxValue | int.MinValue);
        }
    }
}
